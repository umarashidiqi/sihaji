-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 06 Okt 2016 pada 05.52
-- Versi Server: 5.6.12-log
-- Versi PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Struktur dari tabel `catatan`
--

CREATE TABLE IF NOT EXISTS `catatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` varchar(255) DEFAULT NULL,
  `uraian_checklist` varchar(255) DEFAULT NULL,
  `uraian_uji` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data untuk tabel `catatan`
--

INSERT INTO `catatan` (`id`, `id_data_umum`, `uraian_checklist`, `uraian_uji`) VALUES
(57, '58', '', ''),
(58, '59', NULL, ''),
(59, '60', '', 'Tekanan Udara = 15 kg/cm'),
(60, '61', '', ''),
(61, '62', NULL, ''),
(62, '63', '', ''),
(64, '65', '', ''),
(65, '66', NULL, ''),
(66, '67', '', 'PT tidak bisa dikeluarkan terminal kabel berada di sisi dalam'),
(67, '68', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `checklist_ct`
--

CREATE TABLE IF NOT EXISTS `checklist_ct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(50) DEFAULT NULL,
  `id_alat` int(50) DEFAULT NULL,
  `k1` varchar(50) DEFAULT NULL,
  `k2` varchar(50) DEFAULT NULL,
  `k3` varchar(50) DEFAULT NULL,
  `k4` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=577 ;

--
-- Dumping data untuk tabel `checklist_ct`
--

INSERT INTO `checklist_ct` (`id`, `id_data_umum`, `id_alat`, `k1`, `k2`, `k3`, `k4`) VALUES
(493, 58, 1, NULL, NULL, NULL, NULL),
(494, 58, 2, '1', NULL, '1', NULL),
(495, 58, 3, '1', NULL, '1', NULL),
(496, 58, 4, NULL, NULL, NULL, NULL),
(497, 58, 5, NULL, '1', NULL, '1'),
(498, 58, 6, NULL, '1', NULL, '1'),
(499, 58, 7, '1', NULL, '1', NULL),
(500, 58, 8, NULL, '1', NULL, '1'),
(501, 58, 9, NULL, NULL, NULL, NULL),
(502, 58, 10, NULL, '1', NULL, '1'),
(503, 58, 11, NULL, '1', NULL, '1'),
(504, 58, 12, NULL, '1', NULL, '1'),
(505, 58, 13, NULL, NULL, NULL, NULL),
(506, 58, 14, NULL, '1', NULL, '1'),
(507, 58, 15, NULL, '1', NULL, '1'),
(508, 58, 16, NULL, NULL, NULL, NULL),
(509, 58, 17, NULL, NULL, NULL, NULL),
(510, 58, 18, NULL, NULL, NULL, NULL),
(511, 58, 19, NULL, NULL, NULL, NULL),
(512, 58, 20, '1', NULL, '1', NULL),
(513, 58, 21, '1', NULL, '1', NULL),
(514, 61, 1, NULL, NULL, NULL, NULL),
(515, 61, 2, '1', NULL, NULL, NULL),
(516, 61, 3, '1', NULL, NULL, NULL),
(517, 61, 4, NULL, NULL, NULL, NULL),
(518, 61, 5, NULL, '1', NULL, '1'),
(519, 61, 6, '1', NULL, NULL, '1'),
(520, 61, 7, '1', NULL, '1', NULL),
(521, 61, 8, NULL, '1', NULL, '1'),
(522, 61, 9, NULL, NULL, NULL, NULL),
(523, 61, 10, '1', NULL, NULL, '1'),
(524, 61, 11, NULL, '1', NULL, '1'),
(525, 61, 12, NULL, '1', NULL, '1'),
(526, 61, 13, NULL, NULL, NULL, NULL),
(527, 61, 14, NULL, '1', NULL, '1'),
(528, 61, 15, NULL, '1', NULL, '1'),
(529, 61, 16, NULL, NULL, NULL, NULL),
(530, 61, 17, '1', NULL, '1', NULL),
(531, 61, 18, '1', NULL, '1', NULL),
(532, 61, 19, NULL, NULL, NULL, NULL),
(533, 61, 20, NULL, '1', NULL, '1'),
(534, 61, 21, NULL, '1', NULL, '1'),
(556, 65, 1, NULL, NULL, NULL, NULL),
(557, 65, 2, '1', NULL, '1', NULL),
(558, 65, 3, '1', NULL, '1', NULL),
(559, 65, 4, NULL, NULL, NULL, NULL),
(560, 65, 5, NULL, '1', NULL, '1'),
(561, 65, 6, '1', NULL, NULL, '1'),
(562, 65, 7, NULL, NULL, NULL, NULL),
(563, 65, 8, NULL, '1', NULL, '1'),
(564, 65, 9, NULL, NULL, NULL, NULL),
(565, 65, 10, '1', NULL, NULL, '1'),
(566, 65, 11, NULL, '1', NULL, '1'),
(567, 65, 12, NULL, '1', NULL, '1'),
(568, 65, 13, NULL, NULL, NULL, NULL),
(569, 65, 14, NULL, '1', NULL, '1'),
(570, 65, 15, NULL, '1', NULL, '1'),
(571, 65, 16, NULL, NULL, NULL, NULL),
(572, 65, 17, '1', NULL, '1', NULL),
(573, 65, 18, NULL, NULL, NULL, NULL),
(574, 65, 19, NULL, NULL, NULL, NULL),
(575, 65, 20, NULL, '1', NULL, '1'),
(576, 65, 21, NULL, '1', NULL, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `checklist_la`
--

CREATE TABLE IF NOT EXISTS `checklist_la` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_alat` int(11) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `checklist_pmt`
--

CREATE TABLE IF NOT EXISTS `checklist_pmt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_alat` int(11) DEFAULT NULL,
  `k1` varchar(50) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(50) DEFAULT NULL,
  `k4` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=771 ;

--
-- Dumping data untuk tabel `checklist_pmt`
--

INSERT INTO `checklist_pmt` (`id`, `id_data_umum`, `id_alat`, `k1`, `k2`, `k3`, `k4`) VALUES
(643, 59, 1, NULL, NULL, NULL, NULL),
(644, 59, 2, '1', NULL, '1', NULL),
(645, 59, 3, '1', NULL, '1', NULL),
(646, 59, 4, '1', NULL, '1', NULL),
(647, 59, 5, NULL, NULL, NULL, NULL),
(648, 59, 6, NULL, '1', NULL, '1'),
(649, 59, 7, '', '1', NULL, '1'),
(650, 59, 8, '1', NULL, '1', NULL),
(651, 59, 9, '1', NULL, '1', NULL),
(652, 59, 10, NULL, NULL, NULL, NULL),
(653, 59, 11, '1', NULL, NULL, '1'),
(654, 59, 12, NULL, '1', NULL, '1'),
(655, 59, 13, '1', NULL, NULL, '1'),
(656, 59, 14, NULL, NULL, NULL, NULL),
(657, 59, 15, NULL, '1', NULL, '1'),
(658, 59, 16, NULL, '1', NULL, '1'),
(659, 59, 17, NULL, '1', NULL, '1'),
(660, 59, 18, NULL, '1', NULL, '1'),
(661, 59, 19, NULL, NULL, NULL, NULL),
(662, 59, 20, '1', NULL, '1', NULL),
(663, 59, 21, NULL, '1', NULL, '1'),
(664, 59, 22, '1', NULL, '1', NULL),
(665, 59, 23, NULL, NULL, NULL, NULL),
(666, 59, 24, '', NULL, '', NULL),
(667, 59, 25, '', NULL, '', NULL),
(668, 59, 26, NULL, '', NULL, ''),
(669, 59, 27, NULL, NULL, NULL, NULL),
(670, 59, 28, '1', NULL, '1', NULL),
(671, 59, 29, '1', NULL, '1', NULL),
(672, 59, 30, NULL, NULL, NULL, NULL),
(673, 59, 31, '1', '', '1', ''),
(674, 59, 32, '1', '', '1', ''),
(675, 62, 1, NULL, NULL, '', NULL),
(676, 62, 2, '1', NULL, '1', NULL),
(677, 62, 3, '1', NULL, '1', NULL),
(678, 62, 4, NULL, NULL, NULL, NULL),
(679, 62, 5, NULL, NULL, NULL, NULL),
(680, 62, 6, NULL, '1', NULL, '1'),
(681, 62, 7, '1', NULL, NULL, '1'),
(682, 62, 8, '1', NULL, '1', NULL),
(683, 62, 9, '1', NULL, '1', NULL),
(684, 62, 10, NULL, NULL, NULL, NULL),
(685, 62, 11, '1', NULL, NULL, '1'),
(686, 62, 12, NULL, '1', NULL, '1'),
(687, 62, 13, '1', NULL, NULL, '1'),
(688, 62, 14, NULL, NULL, NULL, NULL),
(689, 62, 15, NULL, '1', NULL, '1'),
(690, 62, 16, NULL, '1', NULL, '1'),
(691, 62, 17, NULL, '1', NULL, '1'),
(692, 62, 18, NULL, '1', NULL, '1'),
(693, 62, 19, NULL, NULL, NULL, NULL),
(694, 62, 20, '1', NULL, '1', NULL),
(695, 62, 21, NULL, '1', NULL, '1'),
(696, 62, 22, '1', NULL, '1', NULL),
(697, 62, 23, NULL, NULL, NULL, NULL),
(698, 62, 24, '1', NULL, '1', NULL),
(699, 62, 25, '1', NULL, '1', NULL),
(700, 62, 26, NULL, '1', NULL, '1'),
(701, 62, 27, NULL, NULL, NULL, NULL),
(702, 62, 28, '1', NULL, '1', NULL),
(703, 62, 29, '1', NULL, '1', NULL),
(704, 62, 30, NULL, NULL, NULL, NULL),
(705, 62, 31, NULL, '1', NULL, '1'),
(706, 62, 32, NULL, '1', NULL, '1'),
(707, 66, 1, NULL, NULL, NULL, NULL),
(708, 66, 2, '1', NULL, '1', NULL),
(709, 66, 3, '1', NULL, '1', NULL),
(710, 66, 4, '1', NULL, '1', NULL),
(711, 66, 5, NULL, NULL, NULL, NULL),
(712, 66, 6, NULL, '1', NULL, '1'),
(713, 66, 7, NULL, '1', NULL, '1'),
(714, 66, 8, '1', NULL, '1', NULL),
(715, 66, 9, '1', NULL, '1', NULL),
(716, 66, 10, NULL, NULL, NULL, NULL),
(717, 66, 11, '1', NULL, NULL, '1'),
(718, 66, 12, NULL, '1', NULL, '1'),
(719, 66, 13, '1', NULL, NULL, '1'),
(720, 66, 14, NULL, NULL, NULL, NULL),
(721, 66, 15, NULL, '1', NULL, '1'),
(722, 66, 16, NULL, '1', NULL, '1'),
(723, 66, 17, NULL, '1', NULL, '1'),
(724, 66, 18, NULL, '1', NULL, '1'),
(725, 66, 19, NULL, NULL, NULL, NULL),
(726, 66, 20, '1', NULL, '1', NULL),
(727, 66, 21, NULL, '1', NULL, '1'),
(728, 66, 22, '1', NULL, '1', NULL),
(729, 66, 23, NULL, NULL, NULL, NULL),
(730, 66, 24, NULL, NULL, NULL, NULL),
(731, 66, 25, NULL, NULL, NULL, NULL),
(732, 66, 26, NULL, NULL, NULL, NULL),
(733, 66, 27, NULL, NULL, NULL, NULL),
(734, 66, 28, '1', NULL, '1', NULL),
(735, 66, 29, '1', NULL, '1', NULL),
(736, 66, 30, NULL, NULL, NULL, NULL),
(737, 66, 31, '1', NULL, '1', NULL),
(738, 66, 32, '1', NULL, '1', NULL),
(739, 68, 1, NULL, NULL, NULL, NULL),
(740, 68, 2, '1', NULL, '1', NULL),
(741, 68, 3, '1', NULL, '1', NULL),
(742, 68, 4, '1', NULL, '1', NULL),
(743, 68, 5, NULL, NULL, NULL, NULL),
(744, 68, 6, NULL, '1', NULL, '1'),
(745, 68, 7, '1', NULL, NULL, '1'),
(746, 68, 8, '1', NULL, '1', NULL),
(747, 68, 9, '1', NULL, '1', NULL),
(748, 68, 10, NULL, NULL, NULL, NULL),
(749, 68, 11, '1', NULL, NULL, '1'),
(750, 68, 12, NULL, '1', NULL, '1'),
(751, 68, 13, '1', NULL, NULL, '1'),
(752, 68, 14, NULL, NULL, NULL, NULL),
(753, 68, 15, NULL, '1', NULL, '1'),
(754, 68, 16, NULL, '1', NULL, '1'),
(755, 68, 17, NULL, '1', NULL, '1'),
(756, 68, 18, NULL, '1', NULL, '1'),
(757, 68, 19, NULL, NULL, NULL, NULL),
(758, 68, 20, '1', NULL, '1', NULL),
(759, 68, 21, NULL, '1', NULL, '1'),
(760, 68, 22, '1', NULL, '1', NULL),
(761, 68, 23, NULL, NULL, NULL, NULL),
(762, 68, 24, NULL, NULL, NULL, NULL),
(763, 68, 25, '1', NULL, '1', NULL),
(764, 68, 26, NULL, '1', NULL, '1'),
(765, 68, 27, NULL, NULL, NULL, NULL),
(766, 68, 28, '1', NULL, '1', NULL),
(767, 68, 29, '1', NULL, '1', NULL),
(768, 68, 30, NULL, NULL, NULL, NULL),
(769, 68, 31, NULL, '1', NULL, '1'),
(770, 68, 32, NULL, '1', NULL, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `checklist_pt`
--

CREATE TABLE IF NOT EXISTS `checklist_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(50) DEFAULT NULL,
  `id_alat` int(50) DEFAULT NULL,
  `k1` varchar(50) DEFAULT NULL,
  `k2` varchar(50) DEFAULT NULL,
  `k3` varchar(50) DEFAULT NULL,
  `k4` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=589 ;

--
-- Dumping data untuk tabel `checklist_pt`
--

INSERT INTO `checklist_pt` (`id`, `id_data_umum`, `id_alat`, `k1`, `k2`, `k3`, `k4`) VALUES
(520, 60, 0, NULL, NULL, NULL, NULL),
(521, 60, 1, NULL, NULL, NULL, NULL),
(522, 60, 2, '1', NULL, '1', NULL),
(523, 60, 3, '1', NULL, '1', NULL),
(524, 60, 4, NULL, NULL, NULL, NULL),
(525, 60, 5, NULL, '', '', NULL),
(526, 60, 6, NULL, '1', '1', NULL),
(527, 60, 7, '1', NULL, '1', NULL),
(528, 60, 8, NULL, '1', NULL, '1'),
(529, 60, 9, NULL, NULL, NULL, NULL),
(530, 60, 10, NULL, '1', '1', NULL),
(531, 60, 11, NULL, '1', NULL, '1'),
(532, 60, 12, NULL, '1', '1', NULL),
(533, 60, 13, NULL, NULL, NULL, NULL),
(534, 60, 14, NULL, '1', NULL, '1'),
(535, 60, 15, NULL, '1', NULL, '1'),
(536, 60, 16, NULL, '1', NULL, '1'),
(537, 60, 17, NULL, NULL, NULL, NULL),
(538, 60, 18, '1', NULL, '1', NULL),
(539, 60, 19, '', NULL, '', NULL),
(540, 60, 20, NULL, NULL, NULL, NULL),
(541, 60, 21, NULL, '1', NULL, '1'),
(542, 60, 22, NULL, '1', NULL, '1'),
(543, 63, 0, NULL, NULL, NULL, NULL),
(544, 63, 1, NULL, NULL, NULL, NULL),
(545, 63, 2, '1', NULL, '1', NULL),
(546, 63, 3, '1', NULL, '1', NULL),
(547, 63, 4, NULL, NULL, NULL, NULL),
(548, 63, 5, NULL, '1', '1', NULL),
(549, 63, 6, NULL, '1', '1', NULL),
(550, 63, 7, '1', NULL, '1', NULL),
(551, 63, 8, NULL, '1', NULL, '1'),
(552, 63, 9, NULL, NULL, NULL, NULL),
(553, 63, 10, NULL, '1', '1', NULL),
(554, 63, 11, NULL, '1', NULL, '1'),
(555, 63, 12, NULL, '1', '1', NULL),
(556, 63, 13, NULL, NULL, NULL, NULL),
(557, 63, 14, NULL, '1', NULL, '1'),
(558, 63, 15, NULL, '1', NULL, '1'),
(559, 63, 16, NULL, '1', NULL, '1'),
(560, 63, 17, NULL, NULL, NULL, NULL),
(561, 63, 18, '1', NULL, '1', NULL),
(562, 63, 19, '1', NULL, '1', NULL),
(563, 63, 20, NULL, NULL, NULL, NULL),
(564, 63, 21, NULL, '1', NULL, '1'),
(565, 63, 22, NULL, '1', NULL, '1'),
(566, 67, 0, NULL, NULL, NULL, NULL),
(567, 67, 1, NULL, NULL, NULL, NULL),
(568, 67, 2, '1', NULL, '1', NULL),
(569, 67, 3, '1', NULL, '1', NULL),
(570, 67, 4, NULL, NULL, NULL, NULL),
(571, 67, 5, NULL, '1', '1', NULL),
(572, 67, 6, NULL, '1', '1', NULL),
(573, 67, 7, '1', NULL, '1', NULL),
(574, 67, 8, NULL, '1', NULL, '1'),
(575, 67, 9, NULL, NULL, NULL, NULL),
(576, 67, 10, NULL, '1', '1', NULL),
(577, 67, 11, NULL, '1', NULL, '1'),
(578, 67, 12, NULL, NULL, NULL, NULL),
(579, 67, 13, NULL, NULL, NULL, NULL),
(580, 67, 14, NULL, '1', NULL, '1'),
(581, 67, 15, NULL, '1', NULL, '1'),
(582, 67, 16, NULL, '1', NULL, '1'),
(583, 67, 17, NULL, NULL, NULL, NULL),
(584, 67, 18, '1', NULL, '1', NULL),
(585, 67, 19, NULL, NULL, NULL, NULL),
(586, 67, 20, NULL, NULL, NULL, NULL),
(587, 67, 21, NULL, '1', NULL, '1'),
(588, 67, 22, NULL, '1', NULL, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_umum`
--

CREATE TABLE IF NOT EXISTS `data_umum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `merk` varchar(50) DEFAULT NULL,
  `type_fasa_r` varchar(50) DEFAULT NULL,
  `type_fasa_s` varchar(50) DEFAULT NULL,
  `type_fasa_t` varchar(50) DEFAULT NULL,
  `ratio_core_1` varchar(50) DEFAULT NULL,
  `ratio_core_2` varchar(50) DEFAULT NULL,
  `ratio_core_3` varchar(50) DEFAULT NULL,
  `ratio_core_4` varchar(50) DEFAULT NULL,
  `ratio_core_5` varchar(50) DEFAULT NULL,
  `bay` varchar(50) DEFAULT NULL,
  `lokasi_gi` varchar(50) DEFAULT NULL,
  `jenis_mode_testing` varchar(50) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data untuk tabel `data_umum`
--

INSERT INTO `data_umum` (`id`, `id_user`, `nomor`, `tanggal`, `type`, `merk`, `type_fasa_r`, `type_fasa_s`, `type_fasa_t`, `ratio_core_1`, `ratio_core_2`, `ratio_core_3`, `ratio_core_4`, `ratio_core_5`, `bay`, `lokasi_gi`, `jenis_mode_testing`, `tahun`) VALUES
(58, 7, '', '2015-01-04 00:00:00', 'PC 14 S', 'MITSHUBISHI', '3094 02', '3094 03', '3094 04', '600 / 1A', '600 / 1A', '1200 / 1A', '', '', 'KRAPYAK 1', 'SRONDOL', 'CT', '2015'),
(59, 7, '', '2015-04-01 00:00:00', 'IAO-SFM-40A', 'MITSHUBISHI', '', '00 351', '', NULL, NULL, NULL, NULL, NULL, 'KRAPYAK 1', 'SRONDOL', 'PMT', '2015'),
(60, 7, '', '2015-04-02 00:00:00', 'PY 2A - 14', 'MITSHUBITSHI', '309 305', '309 337', '309 304', '', '', NULL, NULL, NULL, 'KRAPYAK 1', 'SRONDOL', 'PT', '2015'),
(61, 7, '', '2015-02-11 00:00:00', 'PC145', 'MITSUBISHI', '', '', '309406T', '600/1', '600/1', '1200/1', '', '', 'PDLAMP 1', 'SRONDOL', 'CT', '2015'),
(62, 7, '', '2015-02-11 00:00:00', '8665687', 'ABB', 'A1', 'B1', 'C1', NULL, NULL, NULL, NULL, NULL, 'PDLAM 1', 'SRONDOL', 'PMT', '2015'),
(63, 7, '', '2015-02-11 00:00:00', 'PY/2C/14', 'MITSHUBISHI', 'H0T-7-117', '', '', '', '', NULL, NULL, NULL, 'PDLAMP 1', 'SRONDOL', 'PT', '2015'),
(65, 7, '', '2015-06-11 00:00:00', 'PC - 145', 'MITSHUBISHI', '309375', '310084', '310082', '', '', '', '', '', 'TRAFO 1', 'SRONDOL', 'CT', '2015'),
(66, 7, '', '2015-06-01 00:00:00', 'LTB 170 DI', 'ABB HV', '813 6549', '', '', NULL, NULL, NULL, NULL, NULL, 'TRAFO 1', 'SRONDOL', 'PMT', '2015'),
(67, 7, '', '2015-06-01 00:00:00', 'VTB 24', 'TRAFINDO', '08V 14637', '08V 14642', '08V 14632', '', '', NULL, NULL, NULL, 'TRAFO 1', 'SRONDOL', 'PT', '2015'),
(68, 7, '', '2014-05-07 00:00:00', 'LTB170E1', 'ABB', '1HSB0441027', '', '', NULL, NULL, NULL, NULL, NULL, 'KOPEL', 'SRONDOL', 'PMT', '2014');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_uji_ct`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_ct` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` int(11) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_uji_ct`
--

INSERT INTO `hasil_uji_ct` (`id`, `kode`, `pembeda`, `titik_ukur`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, 'A.A.A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1.1', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '', 1, 'Primer - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '', 1, 'Primer - Sek 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '', 1, 'Primer - Sek 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '', 1, 'Primer - Sek 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '', 1, 'Primer - Sek 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '', 1, 'Primer - Sek 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '1.2', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '7', 2, 'Sek. 1 - Sek. 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '', 2, 'Sek. 1 - Sek. 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '', 2, 'Sek. 1 - Sek. 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '', 2, 'Sek. 1 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '', 2, 'Sek. 2 - Sek. 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '', 2, 'Sek. 2 - Sek. 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '', 2, 'Sek. 2 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '', 2, 'Sek. 3 - Sek. 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '', 2, 'Sek. 3 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '', 2, 'Sek. 4 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '', 2, 'Sek. 1 - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '', 2, 'Sek. 2 - Tanah', '', '', '', '', '', '', '', '', ''),
(23, '', 2, 'Sek. 2 - Tanah', '', '', '', '', '', '', '', '', ''),
(24, '', 2, 'Sek. 4 - Tanah', '', '', '', '', '', '', '', '', ''),
(25, '', 2, 'Sek. 5 - Tanah', '', '', '', '', '', '', '', '', ''),
(26, '', 3, 'Alat Uji Yang Digunakan :', '', '', '', '', '', '', '', '', ''),
(27, 'B.B.B', 4, 'Tahanan Pentanahan ', '', '', '', '', '', '', '', '', ''),
(28, '', 5, 'Tahanan Pentanahan', '', '', '', '', '', '', '', '', ''),
(29, '', 6, 'Alat Uji Yang Digunakan :', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_uji_la`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_la` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` int(11) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_uji_la`
--

INSERT INTO `hasil_uji_la` (`id`, `kode`, `pembeda`, `titik_ukur`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, '', 1, 'Tahanan Isolasi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1', 1, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1.1', 1, 'Atas - Bawah ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '1.2', 1, 'Atas - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '1.3', 1, 'Bawah - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '10', 1, 'Alat Uji Yang Digunakan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '2', 2, 'Tahanan Petanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '2.1', 2, 'Tahanan Petanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '', 2, 'Alat Uji Yang Digunakan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '3', 3, 'Counter Arrester', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '', 3, 'Penunjukan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_uji_pmt`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_pmt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` varchar(255) NOT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `tu_sambung` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_uji_pmt`
--

INSERT INTO `hasil_uji_pmt` (`id`, `kode`, `pembeda`, `titik_ukur`, `tu_sambung`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, '1', '', 'Tahanan Isolasi', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, '1', 'Megger', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1.1', '1', 'Atas - Bawah                          PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '1.2', '1', 'Atas - Tanah                           PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '1.3', '1', 'Bawah - Tanah                       PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '1.4', '1', 'Fasa - Tanah                           PMT ON', 'PMT ON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '10', '1', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '2', '2', 'Tahanan Kontak ( mW )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '', '2', 'Atas - Bawah                         PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '10', '2', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '3', '3', 'Tegangan Tembus Minyak ( kV / 2,5 mm )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, '3', 'Minyak PMT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '10', '3', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '4', '4', 'Tekanan Gas SF 6 ( Mpa ), ( Bar ), ( Psi ) ( kg/cm2)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '', '4', 'Pressure Gauge ( Visual )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '10', '4', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '5', '5', 'Tahanan Pentanahan ( W )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '', '5', 'Tahanan Pentanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '10', '5', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '6', '6', 'Pengujian Keserempakan / Breaker ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '', '6', 'Counter PMT Sebelum Pengujian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '', '6', 'Counter PMT Setelah Pengujian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '6.1', '6', 'OPEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '6.2', '6', 'CLOSE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '10', '6', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '7', '7', 'Print - Out  /  Breaker Analyzer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_uji_pt`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_pt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` int(11) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_uji_pt`
--

INSERT INTO `hasil_uji_pt` (`id`, `kode`, `pembeda`, `titik_ukur`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, 'A.A.A', NULL, 'Tahanan  Isolasi belitan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1.1', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1.1', 1, 'Primer - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '1.2', 1, 'Primer - Sekunder (1a)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '1.3', 1, 'Primer - Sekunder (2a)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '1.2', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '1.4', 2, 'Sekunder 1a - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '1.5', 2, 'Sekunder 2a - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '1.6', 2, 'Sekunder 1a - 2a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '', 3, 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'B.B.B', 4, 'Tahanan Pentanahan ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '', 5, 'Tahanan Pentanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '', 6, 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penanggung_jawab`
--

CREATE TABLE IF NOT EXISTS `penanggung_jawab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pelaksana` varchar(50) DEFAULT NULL,
  `pengawas` varchar(50) DEFAULT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=190 ;

--
-- Dumping data untuk tabel `penanggung_jawab`
--

INSERT INTO `penanggung_jawab` (`id`, `pelaksana`, `pengawas`, `id_data_umum`) VALUES
(157, 'Martono', 'Ami W', 58),
(158, 'Taufan', NULL, 58),
(159, 'Febrian', NULL, 58),
(160, 'Ngalimin', 'Abdurrahman', 59),
(161, 'Taufan', NULL, 59),
(162, 'Febrian', NULL, 59),
(163, 'Martono', 'Abdurrahman', 60),
(164, 'Bambang', NULL, 60),
(165, '', NULL, 60),
(166, 'Prasetya', 'Abdurrahman', 61),
(167, 'Kusnadi', NULL, 61),
(168, '', NULL, 61),
(169, 'Topan', 'Abdurrahman', 62),
(170, 'Kusnadi', NULL, 62),
(171, 'Darminto', NULL, 62),
(172, 'Prasetyo', 'Abdurrahman', 63),
(173, 'Kusnadi', NULL, 63),
(174, '', NULL, 63),
(178, 'Yanuar', 'Riyanto', 65),
(179, 'Azmy', NULL, 65),
(180, '', NULL, 65),
(181, 'Ami W', 'Rianto', 66),
(182, 'Setyawan', NULL, 66),
(183, 'Abdul Rahman', NULL, 66),
(184, 'Martono', 'Riyanto', 67),
(185, 'Taufan', NULL, 67),
(186, '', NULL, 67),
(187, 'Teguh', 'Ahmadi', 68),
(188, 'Taufan', NULL, 68),
(189, 'Ripan', NULL, 68);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengujian_ct`
--

CREATE TABLE IF NOT EXISTS `pengujian_ct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `injeksi_teg1` varchar(255) DEFAULT NULL,
  `injeksi_teg2` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `injeksi_alat_uji` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=650 ;

--
-- Dumping data untuk tabel `pengujian_ct`
--

INSERT INTO `pengujian_ct` (`id`, `id_data_umum`, `id_hasil_uji`, `injeksi_teg1`, `injeksi_teg2`, `fasa_r_std`, `fasa_r_th`, `fasa_r_hu`, `fasa_s_std`, `fasa_s_th`, `fasa_s_hu`, `fasa_t_std`, `fasa_t_th`, `fasa_t_hu`, `injeksi_alat_uji`) VALUES
(546, 58, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(547, 58, 3, NULL, NULL, '', '', '81800', '', '', '73900', '', '', '64500', NULL),
(548, 58, 4, NULL, NULL, '', '', '64900', '', '', '79400', '', '', '47400', NULL),
(549, 58, 5, NULL, NULL, '', '', '81400', '', '', '54800', '', '', '45900', NULL),
(550, 58, 6, NULL, NULL, '', '', '78900', '', '', '34600', '', '', '57400', NULL),
(551, 58, 7, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(552, 58, 8, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(553, 58, 9, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '500'),
(554, 58, 10, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(555, 58, 11, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(556, 58, 12, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(557, 58, 13, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(558, 58, 14, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(559, 58, 15, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(560, 58, 16, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(561, 58, 17, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(562, 58, 18, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(563, 58, 19, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(564, 58, 20, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(565, 58, 22, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(566, 58, 23, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(567, 58, 24, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(568, 58, 25, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(569, 58, 26, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(570, 58, 28, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(571, 58, 29, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(572, 61, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(573, 61, 3, NULL, NULL, '', '', '33400', '', '', '18000', '', '', '55900', NULL),
(574, 61, 4, NULL, NULL, '', '', '37800', '', '', '36200', '', '', '49300', NULL),
(575, 61, 5, NULL, NULL, '', '', '40100', '', '', '42700', '', '', '57200', NULL),
(576, 61, 6, NULL, NULL, '', '', '-', '', '', '52200', '', '', '63100', NULL),
(577, 61, 7, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(578, 61, 8, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(579, 61, 9, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '500'),
(580, 61, 10, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(581, 61, 11, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(582, 61, 12, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(583, 61, 13, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(584, 61, 14, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(585, 61, 15, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(586, 61, 16, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(587, 61, 17, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(588, 61, 18, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(589, 61, 19, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(590, 61, 20, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(591, 61, 22, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(592, 61, 23, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(593, 61, 24, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(594, 61, 25, NULL, NULL, '', '', '-', '', '', '-', '', '', '-', NULL),
(595, 61, 26, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(596, 61, 28, NULL, NULL, '', '', '0.3', '', '', '0.3', '', '', '0.3', NULL),
(597, 61, 29, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(624, 65, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(625, 65, 3, NULL, NULL, '', '', '40000', '', '', '38500', '', '', '39800', NULL),
(626, 65, 4, NULL, NULL, '', '', '65900', '', '', '65000', '', '', '65300', NULL),
(627, 65, 5, NULL, NULL, '', '', '46300', '', '', '52200', '', '', '56800', NULL),
(628, 65, 6, NULL, NULL, '', '', '50000', '', '', '63100', '', '', '85000', NULL),
(629, 65, 7, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(630, 65, 8, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(631, 65, 9, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(632, 65, 10, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(633, 65, 11, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(634, 65, 12, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(635, 65, 13, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(636, 65, 14, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(637, 65, 15, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(638, 65, 16, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(639, 65, 17, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(640, 65, 18, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(641, 65, 19, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(642, 65, 20, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(643, 65, 22, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(644, 65, 23, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(645, 65, 24, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(646, 65, 25, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(647, 65, 26, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(648, 65, 28, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(649, 65, 29, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengujian_la`
--

CREATE TABLE IF NOT EXISTS `pengujian_la` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `jenis_uji` varchar(255) DEFAULT NULL,
  `injeksi_teg` varchar(255) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `fasa_r_sebelum` varchar(255) DEFAULT NULL,
  `fasa_r_sesudah` varchar(255) DEFAULT NULL,
  `fasa_s_sebelum` varchar(255) DEFAULT NULL,
  `fasa_s_sesudah` varchar(255) DEFAULT NULL,
  `fasa_t_sebelum` varchar(255) DEFAULT NULL,
  `fasa_t_sesudah` varchar(255) DEFAULT NULL,
  `catatan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengujian_pmt`
--

CREATE TABLE IF NOT EXISTS `pengujian_pmt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(255) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `jenis_uji` varchar(255) DEFAULT NULL,
  `tahanan_isolasi` varchar(255) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `alat_uji` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `catatan` text,
  `pmt_sebelum` varchar(255) DEFAULT NULL,
  `pmt_sesudah` varchar(255) DEFAULT NULL,
  `meger_alat_uji` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=427 ;

--
-- Dumping data untuk tabel `pengujian_pmt`
--

INSERT INTO `pengujian_pmt` (`id`, `id_data_umum`, `id_hasil_uji`, `kode`, `jenis_uji`, `tahanan_isolasi`, `titik_ukur`, `alat_uji`, `fasa_r_std`, `fasa_r_th`, `fasa_r_hu`, `fasa_s_std`, `fasa_s_th`, `fasa_s_hu`, `fasa_t_std`, `fasa_t_th`, `fasa_t_hu`, `catatan`, `pmt_sebelum`, `pmt_sesudah`, `meger_alat_uji`) VALUES
(351, 59, 2, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '5000'),
(352, 59, 3, NULL, NULL, NULL, NULL, NULL, '', '', '3350', '', '', '11000', '', '', '46700', NULL, NULL, NULL, NULL),
(353, 59, 4, NULL, NULL, NULL, NULL, NULL, '', '', '53100', '', '', '19300', '', '', '46000', NULL, NULL, NULL, NULL),
(354, 59, 5, NULL, NULL, NULL, NULL, NULL, '', '', '4800', '', '', '15500', '', '', '7890', NULL, NULL, NULL, NULL),
(355, 59, 6, NULL, NULL, NULL, NULL, NULL, '', '', '6030', '', '', '4760', '', '', '7200', NULL, NULL, NULL, NULL),
(356, 59, 7, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(357, 59, 9, NULL, NULL, NULL, NULL, NULL, '', '', '44', '', '', '38', '', '', '36', NULL, NULL, NULL, NULL),
(358, 59, 10, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(359, 59, 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL),
(360, 59, 13, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(361, 59, 15, NULL, NULL, NULL, NULL, NULL, '', '', '6.5', '', '', '6.5', '', '', '6.5', NULL, NULL, NULL, NULL),
(362, 59, 16, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(363, 59, 18, NULL, NULL, NULL, NULL, NULL, '', '', '0.22', '', '', '0.22', '', '', '0.23', NULL, NULL, NULL, NULL),
(364, 59, 19, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(365, 59, 21, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, '184', NULL, NULL),
(366, 59, 22, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', NULL),
(367, 59, 23, NULL, NULL, NULL, NULL, NULL, '', '', '22', '', '', '22', '', '', '22', NULL, NULL, NULL, NULL),
(368, 59, 24, NULL, NULL, NULL, NULL, NULL, '', '', '82', '', '', '83', '', '', '83', NULL, NULL, NULL, NULL),
(369, 59, 25, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'ISA CBA 2000'),
(370, 62, 2, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '5000'),
(371, 62, 3, NULL, NULL, NULL, NULL, NULL, '', '', '87300', '', '', '24000', '', '', '182000', NULL, NULL, NULL, NULL),
(372, 62, 4, NULL, NULL, NULL, NULL, NULL, '', '', '30300', '', '', '62800', '', '', '32900', NULL, NULL, NULL, NULL),
(373, 62, 5, NULL, NULL, NULL, NULL, NULL, '', '', '151000', '', '', '30100', '', '', '166000', NULL, NULL, NULL, NULL),
(374, 62, 6, NULL, NULL, NULL, NULL, NULL, '', '', '31300', '', '', '5400', '', '', '37900', NULL, NULL, NULL, NULL),
(375, 62, 7, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(376, 62, 9, NULL, NULL, NULL, NULL, NULL, '', '', '40', '', '', '39', '', '', '39', NULL, NULL, NULL, NULL),
(377, 62, 10, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(378, 62, 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL),
(379, 62, 13, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(380, 62, 15, NULL, NULL, NULL, NULL, NULL, '', '', '0.75', '', '', '0.49', '', '', '0.49', NULL, NULL, NULL, NULL),
(381, 62, 16, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(382, 62, 18, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL),
(383, 62, 19, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(384, 62, 21, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, '140', NULL, NULL),
(385, 62, 22, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '144', NULL),
(386, 62, 23, NULL, NULL, NULL, NULL, NULL, '', '', '27', '', '', '27', '', '', '26', NULL, NULL, NULL, NULL),
(387, 62, 24, NULL, NULL, NULL, NULL, NULL, '', '', '33', '', '', '32', '', '', '33', NULL, NULL, NULL, NULL),
(388, 62, 25, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(389, 66, 2, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '5000'),
(390, 66, 3, NULL, NULL, NULL, NULL, NULL, '', '', '56600', '', '', '47600', '', '', '54200', NULL, NULL, NULL, NULL),
(391, 66, 4, NULL, NULL, NULL, NULL, NULL, '', '', '39900', '', '', '33900', '', '', '39400', NULL, NULL, NULL, NULL),
(392, 66, 5, NULL, NULL, NULL, NULL, NULL, '', '', '15800', '', '', '24700', '', '', '59400', NULL, NULL, NULL, NULL),
(393, 66, 6, NULL, NULL, NULL, NULL, NULL, '', '', '24800', '', '', '23500', '', '', '29100', NULL, NULL, NULL, NULL),
(394, 66, 7, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'KYORITSU'),
(395, 66, 9, NULL, NULL, NULL, NULL, NULL, '', '', '40', '', '', '38.8', '', '', '37.3', NULL, NULL, NULL, NULL),
(396, 66, 10, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(397, 66, 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL),
(398, 66, 13, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(399, 66, 15, NULL, NULL, NULL, NULL, NULL, '', '', '70', '', '', '70', '', '', '70', NULL, NULL, NULL, NULL),
(400, 66, 16, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(401, 66, 18, NULL, NULL, NULL, NULL, NULL, '', '', '0.45', '', '', '0.45', '', '', '0.45', NULL, NULL, NULL, NULL),
(402, 66, 19, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(403, 66, 21, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, '198', NULL, NULL),
(404, 66, 22, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '', NULL),
(405, 66, 23, NULL, NULL, NULL, NULL, NULL, '', '', '19.75', '', '', '20.5', '', '', '21.75', NULL, NULL, NULL, NULL),
(406, 66, 24, NULL, NULL, NULL, NULL, NULL, '', '', '30.45', '', '', '36.05', '', '', '35.5', NULL, NULL, NULL, NULL),
(407, 66, 25, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'DIGITAL SIRCUIT BREAKER ANALYSIS (T.6500/VANGUARD)'),
(408, 68, 2, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '5000'),
(409, 68, 3, NULL, NULL, NULL, NULL, NULL, '', '', '688000', '', '', '154000', '', '', '113000', NULL, NULL, NULL, NULL),
(410, 68, 4, NULL, NULL, NULL, NULL, NULL, '', '', '139000', '', '', '164000', '', '', '482000', NULL, NULL, NULL, NULL),
(411, 68, 5, NULL, NULL, NULL, NULL, NULL, '', '', '199000', '', '', '47800', '', '', '44200', NULL, NULL, NULL, NULL),
(412, 68, 6, NULL, NULL, NULL, NULL, NULL, '', '', '57300', '', '', '32400', '', '', '24300', NULL, NULL, NULL, NULL),
(413, 68, 7, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'Kyoritsu 3125'),
(414, 68, 9, NULL, NULL, NULL, NULL, NULL, '', '', '32.7', '', '', '30.3', '', '', '34.3', NULL, NULL, NULL, NULL),
(415, 68, 10, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'Megger DLRO 600'),
(416, 68, 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL),
(417, 68, 13, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(418, 68, 15, NULL, NULL, NULL, NULL, NULL, '', '', '0.75', '', '', '0.75', '', '', '0.5', NULL, NULL, NULL, NULL),
(419, 68, 16, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'Visual (mata)'),
(420, 68, 18, NULL, NULL, NULL, NULL, NULL, '', '', '0.5', '', '', '0.5', '', '', '0.5', NULL, NULL, NULL, NULL),
(421, 68, 19, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'Kyoritsu 4105 A'),
(422, 68, 21, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, '213', NULL, NULL),
(423, 68, 22, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '216', NULL),
(424, 68, 23, NULL, NULL, NULL, NULL, NULL, '', '', '23.7', '', '', '23.8', '', '', '23.55', NULL, NULL, NULL, NULL),
(425, 68, 24, NULL, NULL, NULL, NULL, NULL, '', '', '51.65', '', '', '51.15', '', '', '51.10', NULL, NULL, NULL, NULL),
(426, 68, 25, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'Van Guard');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengujian_pt`
--

CREATE TABLE IF NOT EXISTS `pengujian_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `injeksi_teg1` varchar(255) DEFAULT NULL,
  `injeksi_teg2` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `injeksi_alat_uji` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=560 ;

--
-- Dumping data untuk tabel `pengujian_pt`
--

INSERT INTO `pengujian_pt` (`id`, `id_data_umum`, `id_hasil_uji`, `injeksi_teg1`, `injeksi_teg2`, `fasa_r_std`, `fasa_r_th`, `fasa_r_hu`, `fasa_s_std`, `fasa_s_th`, `fasa_s_hu`, `fasa_t_std`, `fasa_t_th`, `fasa_t_hu`, `injeksi_alat_uji`) VALUES
(527, 60, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(528, 60, 3, NULL, NULL, '', '', '>1000000', '', '', '357000', '', '', '317000', NULL),
(529, 60, 4, NULL, NULL, '', '', '201000', '', '', '4970', '', '', '113000', NULL),
(530, 60, 5, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(531, 60, 6, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '500'),
(532, 60, 7, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '11', NULL),
(533, 60, 8, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(534, 60, 9, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(535, 60, 10, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(536, 60, 12, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(537, 60, 13, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(538, 63, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(539, 63, 3, NULL, NULL, '', '', '564000', '', '', '307000', '', '', '388000', NULL),
(540, 63, 4, NULL, NULL, '', '', '15600', '', '', '15000', '', '', '11700', NULL),
(541, 63, 5, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(542, 63, 6, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '500'),
(543, 63, 7, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(544, 63, 8, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(545, 63, 9, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(546, 63, 10, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(547, 63, 12, NULL, NULL, '', '', '0.3', '', '', '0.3', '', '', '0.3', NULL),
(548, 63, 13, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(549, 67, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(550, 67, 3, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(551, 67, 4, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(552, 67, 5, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(553, 67, 6, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(554, 67, 7, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(555, 67, 8, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(556, 67, 9, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(557, 67, 10, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(558, 67, 12, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(559, 67, 13, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peralatan_ct`
--

CREATE TABLE IF NOT EXISTS `peralatan_ct` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peralatan_ct`
--

INSERT INTO `peralatan_ct` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(1, '1', 'Pentanahan ( Grounding )', NULL, '', '', NULL, ''),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Lemari / Box Terminal', NULL, '', NULL, '', NULL),
(5, '2.1', 'Baut - baut wiring kontrol dan proteksi', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(6, '2.2', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(7, '2.3', 'Heater', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(8, '2.4', 'Lubang Binatang', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(9, '3', 'Bodi dan Bushing', NULL, NULL, NULL, NULL, NULL),
(10, '3.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(11, '3.2', 'Bagian bodi yang lecet / berkarat', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(12, '3.3', 'Bagian yang retak', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(13, '4', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(14, '4.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(15, '4.2', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(16, '5', 'Uji Fungsi Limit Switch', NULL, NULL, NULL, NULL, NULL),
(17, '5.1', 'Indikator', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(18, '5.2', 'alarm Low Pressure SF 6 (CT dengan media SF 6)        ', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(19, '6', 'Pondasi', NULL, NULL, NULL, NULL, NULL),
(20, '6.1', 'Keretakan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(21, '6.2', 'Kemiringan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peralatan_la`
--

CREATE TABLE IF NOT EXISTS `peralatan_la` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peralatan_la`
--

INSERT INTO `peralatan_la` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(1, '1', ' Pentanahan ( Grounding )', NULL, NULL, NULL, NULL, NULL),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Isolator', NULL, NULL, NULL, NULL, NULL),
(5, '2.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(6, '2.2', 'Retak-retak', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(7, '3', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(8, '3.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(9, '3.2', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(10, '4', 'Pondasi', NULL, NULL, NULL, NULL, NULL),
(11, '4.1', 'Keretakan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(12, '4.2', 'Kemiringan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peralatan_pmt`
--

CREATE TABLE IF NOT EXISTS `peralatan_pmt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peralatan_pmt`
--

INSERT INTO `peralatan_pmt` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(1, '1', 'Pentanahan ( Grounding )', NULL, NULL, NULL, NULL, NULL),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Alat Pernapasan dan Ventilasi', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(5, '3', 'Lemari / Box Kontrol', NULL, NULL, NULL, NULL, NULL),
(6, '3.1', 'Baut-baut wiring kontrol dan proteksi', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(7, '3.2', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(8, '3.3', 'Heater', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(9, '3.4', 'Sumber tegangan AC/DC', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(10, '4', 'Bodi dan Bushing', NULL, NULL, NULL, NULL, NULL),
(11, '4.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(12, '4.2', 'Bagian Bodi yang lecet, berkarat', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(13, '4.3', 'Mekanik Penggerak', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(14, '5', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(15, '5.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(16, '5.2', 'Bagian Bodi', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(17, '5.3', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(18, '5.4', 'Baut-baut wiring pada box kontrol', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(19, '6', 'Mekanik Penggerak', NULL, NULL, NULL, NULL, NULL),
(20, '6.1', 'Mekanik  Penggerak', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(21, '6.2', 'Mur Baut', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(22, '6.3', 'Pelumasan pada roda gigi dan pegas ', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(23, '7', 'Pengujian Sistem Hydrolic', NULL, NULL, NULL, NULL, NULL),
(24, '7.1', 'Minyak hydrolic', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(25, '7.2', 'Penunjukan meter-meter', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(26, '7.3', 'Kebocor slang / pipa hydrolic', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(27, '8', 'Percobaan ON / OFF PMT', NULL, NULL, NULL, NULL, NULL),
(28, '8.1', 'Posisi ON', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(29, '8.2', 'Posisi OFF', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(30, '9', 'Pondasi', NULL, NULL, NULL, NULL, NULL),
(31, '9.1', 'Keretakan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(32, '9.2', 'Kemiringan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peralatan_pt`
--

CREATE TABLE IF NOT EXISTS `peralatan_pt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peralatan_pt`
--

INSERT INTO `peralatan_pt` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1, '1', 'Pentanahan ( Grounding )', NULL, '', '', NULL, ''),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Lemari / Box Terminal', NULL, '', NULL, '', NULL),
(5, '2.1', 'Baut - baut wiring kontrol dan proteksi', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(6, '2.2', 'Kebersihan', NULL, 'tidak', 'ya', 'tidak', 'ya'),
(7, '2.3', 'Heater', NULL, 'tidak', 'ya', 'tidak', 'ya'),
(8, '2.4', 'Lubang Binatang', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(9, '3', 'Bodi dan Bushing', NULL, NULL, NULL, NULL, NULL),
(10, '3.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(11, '3.2', 'Bagian bodi yang lecet / berkarat', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(12, '3.3', 'Kaca Penduga', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(13, '4', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(14, '4.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(15, '4.2', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(16, '4.3', 'Baut - baut wiring pada terminal box', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(17, '5', 'Pemeriksaan Fuse dan MCB', '', '', '', '', ''),
(18, '5.1', 'Fuse', '', 'normal', 'abnormal', 'normal', 'abnormal'),
(19, '5.2', 'MCB(CT dengan media SF 6)', '', 'normal', 'abnormal', 'normal', 'abnormal'),
(20, '6', 'Pondasi', '', '', '', '', ''),
(21, '6.1', 'Keretakan', '', 'ada', 'tidak ada', 'ada', 'tidak ada'),
(22, '6.2', 'Kemiringan', '', 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `level`, `password`) VALUES
(1, 'admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 'kasino', NULL, '123456'),
(3, 'indro', NULL, 'asdfgh'),
(4, 'aku', NULL, '10406c1d7b7421b1a56f0d951e952a95'),
(5, 'user1', 'user', 'e10adc3949ba59abbe56e057f20f883e'),
(6, 'user2', 'user', 'e10adc3949ba59abbe56e057f20f883e'),
(7, 'user', 'user', 'e10adc3949ba59abbe56e057f20f883e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
