-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2016 at 06:53 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_pln`
--

-- --------------------------------------------------------

--
-- Table structure for table `catatan`
--

CREATE TABLE IF NOT EXISTS `catatan` (
`id` int(11) NOT NULL,
  `id_data_umum` varchar(255) DEFAULT NULL,
  `uraian_checklist` varchar(255) DEFAULT NULL,
  `uraian_uji` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `catatan`
--

INSERT INTO `catatan` (`id`, `id_data_umum`, `uraian_checklist`, `uraian_uji`) VALUES
(57, '58', '', ''),
(58, '59', NULL, ''),
(59, '60', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `checklist_ct`
--

CREATE TABLE IF NOT EXISTS `checklist_ct` (
`id` int(11) NOT NULL,
  `id_data_umum` int(50) DEFAULT NULL,
  `id_alat` int(50) DEFAULT NULL,
  `k1` varchar(50) DEFAULT NULL,
  `k2` varchar(50) DEFAULT NULL,
  `k3` varchar(50) DEFAULT NULL,
  `k4` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=514 ;

--
-- Dumping data for table `checklist_ct`
--

INSERT INTO `checklist_ct` (`id`, `id_data_umum`, `id_alat`, `k1`, `k2`, `k3`, `k4`) VALUES
(493, 58, 1, NULL, NULL, NULL, NULL),
(494, 58, 2, '1', NULL, '1', NULL),
(495, 58, 3, '1', NULL, '1', NULL),
(496, 58, 4, NULL, NULL, NULL, NULL),
(497, 58, 5, NULL, '1', NULL, '1'),
(498, 58, 6, NULL, '1', NULL, '1'),
(499, 58, 7, '1', NULL, '1', NULL),
(500, 58, 8, NULL, '1', NULL, '1'),
(501, 58, 9, NULL, NULL, NULL, NULL),
(502, 58, 10, NULL, '1', NULL, '1'),
(503, 58, 11, NULL, '1', NULL, '1'),
(504, 58, 12, NULL, '1', NULL, '1'),
(505, 58, 13, NULL, NULL, NULL, NULL),
(506, 58, 14, NULL, '1', NULL, '1'),
(507, 58, 15, NULL, '1', NULL, '1'),
(508, 58, 16, NULL, NULL, NULL, NULL),
(509, 58, 17, NULL, NULL, NULL, NULL),
(510, 58, 18, NULL, NULL, NULL, NULL),
(511, 58, 19, NULL, NULL, NULL, NULL),
(512, 58, 20, '1', NULL, '1', NULL),
(513, 58, 21, '1', NULL, '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `checklist_la`
--

CREATE TABLE IF NOT EXISTS `checklist_la` (
`id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_alat` int(11) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Table structure for table `checklist_pmt`
--

CREATE TABLE IF NOT EXISTS `checklist_pmt` (
`id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_alat` int(11) DEFAULT NULL,
  `k1` varchar(50) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(50) DEFAULT NULL,
  `k4` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=675 ;

--
-- Dumping data for table `checklist_pmt`
--

INSERT INTO `checklist_pmt` (`id`, `id_data_umum`, `id_alat`, `k1`, `k2`, `k3`, `k4`) VALUES
(643, 59, 1, NULL, NULL, NULL, NULL),
(644, 59, 2, '1', NULL, '1', NULL),
(645, 59, 3, '1', NULL, '1', NULL),
(646, 59, 4, NULL, NULL, NULL, NULL),
(647, 59, 5, NULL, NULL, NULL, NULL),
(648, 59, 6, NULL, '1', NULL, '1'),
(649, 59, 7, '1', NULL, NULL, '1'),
(650, 59, 8, '1', NULL, '1', NULL),
(651, 59, 9, '1', NULL, '1', NULL),
(652, 59, 10, NULL, NULL, NULL, NULL),
(653, 59, 11, '1', NULL, NULL, '1'),
(654, 59, 12, NULL, '1', NULL, '1'),
(655, 59, 13, '1', NULL, NULL, '1'),
(656, 59, 14, NULL, NULL, NULL, NULL),
(657, 59, 15, NULL, '1', NULL, '1'),
(658, 59, 16, NULL, '1', NULL, '1'),
(659, 59, 17, NULL, '1', NULL, '1'),
(660, 59, 18, NULL, '1', NULL, '1'),
(661, 59, 19, NULL, NULL, NULL, NULL),
(662, 59, 20, '1', NULL, '1', NULL),
(663, 59, 21, NULL, '1', NULL, '1'),
(664, 59, 22, '1', NULL, '1', NULL),
(665, 59, 23, NULL, NULL, NULL, NULL),
(666, 59, 24, '1', NULL, '1', NULL),
(667, 59, 25, '1', NULL, '1', NULL),
(668, 59, 26, NULL, '1', NULL, '1'),
(669, 59, 27, NULL, NULL, NULL, NULL),
(670, 59, 28, '1', NULL, '1', NULL),
(671, 59, 29, '1', NULL, '1', NULL),
(672, 59, 30, NULL, NULL, NULL, NULL),
(673, 59, 31, NULL, '1', NULL, '1'),
(674, 59, 32, NULL, '1', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `checklist_pt`
--

CREATE TABLE IF NOT EXISTS `checklist_pt` (
`id` int(11) NOT NULL,
  `id_data_umum` int(50) DEFAULT NULL,
  `id_alat` int(50) DEFAULT NULL,
  `k1` varchar(50) DEFAULT NULL,
  `k2` varchar(50) DEFAULT NULL,
  `k3` varchar(50) DEFAULT NULL,
  `k4` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=543 ;

--
-- Dumping data for table `checklist_pt`
--

INSERT INTO `checklist_pt` (`id`, `id_data_umum`, `id_alat`, `k1`, `k2`, `k3`, `k4`) VALUES
(520, 60, 0, NULL, NULL, NULL, NULL),
(521, 60, 1, NULL, NULL, NULL, NULL),
(522, 60, 2, '1', NULL, '1', NULL),
(523, 60, 3, '1', NULL, '1', NULL),
(524, 60, 4, NULL, NULL, NULL, NULL),
(525, 60, 5, NULL, '1', '1', NULL),
(526, 60, 6, NULL, '1', '1', NULL),
(527, 60, 7, '1', NULL, '1', NULL),
(528, 60, 8, NULL, '1', NULL, '1'),
(529, 60, 9, NULL, NULL, NULL, NULL),
(530, 60, 10, NULL, '1', '1', NULL),
(531, 60, 11, NULL, '1', NULL, '1'),
(532, 60, 12, NULL, '1', '1', NULL),
(533, 60, 13, NULL, NULL, NULL, NULL),
(534, 60, 14, NULL, '1', NULL, '1'),
(535, 60, 15, NULL, '1', NULL, '1'),
(536, 60, 16, NULL, '1', NULL, '1'),
(537, 60, 17, NULL, NULL, NULL, NULL),
(538, 60, 18, '1', NULL, '1', NULL),
(539, 60, 19, '1', NULL, '1', NULL),
(540, 60, 20, NULL, NULL, NULL, NULL),
(541, 60, 21, NULL, '1', NULL, '1'),
(542, 60, 22, NULL, '1', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `data_umum`
--

CREATE TABLE IF NOT EXISTS `data_umum` (
`id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `merk` varchar(50) DEFAULT NULL,
  `type_fasa_r` varchar(50) DEFAULT NULL,
  `type_fasa_s` varchar(50) DEFAULT NULL,
  `type_fasa_t` varchar(50) DEFAULT NULL,
  `ratio_core_1` varchar(50) DEFAULT NULL,
  `ratio_core_2` varchar(50) DEFAULT NULL,
  `ratio_core_3` varchar(50) DEFAULT NULL,
  `ratio_core_4` varchar(50) DEFAULT NULL,
  `ratio_core_5` varchar(50) DEFAULT NULL,
  `bay` varchar(50) DEFAULT NULL,
  `lokasi_gi` varchar(50) DEFAULT NULL,
  `jenis_mode_testing` varchar(50) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `data_umum`
--

INSERT INTO `data_umum` (`id`, `id_user`, `nomor`, `tanggal`, `type`, `merk`, `type_fasa_r`, `type_fasa_s`, `type_fasa_t`, `ratio_core_1`, `ratio_core_2`, `ratio_core_3`, `ratio_core_4`, `ratio_core_5`, `bay`, `lokasi_gi`, `jenis_mode_testing`, `tahun`) VALUES
(58, 7, '', '2015-01-04 00:00:00', 'PC 14 S', 'MITSHUBISHI', '3094 02', '3094 03', '3094 04', '600 / 1A', '600 / 1A', '1200 / 1A', '', '', 'KRAPYAK 1', 'SRONDOL', 'CT', '2015'),
(59, 7, '', '2015-02-11 00:00:00', '8665687', 'ABB', 'A1', 'B1', 'C1', NULL, NULL, NULL, NULL, NULL, 'PDLAM 1', 'SRONDOL', 'PMT', '2015'),
(60, 7, '', '2015-02-11 00:00:00', 'PY/2C/14', 'MITSHUBISHI', 'H0T-7-117', '', '', '', '', NULL, NULL, NULL, 'PDLAMP 1', 'SRONDOL', 'PT', '2015');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_uji_ct`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_ct` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` int(11) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_uji_ct`
--

INSERT INTO `hasil_uji_ct` (`id`, `kode`, `pembeda`, `titik_ukur`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, 'A.A.A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1.1', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '', 1, 'Primer - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '', 1, 'Primer - Sek 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '', 1, 'Primer - Sek 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '', 1, 'Primer - Sek 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '', 1, 'Primer - Sek 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '', 1, 'Primer - Sek 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '1.2', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '7', 2, 'Sek. 1 - Sek. 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '', 2, 'Sek. 1 - Sek. 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '', 2, 'Sek. 1 - Sek. 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '', 2, 'Sek. 1 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '', 2, 'Sek. 2 - Sek. 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '', 2, 'Sek. 2 - Sek. 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '', 2, 'Sek. 2 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '', 2, 'Sek. 3 - Sek. 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '', 2, 'Sek. 3 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '', 2, 'Sek. 4 - Sek. 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '', 2, 'Sek. 1 - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '', 2, 'Sek. 2 - Tanah', '', '', '', '', '', '', '', '', ''),
(23, '', 2, 'Sek. 2 - Tanah', '', '', '', '', '', '', '', '', ''),
(24, '', 2, 'Sek. 4 - Tanah', '', '', '', '', '', '', '', '', ''),
(25, '', 2, 'Sek. 5 - Tanah', '', '', '', '', '', '', '', '', ''),
(26, '', 3, 'Alat Uji Yang Digunakan :', '', '', '', '', '', '', '', '', ''),
(27, 'B.B.B', 4, 'Tahanan Pentanahan ', '', '', '', '', '', '', '', '', ''),
(28, '', 5, 'Tahanan Pentanahan', '', '', '', '', '', '', '', '', ''),
(29, '', 6, 'Alat Uji Yang Digunakan :', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_uji_la`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_la` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` int(11) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_uji_la`
--

INSERT INTO `hasil_uji_la` (`id`, `kode`, `pembeda`, `titik_ukur`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, '', 1, 'Tahanan Isolasi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1', 1, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1.1', 1, 'Atas - Bawah ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '1.2', 1, 'Atas - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '1.3', 1, 'Bawah - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '10', 1, 'Alat Uji Yang Digunakan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '2', 2, 'Tahanan Petanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '2.1', 2, 'Tahanan Petanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '', 2, 'Alat Uji Yang Digunakan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '3', 3, 'Counter Arrester', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '', 3, 'Penunjukan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hasil_uji_pmt`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_pmt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` varchar(255) NOT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `tu_sambung` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_uji_pmt`
--

INSERT INTO `hasil_uji_pmt` (`id`, `kode`, `pembeda`, `titik_ukur`, `tu_sambung`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, '1', '', 'Tahanan Isolasi', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, '1', 'Megger', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1.1', '1', 'Atas - Bawah                          PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '1.2', '1', 'Atas - Tanah                           PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '1.3', '1', 'Bawah - Tanah                       PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '1.4', '1', 'Fasa - Tanah                           PMT ON', 'PMT ON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '10', '1', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '2', '2', 'Tahanan Kontak ( mW )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '', '2', 'Atas - Bawah                         PMT OFF', 'PMT OFF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '10', '2', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '3', '3', 'Tegangan Tembus Minyak ( kV / 2,5 mm )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, '3', 'Minyak PMT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '10', '3', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '4', '4', 'Tekanan Gas SF 6 ( Mpa ), ( Bar ), ( Psi ) ( kg/cm2)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '', '4', 'Pressure Gauge ( Visual )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '10', '4', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '5', '5', 'Tahanan Pentanahan ( W )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '', '5', 'Tahanan Pentanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '10', '5', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '6', '6', 'Pengujian Keserempakan / Breaker ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '', '6', 'Counter PMT Sebelum Pengujian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '', '6', 'Counter PMT Setelah Pengujian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '6.1', '6', 'OPEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '6.2', '6', 'CLOSE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '10', '6', 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '7', '7', 'Print - Out  /  Breaker Analyzer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hasil_uji_pt`
--

CREATE TABLE IF NOT EXISTS `hasil_uji_pt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `pembeda` int(11) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `std1` varchar(255) DEFAULT NULL,
  `tahun1` varchar(255) DEFAULT NULL,
  `hasil_ukur1` varchar(255) DEFAULT NULL,
  `std2` varchar(255) DEFAULT NULL,
  `tahun2` varchar(255) DEFAULT NULL,
  `hasil_ukur2` varchar(255) DEFAULT NULL,
  `std3` varchar(255) DEFAULT NULL,
  `tahun3` varchar(255) DEFAULT NULL,
  `hasil_ukur3` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_uji_pt`
--

INSERT INTO `hasil_uji_pt` (`id`, `kode`, `pembeda`, `titik_ukur`, `std1`, `tahun1`, `hasil_ukur1`, `std2`, `tahun2`, `hasil_ukur2`, `std3`, `tahun3`, `hasil_ukur3`) VALUES
(1, 'A.A.A', NULL, 'Tahanan  Isolasi belitan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1.1', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1.1', 1, 'Primer - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '1.2', 1, 'Primer - Sekunder (1a)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '1.3', 1, 'Primer - Sekunder (2a)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '1.2', NULL, 'Injeksi Teg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '1.4', 2, 'Sekunder 1a - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '1.5', 2, 'Sekunder 2a - Tanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '1.6', 2, 'Sekunder 1a - 2a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '', 3, 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'B.B.B', 4, 'Tahanan Pentanahan ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '', 5, 'Tahanan Pentanahan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '', 6, 'Alat Uji Yang Digunakan :', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `penanggung_jawab`
--

CREATE TABLE IF NOT EXISTS `penanggung_jawab` (
`id` int(11) NOT NULL,
  `pelaksana` varchar(50) DEFAULT NULL,
  `pengawas` varchar(50) DEFAULT NULL,
  `id_data_umum` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=166 ;

--
-- Dumping data for table `penanggung_jawab`
--

INSERT INTO `penanggung_jawab` (`id`, `pelaksana`, `pengawas`, `id_data_umum`) VALUES
(157, 'Martono', 'Ami W', 58),
(158, 'Taufan', NULL, 58),
(159, 'Febrian', NULL, 58),
(160, 'Topan', 'Abdurrahman', 59),
(161, 'Kusnadi', NULL, 59),
(162, 'Darminto', NULL, 59),
(163, 'Prasetyo', 'Abdurrahman', 60),
(164, 'Kusnadi', NULL, 60),
(165, '', NULL, 60);

-- --------------------------------------------------------

--
-- Table structure for table `pengujian_ct`
--

CREATE TABLE IF NOT EXISTS `pengujian_ct` (
`id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `injeksi_teg1` varchar(255) DEFAULT NULL,
  `injeksi_teg2` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `injeksi_alat_uji` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=572 ;

--
-- Dumping data for table `pengujian_ct`
--

INSERT INTO `pengujian_ct` (`id`, `id_data_umum`, `id_hasil_uji`, `injeksi_teg1`, `injeksi_teg2`, `fasa_r_std`, `fasa_r_th`, `fasa_r_hu`, `fasa_s_std`, `fasa_s_th`, `fasa_s_hu`, `fasa_t_std`, `fasa_t_th`, `fasa_t_hu`, `injeksi_alat_uji`) VALUES
(546, 58, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(547, 58, 3, NULL, NULL, '', '', '81800', '', '', '73900', '', '', '64500', NULL),
(548, 58, 4, NULL, NULL, '', '', '64900', '', '', '79400', '', '', '47400', NULL),
(549, 58, 5, NULL, NULL, '', '', '81400', '', '', '54800', '', '', '45900', NULL),
(550, 58, 6, NULL, NULL, '', '', '78900', '', '', '34600', '', '', '57400', NULL),
(551, 58, 7, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(552, 58, 8, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(553, 58, 9, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '500'),
(554, 58, 10, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(555, 58, 11, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(556, 58, 12, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(557, 58, 13, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(558, 58, 14, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(559, 58, 15, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(560, 58, 16, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(561, 58, 17, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(562, 58, 18, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(563, 58, 19, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(564, 58, 20, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(565, 58, 22, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(566, 58, 23, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>1000', NULL),
(567, 58, 24, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(568, 58, 25, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(569, 58, 26, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(570, 58, 28, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(571, 58, 29, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `pengujian_la`
--

CREATE TABLE IF NOT EXISTS `pengujian_la` (
`id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `jenis_uji` varchar(255) DEFAULT NULL,
  `injeksi_teg` varchar(255) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `fasa_r_sebelum` varchar(255) DEFAULT NULL,
  `fasa_r_sesudah` varchar(255) DEFAULT NULL,
  `fasa_s_sebelum` varchar(255) DEFAULT NULL,
  `fasa_s_sesudah` varchar(255) DEFAULT NULL,
  `fasa_t_sebelum` varchar(255) DEFAULT NULL,
  `fasa_t_sesudah` varchar(255) DEFAULT NULL,
  `catatan` text
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Table structure for table `pengujian_pmt`
--

CREATE TABLE IF NOT EXISTS `pengujian_pmt` (
`id` int(11) NOT NULL,
  `id_data_umum` int(255) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `jenis_uji` varchar(255) DEFAULT NULL,
  `tahanan_isolasi` varchar(255) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `alat_uji` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `catatan` text,
  `pmt_sebelum` varchar(255) DEFAULT NULL,
  `pmt_sesudah` varchar(255) DEFAULT NULL,
  `meger_alat_uji` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=370 ;

--
-- Dumping data for table `pengujian_pmt`
--

INSERT INTO `pengujian_pmt` (`id`, `id_data_umum`, `id_hasil_uji`, `kode`, `jenis_uji`, `tahanan_isolasi`, `titik_ukur`, `alat_uji`, `fasa_r_std`, `fasa_r_th`, `fasa_r_hu`, `fasa_s_std`, `fasa_s_th`, `fasa_s_hu`, `fasa_t_std`, `fasa_t_th`, `fasa_t_hu`, `catatan`, `pmt_sebelum`, `pmt_sesudah`, `meger_alat_uji`) VALUES
(351, 59, 2, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '5000'),
(352, 59, 3, NULL, NULL, NULL, NULL, NULL, '', '', '87300', '', '', '24000', '', '', '182000', NULL, NULL, NULL, NULL),
(353, 59, 4, NULL, NULL, NULL, NULL, NULL, '', '', '30300', '', '', '62800', '', '', '329000', NULL, NULL, NULL, NULL),
(354, 59, 5, NULL, NULL, NULL, NULL, NULL, '', '', '151000', '', '', '30100', '', '', '166000', NULL, NULL, NULL, NULL),
(355, 59, 6, NULL, NULL, NULL, NULL, NULL, '', '', '31300', '', '', '5400', '', '', '37900', NULL, NULL, NULL, NULL),
(356, 59, 7, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(357, 59, 9, NULL, NULL, NULL, NULL, NULL, '', '', '40', '', '', '39', '', '', '39', NULL, NULL, NULL, NULL),
(358, 59, 10, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(359, 59, 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL),
(360, 59, 13, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(361, 59, 15, NULL, NULL, NULL, NULL, NULL, '', '', '0.75', '', '', '0.49', '', '', '0.49', NULL, NULL, NULL, NULL),
(362, 59, 16, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(363, 59, 18, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL),
(364, 59, 19, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, ''),
(365, 59, 21, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, '140', NULL, NULL),
(366, 59, 22, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, '144', NULL),
(367, 59, 23, NULL, NULL, NULL, NULL, NULL, '', '', '27', '', '', '27', '', '', '26', NULL, NULL, NULL, NULL),
(368, 59, 24, NULL, NULL, NULL, NULL, NULL, '', '', '33', '', '', '32', '', '', '33', NULL, NULL, NULL, NULL),
(369, 59, 25, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `pengujian_pt`
--

CREATE TABLE IF NOT EXISTS `pengujian_pt` (
`id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `id_hasil_uji` int(11) DEFAULT NULL,
  `injeksi_teg1` varchar(255) DEFAULT NULL,
  `injeksi_teg2` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `injeksi_alat_uji` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=538 ;

--
-- Dumping data for table `pengujian_pt`
--

INSERT INTO `pengujian_pt` (`id`, `id_data_umum`, `id_hasil_uji`, `injeksi_teg1`, `injeksi_teg2`, `fasa_r_std`, `fasa_r_th`, `fasa_r_hu`, `fasa_s_std`, `fasa_s_th`, `fasa_s_hu`, `fasa_t_std`, `fasa_t_th`, `fasa_t_hu`, `injeksi_alat_uji`) VALUES
(527, 60, 2, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '5000'),
(528, 60, 3, NULL, NULL, '', '', '564000', '', '', '307000', '', '', '388000', NULL),
(529, 60, 4, NULL, NULL, '', '', '15600', '', '', '15000', '', '', '11700', NULL),
(530, 60, 5, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(531, 60, 6, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '500'),
(532, 60, 7, NULL, NULL, '', '', '>1000', '', '', '>1000', '', '', '>810', NULL),
(533, 60, 8, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(534, 60, 9, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL),
(535, 60, 10, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, ''),
(536, 60, 12, NULL, NULL, '', '', '0.3', '', '', '0.3', '', '', '0.3', NULL),
(537, 60, 13, NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `peralatan_ct`
--

CREATE TABLE IF NOT EXISTS `peralatan_ct` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peralatan_ct`
--

INSERT INTO `peralatan_ct` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(1, '1', 'Pentanahan ( Grounding )', NULL, '', '', NULL, ''),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Lemari / Box Terminal', NULL, '', NULL, '', NULL),
(5, '2.1', 'Baut - baut wiring kontrol dan proteksi', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(6, '2.2', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(7, '2.3', 'Heater', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(8, '2.4', 'Lubang Binatang', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(9, '3', 'Bodi dan Bushing', NULL, NULL, NULL, NULL, NULL),
(10, '3.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(11, '3.2', 'Bagian bodi yang lecet / berkarat', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(12, '3.3', 'Bagian yang retak', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(13, '4', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(14, '4.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(15, '4.2', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(16, '5', 'Uji Fungsi Limit Switch', NULL, NULL, NULL, NULL, NULL),
(17, '5.1', 'Indikator', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(18, '5.2', 'alarm Low Pressure SF 6 (CT dengan media SF 6)        ', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(19, '6', 'Pondasi', NULL, NULL, NULL, NULL, NULL),
(20, '6.1', 'Keretakan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(21, '6.2', 'Kemiringan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Table structure for table `peralatan_la`
--

CREATE TABLE IF NOT EXISTS `peralatan_la` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peralatan_la`
--

INSERT INTO `peralatan_la` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(1, '1', ' Pentanahan ( Grounding )', NULL, NULL, NULL, NULL, NULL),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Isolator', NULL, NULL, NULL, NULL, NULL),
(5, '2.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(6, '2.2', 'Retak-retak', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(7, '3', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(8, '3.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(9, '3.2', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(10, '4', 'Pondasi', NULL, NULL, NULL, NULL, NULL),
(11, '4.1', 'Keretakan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(12, '4.2', 'Kemiringan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Table structure for table `peralatan_pmt`
--

CREATE TABLE IF NOT EXISTS `peralatan_pmt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peralatan_pmt`
--

INSERT INTO `peralatan_pmt` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(1, '1', 'Pentanahan ( Grounding )', NULL, NULL, NULL, NULL, NULL),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Alat Pernapasan dan Ventilasi', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(5, '3', 'Lemari / Box Kontrol', NULL, NULL, NULL, NULL, NULL),
(6, '3.1', 'Baut-baut wiring kontrol dan proteksi', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(7, '3.2', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(8, '3.3', 'Heater', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(9, '3.4', 'Sumber tegangan AC/DC', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(10, '4', 'Bodi dan Bushing', NULL, NULL, NULL, NULL, NULL),
(11, '4.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(12, '4.2', 'Bagian Bodi yang lecet, berkarat', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(13, '4.3', 'Mekanik Penggerak', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(14, '5', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(15, '5.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(16, '5.2', 'Bagian Bodi', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(17, '5.3', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(18, '5.4', 'Baut-baut wiring pada box kontrol', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(19, '6', 'Mekanik Penggerak', NULL, NULL, NULL, NULL, NULL),
(20, '6.1', 'Mekanik  Penggerak', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(21, '6.2', 'Mur Baut', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(22, '6.3', 'Pelumasan pada roda gigi dan pegas ', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(23, '7', 'Pengujian Sistem Hydrolic', NULL, NULL, NULL, NULL, NULL),
(24, '7.1', 'Minyak hydrolic', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(25, '7.2', 'Penunjukan meter-meter', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(26, '7.3', 'Kebocor slang / pipa hydrolic', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(27, '8', 'Percobaan ON / OFF PMT', NULL, NULL, NULL, NULL, NULL),
(28, '8.1', 'Posisi ON', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(29, '8.2', 'Posisi OFF', NULL, 'normal', 'abnormal', 'normal', 'abnormal'),
(30, '9', 'Pondasi', NULL, NULL, NULL, NULL, NULL),
(31, '9.1', 'Keretakan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(32, '9.2', 'Kemiringan', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Table structure for table `peralatan_pt`
--

CREATE TABLE IF NOT EXISTS `peralatan_pt` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `k1` varchar(255) DEFAULT NULL,
  `k2` varchar(255) DEFAULT NULL,
  `k3` varchar(255) DEFAULT NULL,
  `k4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peralatan_pt`
--

INSERT INTO `peralatan_pt` (`id`, `kode`, `nama_alat`, `type`, `k1`, `k2`, `k3`, `k4`) VALUES
(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1, '1', 'Pentanahan ( Grounding )', NULL, '', '', NULL, ''),
(2, '1.1', 'Kawat pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(3, '1.2', 'Terminal pentanahan', NULL, 'baik', 'tidak baik', 'baik', 'tidak baik'),
(4, '2', 'Lemari / Box Terminal', NULL, '', NULL, '', NULL),
(5, '2.1', 'Baut - baut wiring kontrol dan proteksi', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(6, '2.2', 'Kebersihan', NULL, 'tidak', 'ya', 'tidak', 'ya'),
(7, '2.3', 'Heater', NULL, 'tidak', 'ya', 'tidak', 'ya'),
(8, '2.4', 'Lubang Binatang', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(9, '3', 'Bodi dan Bushing', NULL, NULL, NULL, NULL, NULL),
(10, '3.1', 'Kebersihan', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(11, '3.2', 'Bagian bodi yang lecet / berkarat', NULL, 'ada', 'tidak ada', 'ada', 'tidak ada'),
(12, '3.3', 'Kaca Penduga', NULL, 'kotor', 'bersih', 'kotor', 'bersih'),
(13, '4', 'Kekencangan Baut', NULL, NULL, NULL, NULL, NULL),
(14, '4.1', 'Terminal Utama', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(15, '4.2', 'Pentanahan', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(16, '4.3', 'Baut - baut wiring pada terminal box', NULL, 'longgar', 'kencang', 'longgar', 'kencang'),
(17, '5', 'Pemeriksaan Fuse dan MCB', '', '', '', '', ''),
(18, '5.1', 'Fuse', '', 'normal', 'abnormal', 'normal', 'abnormal'),
(19, '5.2', 'MCB(CT dengan media SF 6)', '', 'normal', 'abnormal', 'normal', 'abnormal'),
(20, '6', 'Pondasi', '', '', '', '', ''),
(21, '6.1', 'Keretakan', '', 'ada', 'tidak ada', 'ada', 'tidak ada'),
(22, '6.2', 'Kemiringan', '', 'ada', 'tidak ada', 'ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `level`, `password`) VALUES
(1, 'dono', NULL, '123456'),
(2, 'kasino', NULL, '123456'),
(3, 'indro', NULL, 'asdfgh'),
(4, 'aku', NULL, '10406c1d7b7421b1a56f0d951e952a95'),
(5, 'user1', 'user', 'e10adc3949ba59abbe56e057f20f883e'),
(6, 'user2', 'user', 'e10adc3949ba59abbe56e057f20f883e'),
(7, 'user', 'user', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catatan`
--
ALTER TABLE `catatan`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_ct`
--
ALTER TABLE `checklist_ct`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_la`
--
ALTER TABLE `checklist_la`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_pmt`
--
ALTER TABLE `checklist_pmt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_pt`
--
ALTER TABLE `checklist_pt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_umum`
--
ALTER TABLE `data_umum`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_uji_ct`
--
ALTER TABLE `hasil_uji_ct`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_uji_la`
--
ALTER TABLE `hasil_uji_la`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_uji_pmt`
--
ALTER TABLE `hasil_uji_pmt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_uji_pt`
--
ALTER TABLE `hasil_uji_pt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penanggung_jawab`
--
ALTER TABLE `penanggung_jawab`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengujian_ct`
--
ALTER TABLE `pengujian_ct`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengujian_la`
--
ALTER TABLE `pengujian_la`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengujian_pmt`
--
ALTER TABLE `pengujian_pmt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengujian_pt`
--
ALTER TABLE `pengujian_pt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peralatan_ct`
--
ALTER TABLE `peralatan_ct`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peralatan_la`
--
ALTER TABLE `peralatan_la`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peralatan_pt`
--
ALTER TABLE `peralatan_pt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catatan`
--
ALTER TABLE `catatan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `checklist_ct`
--
ALTER TABLE `checklist_ct`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=514;
--
-- AUTO_INCREMENT for table `checklist_la`
--
ALTER TABLE `checklist_la`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `checklist_pmt`
--
ALTER TABLE `checklist_pmt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=675;
--
-- AUTO_INCREMENT for table `checklist_pt`
--
ALTER TABLE `checklist_pt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=543;
--
-- AUTO_INCREMENT for table `data_umum`
--
ALTER TABLE `data_umum`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `penanggung_jawab`
--
ALTER TABLE `penanggung_jawab`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT for table `pengujian_ct`
--
ALTER TABLE `pengujian_ct`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=572;
--
-- AUTO_INCREMENT for table `pengujian_la`
--
ALTER TABLE `pengujian_la`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `pengujian_pmt`
--
ALTER TABLE `pengujian_pmt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=370;
--
-- AUTO_INCREMENT for table `pengujian_pt`
--
ALTER TABLE `pengujian_pt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=538;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
