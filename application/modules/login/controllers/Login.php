<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->library('form_validation');
    }

    function index() {
        $this->load->view('v_login');
    }

    function aksi_login() {
//        $this->form_validation->set_rules('username', 'username', 'required');
//        $this->form_validation->set_rules('password', 'password', 'required');
//        if ($this->form_validation->run() != false) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $where = array(
                'username' => $username,
                'password' => $password
            );
//            print_r($where);
//        die();
            $cek = $this->Login_m->view("user", $where);
//            print_r($cek);
//        die();
            if (!empty($cek)) {

                $data_session = array(
                    'username' => $username,
                    'status' => "login",
                );

                $this->session->set_userdata($data_session);
                print_r_pre($data_session);
                redirect(base_url("welcome/menu"));
            } else {
                echo "Username dan password salah !";
            }
//        } else {
//            $this->dataTema = 'welcome_message';
//            $this->noview();
//        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

}
