<div class="gallery-fix"></div> 
<div class="header header-light">
    <a href="#" class="open-menu"><i class="fa fa-navicon"></i></a>
    <h3><?php echo $judul; ?></h3>
    <a href="#" onclick="window.history.go(-1);
            return false;" title="Back"><i class="fa fa-chevron-left"></i></a>
</div>
<div class="navigation navigation-light">
    <div class="navigation-scroll">
        <a href="<?php echo base_url('welcome/menu'); ?>" class="menu-item"><i class="fa fa-home"></i><em>Main Menu</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/1'); ?>" class="menu-item"><i class="fa fa-building"></i><em>Tentang Kami</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/2'); ?>" class="menu-item"><i class="fa fa-cube"></i><em>Panduan</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/3'); ?>" class="menu-item"><i class="fa fa-phone"></i><em>Kontak</em><i class="fa fa-circle"></i></a>
        <a href="#" class="menu-item close-menu"><i class="fa fa-times-circle"></i><em>Close</em><i class="fa fa-circle"></i></a>
    </div>
</div>
<div id="page-content" class="bg-1">
    <div id="page-content-scroll">
        <h3 class="header-mask header-mask-light">Welcome</h3>
        <div class="content-strip">
            <div class="strip-content">
                <h4>Daftar Data CT</h4>
                <p>Sistem Informasi Hasil Pengujian</p>
                <i class="fa fa-cogs"></i>
            </div>
            <div class="overlay"></div>
            <img class="preload-image" data-original="<?php echo image() ?>/pictures/9.jpg" alt="img">
        </div>
        <div class="content">
            <a href="<?php echo site_url('user/tambah'); ?>" class="button button-green button-round button-fullscreen" title="Tambah"><i class="fa fa-plus"></i> Tambah</a>
            <div class="decoration"></div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" width="100%" border="1">
                    <thead>
                        <tr>
                            <th width='5%' style="vertical-align: middle" class="text-center">No</th>
                            <th style="vertical-align: middle" class="text-center">Merk</th>
                            <th style="vertical-align: middle" class="text-center">Type</th>
                            <th style="vertical-align: middle" class="text-center">Lokasi</th>                                
                            <th width='20%' style="vertical-align: middle" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($datanya as $value) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $no; ?></td>
                                <td><?php echo $value->nama; ?></td>
                                <td><?php echo $value->password; ?></td>
                                <td><?php echo $value->level; ?></td>
                                <td class="text-center">                                                                            

                                        <!--<a href="<?php // echo site_url('mode_ct/ct/edit/'.$value->id);   ?>" class="button button-green button-round" title="Edit"><i class="fa fa-pencil"></i> Edit</a>-->
                                        <!--<a href="<?php // echo site_url('mode_ct/ct/excel/' . $value->id);  ?>" class="button button-red button-round" title="Download"><i class="fa fa-download"></i></i> Download</a>-->                                                                                                     
                                    <a href="<?php echo site_url('user/delete/' . $value->id); ?>" class="button button-red button-round" title="Download"><i class="fa fa-close"></i></a>                                                                                                     

                                </td>
                            </tr>
                            <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="footer">
            <p class="footer-strip">Copyright <span id="copyright-year"></span> Silvanix.com. All Rights Reserved</p>
        </div>
        <div class="footer-clear"></div>
    </div>
</div>
<a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i>Back to top</a>