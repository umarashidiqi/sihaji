<?php

class Ct_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_data_daftar($jenis, $gi, $bay) {
        $sql = "SELECT * FROM data_umum WHERE jenis_mode_testing='$jenis' and bay ='$bay' and lokasi_gi='$gi' ORDER BY tahun DESC;";
        return $this->db->query($sql)->result();
    }

    function get_unit($id) {
        $sql = "SELECT * FROM kr_organisasi";
        $sql.= ($id) ? " WHERE parent_id = '$id'" : '';
        $sql.= " ORDER BY org_key";
        return $this->db->query($sql)->result();
    }

    function insert($tablename, $data1) {
        return ($this->db->insert($tablename, $data1)) ? $this->db->insert_id() : false;
    }

    function insert_batch($tablename, $data) {
        return $this->db->insert_batch($tablename, $data);
    }

    function update($tablename, $where, $kolom) {
        $this->db->where($where);
        return $this->db->update($tablename, $kolom);
    }


    function delete($tablename, $where) {
        $this->db->where($where);
        return $this->db->delete($tablename);
    }

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function single_view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->row();
    }

    public function detail_organisasi($orgkey) {
        $sql = "select unit_kerja_simpeg unit, parent dinas from kr_organisasi WHERE org_key =" . $orgkey;
        return $this->db->query($sql)->row();
    }

    function get_data_edit($uk_data) {
        $sql = "SELECT
	checklist_ct.id,
	checklist_ct.id_alat,
	checklist_ct.id_data_umum,
	
	checklist_ct.k1,
	checklist_ct.k2,
	checklist_ct.k3,
	checklist_ct.k4,
	peralatan_ct.kode,
	peralatan_ct.nama_alat,
	peralatan_ct.k1 AS alat1,
	peralatan_ct.k2 AS alat2,
	peralatan_ct.k3 AS alat3,
	peralatan_ct.k4 AS alat4
FROM
	checklist_ct
JOIN peralatan_ct ON checklist_ct.id_alat = peralatan_ct.id
WHERE checklist_ct.id_data_umum=$uk_data";
        return $this->db->query($sql)->result();
    }

    function get_data_uji($uk_data) {
        $sql = "SELECT
hasil_uji_ct.id,
hasil_uji_ct.kode,
hasil_uji_ct.pembeda,
hasil_uji_ct.titik_ukur,
pengujian_ct.id as id_pengujian, 
pengujian_ct.injeksi_alat_uji, 
pengujian_ct.fasa_r_hu, 
pengujian_ct.fasa_s_hu, 
pengujian_ct.fasa_t_hu,
pengujian_ct.fasa_r_th,
pengujian_ct.fasa_s_th,
pengujian_ct.fasa_t_th
FROM
	pengujian_ct
JOIN hasil_uji_ct ON pengujian_ct.id_hasil_uji = hasil_uji_ct.id
WHERE pengujian_ct.id_data_umum=$uk_data";
        return $this->db->query($sql)->result();
    }

}
