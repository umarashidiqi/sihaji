<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ct extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Ct_m'));
    }

    public function index() {
        $this->valid_login();
        $this->data['judul'] = "Menu Data CT";
        $gi_option = session('gi_option');
        $bay_option = session('bay_option');
        $this->data['datanya'] = $this->Ct_m->get_data_daftar('CT', $gi_option, $bay_option);
//        print_r($this->data['datanya']);
//        die();
        $this->content = 'daftar';
        $this->view_m();
    }

    public function tambah() {
        $this->valid_login();
        $this->data['action'] = base_url('mode_ct/ct/tambah_proses');
        $this->data['judul'] = "Tambah Data CT ";
        $this->data['sub_judul'] = "Form Tambah Data CT ";
        $this->data['data_alat'] = $this->Ct_m->get('peralatan_ct');
        $this->data['hasil_uji'] = $this->Ct_m->get('hasil_uji_ct');
        $this->content = 'form';
        $this->view();
    }

    public function edit($id) {
        $this->data['action'] = base_url('mode_ct/ct/proses_edit');
        $this->data['judul'] = "Edit Data CT ";
        $this->data['data_edit'] = $this->Ct_m->get_data_edit($id);
        $this->data['data_umum'] = $this->Ct_m->view('data_umum', array('id' => $id));
        $this->data['id_data_umum'] = $this->data['data_umum'][0]->id;
//        print_r_pre($this->data['data_umum']);
        $this->data['data_uji'] = $this->Ct_m->get_data_uji($id);
//        print_r_pre($this->data['data_uji']);
        $this->data['catatan'] = $this->Ct_m->view('catatan', array('id_data_umum' => $id));
        $this->data['penanggung_jawab'] = $this->Ct_m->view('penanggung_jawab', array('id_data_umum' => $id));
//        print_r_pre($this->data['penanggung_jawab']);
        $this->data['data_alat'] = $this->Ct_m->get('peralatan_ct');
        $this->data['hasil_uji'] = $this->Ct_m->get('hasil_uji_ct');
//        $this->data['data_edit'] = $this->Ct_m->get_data_edit(6);

        $this->content = 'form_edit';
        $this->view();
    }

    public function cetak($id) {
        $data['data_edit'] = $this->Ct_m->get_data_edit($id);
        $data['data_umum'] = $this->Ct_m->view('data_umum', array('id' => $id));
        $data['data_uji'] = $this->Ct_m->get_data_uji($id);
        $data['catatan'] = $this->Ct_m->view('catatan', array('id_data_umum' => $id));
        $data['penanggung_jawab'] = $this->Ct_m->view('penanggung_jawab', array('id_data_umum' => $id));

        $data['data_alat'] = $this->Ct_m->get('peralatan_ct');
        $data['hasil_uji'] = $this->Ct_m->get('hasil_uji_ct');

        $this->load->view('cetak', $data);
        $html = $this->output->get_output();
        generate_pdf($html);
    }

    public function tambah_proses() {
        $this->valid_login();
        $rule = array(
            array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required'),
            array('field' => 'tanggal', 'label' => 'Tanggal', 'rules' => 'required'),
            array('field' => 'uraian', 'label' => 'Uraian', 'rules' => 'required')
        );
        $input_data_umum = array(
            'id_user' => session('id_user'),
            'nomor' => $this->input->post('nomor'),
            'tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
            'merk' => $this->input->post('merk'),
            'type' => $this->input->post('type'),
            'type_fasa_r' => $this->input->post('fasa_r'),
            'type_fasa_s' => $this->input->post('fasa_s'),
            'type_fasa_t' => $this->input->post('fasa_t'),
            'ratio_core_1' => $this->input->post('ratio_1'),
            'ratio_core_2' => $this->input->post('ratio_2'),
            'ratio_core_3' => $this->input->post('ratio_3'),
            'ratio_core_4' => $this->input->post('ratio_4'),
            'ratio_core_5' => $this->input->post('ratio_5'),
            'bay' => $this->input->post('bay'),
            'lokasi_gi' => $this->input->post('lokasi_gi'),
            'jenis_mode_testing' => 'CT',
            'tahun' => $this->input->post('tahun')
        );
        if ($id = $this->Ct_m->insert('data_umum', $input_data_umum)) {
            $uk_alat = $this->input->post('idnyak');
            foreach ($uk_alat as $value) {
                $checklist = array(
                    'id_data_umum' => $id,
                    'id_alat' => $value,
                    'k1' => $this->input->post('k1_' . $value),
                    'k2' => $this->input->post('k2_' . $value),
                    'k3' => $this->input->post('k3_' . $value),
                    'k4' => $this->input->post('k4_' . $value)
                );
                $this->Ct_m->insert('checklist_ct', $checklist);
            }

            $input_catatan = array(
                'id_data_umum' => $id,
                'uraian_checklist' => $this->input->post('catatan1'),
                'uraian_uji' => $this->input->post('catatan2')
            );
            $this->Ct_m->insert('catatan', $input_catatan);
            $hasil_ukurnya = $this->input->post('hasil_ukurnya');
            foreach ($hasil_ukurnya as $value) {
                $input_hasiluji = array(
                    'id_data_umum' => $id,
                    'id_hasil_uji' => $value,
                    'fasa_r_std' => '',
                    'fasa_r_th' => $this->input->post('tu1_' . $value),
                    'fasa_r_hu' => $this->input->post('hu1_' . $value),
                    'fasa_s_std' => '',
                    'fasa_s_th' => $this->input->post('tu2_' . $value),
                    'fasa_s_hu' => $this->input->post('hu2_' . $value),
                    'fasa_t_std' => '',
                    'fasa_t_th' => $this->input->post('tu3_' . $value),
                    'fasa_t_hu' => $this->input->post('hu3_' . $value),
                    'injeksi_alat_uji' => $this->input->post('injeksi_' . $value)
                );
                $this->Ct_m->insert('pengujian_ct', $input_hasiluji);
            }
            for ($i = 1; $i < 4; $i++) {
                $pj = array(
                    'id_data_umum' => $id,
                    'pelaksana' => $this->input->post('pelaksana_' . $i),
                    'pengawas' => $this->input->post('pengawas_' . $i)
                );
                $this->Ct_m->insert('penanggung_jawab', $pj);
            }
        }
        redirect(base_url('mode_ct/ct'));
    }

    function hapus($id) {
        //hapus data umu
        $this->Ct_m->delete('checklist_ct', array('id' => $id));
        $this->Ct_m->delete('catatan', array('id' => $id));
        $this->Ct_m->delete('pengujian_ct', array('id' => $id));
        $this->Ct_m->delete('penanggung_jawab', array('id' => $id));
        $this->Ct_m->delete('data_umum', array('id' => $id));
    }

    public function proses_edit() {
        $this->valid_login();
        $rule = array(
            array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required'),
            array('field' => 'tanggal', 'label' => 'Tanggal', 'rules' => 'required'),
            array('field' => 'uraian', 'label' => 'Uraian', 'rules' => 'required')
        );
        $ide = $this->input->post('id_data_umum');
        //catatan,ceklist,pengujian update
        if ($this->Ct_m->delete('checklist_ct', array('id_data_umum' => $ide)) and $this->Ct_m->delete('pengujian_ct', array('id_data_umum' => $ide)) and $this->Ct_m->delete('catatan', array('id_data_umum' => $ide)) and $this->Ct_m->delete('penanggung_jawab', array('id_data_umum' => $ide))) {
            $input_data_umum = array(
                'id_user' => session('id_user'),
                'nomor' => $this->input->post('nomor'),
                'tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
                'merk' => $this->input->post('merk'),
                'type' => $this->input->post('type'),
                'type_fasa_r' => $this->input->post('fasa_r'),
                'type_fasa_s' => $this->input->post('fasa_s'),
                'type_fasa_t' => $this->input->post('fasa_t'),
                'ratio_core_1' => $this->input->post('ratio_1'),
                'ratio_core_2' => $this->input->post('ratio_2'),
                'ratio_core_3' => $this->input->post('ratio_3'),
                'ratio_core_4' => $this->input->post('ratio_4'),
                'ratio_core_5' => $this->input->post('ratio_5'),
                'bay' => $this->input->post('bay'),
                'lokasi_gi' => $this->input->post('lokasi_gi'),
                'jenis_mode_testing' => 'CT',
                'tahun' => $this->input->post('tahun')
            );
            if ($this->Ct_m->update('data_umum', array('id' => $ide), $input_data_umum)) {
                $uk_alat = $this->input->post('idnyak');
                foreach ($uk_alat as $value) {
                    $checklist = array(
                        'id_data_umum' => $ide,
                        'id_alat' => $value,
                        'k1' => $this->input->post('k1_' . $value),
                        'k2' => $this->input->post('k2_' . $value),
                        'k3' => $this->input->post('k3_' . $value),
                        'k4' => $this->input->post('k4_' . $value)
                    );
                    $this->Ct_m->insert('checklist_ct', $checklist);
                }
                $input_catatan = array(
                    'id_data_umum' => $ide,
                    'uraian_checklist' => $this->input->post('catatan1'),
                    'uraian_uji' => $this->input->post('catatan2')
                );
                $this->Ct_m->insert('catatan', $input_catatan);
                $hasil_ukurnya = $this->input->post('hasil_ukurnya');
                foreach ($hasil_ukurnya as $value) {
                    $input_hasiluji = array(
                        'id_data_umum' => $ide,
                        'id_hasil_uji' => $value,
                        'fasa_r_std' => '',
                        'fasa_r_th' => $this->input->post('tu1_' . $value),
                        'fasa_r_hu' => $this->input->post('hu1_' . $value),
                        'fasa_s_std' => '',
                        'fasa_s_th' => $this->input->post('tu2_' . $value),
                        'fasa_s_hu' => $this->input->post('hu2_' . $value),
                        'fasa_t_std' => '',
                        'fasa_t_th' => $this->input->post('tu3_' . $value),
                        'fasa_t_hu' => $this->input->post('hu3_' . $value),
                        'injeksi_alat_uji' => $this->input->post('injeksi_' . $value)
                    );
                    $this->Ct_m->insert('pengujian_ct', $input_hasiluji);
                }
                for ($i = 0; $i < 3; $i++) {
                    $pj = array(
                        'id_data_umum' => $ide,
                        'pelaksana' => $this->input->post('pelaksana_' . $i),
                        'pengawas' => $this->input->post('pengawas_' . $i)
                    );
                    $this->Ct_m->insert('penanggung_jawab', $pj);
                }
            }
        }
        redirect(base_url('mode_ct/ct'));
//        }
    }

}
