<?php

class Pmt_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_data_daftar($jenis, $gi, $bay) {
        $sql = "SELECT * FROM data_umum WHERE jenis_mode_testing='$jenis' and bay ='$bay' and lokasi_gi='$gi' ORDER BY tahun DESC;";
        return $this->db->query($sql)->result();
    }

    function get_unit($id) {
        $sql = "SELECT * FROM kr_organisasi";
        $sql.= ($id) ? " WHERE parent_id = '$id'" : '';
        $sql.= " ORDER BY org_key";
        return $this->db->query($sql)->result();
    }

    function insert($tablename, $data1) {
        return ($this->db->insert($tablename, $data1)) ? $this->db->insert_id() : false;
    }

    function insert_batch($tablename, $data) {
        return $this->db->insert_batch($tablename, $data);
    }

    function update($tablename, $where, $kolom) {
        $this->db->where($where);
        return $this->db->update($tablename, $kolom);
    }

//    function update($tablename, $where, $data) {
//        $this->db->where($where);
//        $this->db->update($tablename, $data);
//        return true;
//    }

    function delete($tablename, $where) {
        $this->db->where($where);
        return $this->db->delete($tablename);
    }

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function single_view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->row();
    }

    public function detail_organisasi($orgkey) {
        $sql = "select unit_kerja_simpeg unit, parent dinas from kr_organisasi WHERE org_key =" . $orgkey;
        return $this->db->query($sql)->row();
    }

    function get_data_edit($uk_data) {
        $sql = "SELECT 
	checklist_pmt.id,
	checklist_pmt.id_alat,
	checklist_pmt.id_data_umum,
	checklist_pmt.k1,
	checklist_pmt.k2,
	checklist_pmt.k3,
	checklist_pmt.k4,
	peralatan_pmt.kode,
	peralatan_pmt.nama_alat,
	peralatan_pmt.k1 AS alat1,
	peralatan_pmt.k2 AS alat2,
	peralatan_pmt.k3 AS alat3,
	peralatan_pmt.k4 AS alat4
FROM
	checklist_pmt
JOIN peralatan_pmt ON checklist_pmt.id_alat = peralatan_pmt.id
WHERE checklist_pmt.id_data_umum=$uk_data
ORDER BY id_alat ASC";
        return $this->db->query($sql)->result();
    }

    function get_data_uji($uk_data) {
        $sql = "SELECT
	hasil_uji_pmt.id,
	hasil_uji_pmt.kode,
	hasil_uji_pmt.pembeda,
	hasil_uji_pmt.titik_ukur,
	pengujian_pmt.id AS id_pengujian,
	pengujian_pmt.meger_alat_uji,
	pengujian_pmt.fasa_r_hu,
	pengujian_pmt.fasa_s_hu,
	pengujian_pmt.fasa_t_hu,
	pengujian_pmt.fasa_r_th,
	pengujian_pmt.fasa_s_th,
	pengujian_pmt.fasa_t_th,
	pengujian_pmt.pmt_sebelum,
	pengujian_pmt.pmt_sesudah
FROM
	pengujian_pmt
JOIN hasil_uji_pmt ON pengujian_pmt.id_hasil_uji = hasil_uji_pmt.id
WHERE
	pengujian_pmt.id_data_umum =$uk_data";
        return $this->db->query($sql)->result();
    }

    function get_data_pmtnyak($uk_data, $id_uji) {
        $sql = "SELECT
	pengujian_pmt.pmt_sebelum,
	pengujian_pmt.pmt_sesudah
FROM
	pengujian_pmt
JOIN hasil_uji_pmt ON pengujian_pmt.id_hasil_uji = hasil_uji_pmt.id
WHERE
	pengujian_pmt.id_data_umum = $uk_data
and hasil_uji_pmt.id=$id_uji";
        return $this->db->query($sql)->result();
    }

}
