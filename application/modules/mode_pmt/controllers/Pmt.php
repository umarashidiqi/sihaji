<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pmt extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Pmt_m'));
    }

    public function index() {
        $this->valid_login();
        $this->data['judul'] = "Menu Data PMT";
        $gi_option = session('gi_option');
        $bay_option = session('bay_option');
//        echo '--'.$gi_option.'.'.$bay_option;
//        die();
        $this->data['datanya'] = $this->Pmt_m->get_data_daftar('PMT', $gi_option, $bay_option);

        $this->content = 'daftar';
        $this->view_m();
    }

    public function tambah() {
        $this->valid_login();
        $this->data['action'] = base_url('mode_pmt/pmt/tambah_proses');
        $this->data['judul'] = "Tambah Data PMT ";
        $this->data['sub_judul'] = "Form Tambah Data PMT ";
        $this->data['data_alat'] = $this->Pmt_m->get('peralatan_pmt');
        $this->data['hasil_uji'] = $this->Pmt_m->get('hasil_uji_pmt');
        $this->content = 'form';
        $this->view();
    }

    public function edit($id) {
        $this->data['action'] = base_url('mode_pmt/pmt/proses_edit');
        $this->data['judul'] = "Edit Data PMT";
        $this->data['data_edit'] = $this->Pmt_m->get_data_edit($id);

        $this->data['data_umum'] = $this->Pmt_m->view('data_umum', array('id' => $id));
        $this->data['id_data_umum'] = $this->data['data_umum'][0]->id;
        $this->data['data_uji'] = $this->Pmt_m->get_data_uji($id);
//        print_r_pre($this->data['data_uji']);
        $this->data['catatan'] = $this->Pmt_m->view('catatan', array('id_data_umum' => $id));
        $this->data['penanggung_jawab'] = $this->Pmt_m->view('penanggung_jawab', array('id_data_umum' => $id));

        $this->data['pmt_sebelum'] = $this->Pmt_m->get_data_pmtnyak($id, 21);
        $this->data['pmt_sesudah'] = $this->Pmt_m->get_data_pmtnyak($id, 22);

        $this->content = 'form_edit';
        $this->view();
    }

    public function cetak($id) {
        $data['data_edit'] = $this->Pmt_m->get_data_edit($id);

        $data['data_umum'] = $this->Pmt_m->view('data_umum', array('id' => $id));
        $data['data_uji'] = $this->Pmt_m->get_data_uji($id);
        $data['catatan'] = $this->Pmt_m->view('catatan', array('id_data_umum' => $id));
        $data['penanggung_jawab'] = $this->Pmt_m->view('penanggung_jawab', array('id_data_umum' => $id));

        $data['pmt_sebelum'] = $this->Pmt_m->get_data_pmtnyak($id, 21);
        $data['pmt_sesudah'] = $this->Pmt_m->get_data_pmtnyak($id, 22);

        $this->load->view('cetak', $data);
        $html = $this->output->get_output();
        //$this->view();
        generate_pdf($html);
    }

    public function tambah_proses() {
        $this->valid_login();
        $rule = array(
            array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required'),
            array('field' => 'tanggal', 'label' => 'Tanggal', 'rules' => 'required'),
            array('field' => 'uraian', 'label' => 'Uraian', 'rules' => 'required')
        );
        $input_data_umum = array(
            'id_user' => session('id_user'),
            'nomor' => $this->input->post('nomor'),
            'tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
            'merk' => $this->input->post('merk'),
            'type' => $this->input->post('type'),
            'type_fasa_r' => $this->input->post('fasa_r'),
            'type_fasa_s' => $this->input->post('fasa_s'),
            'type_fasa_t' => $this->input->post('fasa_t'),
            'bay' => $this->input->post('bay'),
            'lokasi_gi' => $this->input->post('lokasi_gi'),
            'jenis_mode_testing' => 'PMT',
            'tahun' => $this->input->post('tahun')
        );
        if ($id = $this->Pmt_m->insert('data_umum', $input_data_umum)) {
            $uk_alat = $this->input->post('idnyak');
            foreach ($uk_alat as $value) {
                $checklist = array(
                    'id_data_umum' => $id,
                    'id_alat' => $value,
                    'k1' => $this->input->post('k1_' . $value),
                    'k2' => $this->input->post('k2_' . $value),
                    'k3' => $this->input->post('k3_' . $value),
                    'k4' => $this->input->post('k4_' . $value)
                );
                $this->Pmt_m->insert('checklist_pmt', $checklist);
            }

            $input_catatan = array(
                'id_data_umum' => $id,
                'uraian_uji' => $this->input->post('catatan2')
            );
            $this->Pmt_m->insert('catatan', $input_catatan);
            $hasil_ukurnya = $this->input->post('hasil_ukurnya');
            foreach ($hasil_ukurnya as $value) {
                $input_hasiluji = array(
                    'id_data_umum' => $id,
                    'id_hasil_uji' => $value,
                    'fasa_r_std' => '',
                    'fasa_r_th' => $this->input->post('tu1_' . $value),
                    'fasa_r_hu' => $this->input->post('hu1_' . $value),
                    'fasa_s_std' => '',
                    'fasa_s_th' => $this->input->post('tu2_' . $value),
                    'fasa_s_hu' => $this->input->post('hu2_' . $value),
                    'fasa_t_std' => '',
                    'fasa_t_th' => $this->input->post('tu3_' . $value),
                    'fasa_t_hu' => $this->input->post('hu3_' . $value),
                    'meger_alat_uji' => $this->input->post('meger_alat_uji_' . $value),
                    'pmt_sebelum' => $this->input->post('pmt_sebelum_' . $value),
                    'pmt_sesudah' => $this->input->post('pmt_sesudah_' . $value)
                );
                $this->Pmt_m->insert('pengujian_pmt', $input_hasiluji);
            }
           for ($i = 1; $i < 4; $i++) {
                $pj = array(
                    'id_data_umum' => $id,
                    'pelaksana' => $this->input->post('pelaksana_' . $i),
                    'pengawas' => $this->input->post('pengawas_' . $i)
                );
                $this->Pmt_m->insert('penanggung_jawab', $pj);
            }
        }
        redirect(base_url('mode_pmt/pmt'));
    }

    public function proses_edit() {
        $this->valid_login();
        $rule = array(
            array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required'),
            array('field' => 'tanggal', 'label' => 'Tanggal', 'rules' => 'required'),
            array('field' => 'uraian', 'label' => 'Uraian', 'rules' => 'required')
        );
        $ide = $this->input->post('id_data_umum');
        //catatan,ceklist,pengujian update
        if ($this->Pmt_m->delete('checklist_pmt', array('id_data_umum' => $ide)) and $this->Pmt_m->delete('pengujian_pmt', array('id_data_umum' => $ide)) and $this->Pmt_m->delete('catatan', array('id_data_umum' => $ide)) and $this->Pmt_m->delete('penanggung_jawab', array('id_data_umum' => $ide))) {

            $input_data_umum = array(
                'id_user' => session('id_user'),
                'nomor' => $this->input->post('nomor'),
                'tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
                'merk' => $this->input->post('merk'),
                'type' => $this->input->post('type'),
                'type_fasa_r' => $this->input->post('fasa_r'),
                'type_fasa_s' => $this->input->post('fasa_s'),
                'type_fasa_t' => $this->input->post('fasa_t'),
                'bay' => $this->input->post('bay'),
                'lokasi_gi' => $this->input->post('lokasi_gi'),
                'jenis_mode_testing' => 'PMT',
                'tahun' => $this->input->post('tahun')
            );
//             print_r_pre($input_data_umum);
//        die();
            if ($this->Pmt_m->update('data_umum', array('id' => $ide), $input_data_umum)) {
                $uk_alat = $this->input->post('idnyak');
                foreach ($uk_alat as $value) {
                    $checklist = array(
                        'id_data_umum' => $ide,
                        'id_alat' => $value,
                        'k1' => $this->input->post('k1_' . $value),
                        'k2' => $this->input->post('k2_' . $value),
                        'k3' => $this->input->post('k3_' . $value),
                        'k4' => $this->input->post('k4_' . $value)
                    );
                    $this->Pmt_m->insert('checklist_pmt', $checklist);
                }

                $input_catatan = array(
                    'id_data_umum' => $ide,
                    'uraian_uji' => $this->input->post('catatan2')
                );
                $this->Pmt_m->insert('catatan', $input_catatan);
                $hasil_ukurnya = $this->input->post('hasil_ukurnya');
                foreach ($hasil_ukurnya as $value) {
                    $input_hasiluji = array(
                        'id_data_umum' => $ide,
                        'id_hasil_uji' => $value,
                        'fasa_r_std' => '',
                        'fasa_r_th' => $this->input->post('tu1_' . $value),
                        'fasa_r_hu' => $this->input->post('hu1_' . $value),
                        'fasa_s_std' => '',
                        'fasa_s_th' => $this->input->post('tu2_' . $value),
                        'fasa_s_hu' => $this->input->post('hu2_' . $value),
                        'fasa_t_std' => '',
                        'fasa_t_th' => $this->input->post('tu3_' . $value),
                        'fasa_t_hu' => $this->input->post('hu3_' . $value),
                        'meger_alat_uji' => $this->input->post('meger_alat_uji_' . $value),
                        'pmt_sebelum' => $this->input->post('pmt_sebelum_' . $value),
                        'pmt_sesudah' => $this->input->post('pmt_sesudah_' . $value)
                    );
                    $this->Pmt_m->insert('pengujian_pmt', $input_hasiluji);
                }
               for ($i = 0; $i < 3; $i++) {
                    $pj = array(
                        'id_data_umum' => $ide,
                        'pelaksana' => $this->input->post('pelaksana_' . $i),
                        'pengawas' => $this->input->post('pengawas_' . $i)
                    );
                    $this->Pmt_m->insert('penanggung_jawab', $pj);
                }
            }
        }
        redirect(base_url('mode_pmt/pmt'));
    }

    function hapus($id) {
        $this->db->trans_start();
        $this->Pmt_m->delete('checklist_pmt', array('id' => $id));
        $this->Pmt_m->delete('catatan', array('id' => $id));
        $this->Pmt_m->delete('pengujian_pmt', array('id' => $id));
        $this->Pmt_m->delete('penanggung_jawab', array('id' => $id));
        $this->Pmt_m->delete('data_umum', array('id' => $id));
        $this->db->trans_complete();
        echo TRUE;
    }

}
