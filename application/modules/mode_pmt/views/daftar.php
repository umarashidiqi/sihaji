<div class="gallery-fix"></div> 
<div class="header header-light">
    <a href="#" class="open-menu"><i class="fa fa-navicon"></i></a>
    <h3><?php echo $judul; ?></h3>
    <a href="#" onclick="window.history.go(-1);
            return false;" title="Back"><i class="fa fa-chevron-left"></i></a>
</div>
<div class="navigation navigation-light">
    <div class="navigation-scroll">
        <a href="<?php echo base_url('welcome/menu'); ?>" class="menu-item"><i class="fa fa-home"></i><em>Main Menu</em><i class="fa fa-circle"></i></a>
        <?php if (session('level_user') == 'admin') { ?>
            <a href="<?php echo base_url('user'); ?>" class="menu-item"><i class="fa fa-cogs"></i><em>Kelola User</em><i class="fa fa-circle"></i></a>
        <?php } ?>
        <a href="<?php echo base_url('welcome/info/1'); ?>" class="menu-item"><i class="fa fa-building"></i><em>Tentang Kami</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/2'); ?>" class="menu-item"><i class="fa fa-cube"></i><em>Panduan</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/3'); ?>" class="menu-item"><i class="fa fa-phone"></i><em>Kontak</em><i class="fa fa-circle"></i></a>
        <a href="#" class="menu-item close-menu"><i class="fa fa-times-circle"></i><em>Close</em><i class="fa fa-circle"></i></a>
    </div>
</div>
<div id="page-content" class="bg-1">
    <div id="page-content-scroll">
        <h3 class="header-mask header-mask-light">Welcome</h3>
        <div class="content-strip">
            <div class="strip-content">
                <h4>Daftar Data PMT</h4>
                <p>Sistem Informasi Hasil Pengujian</p>
                <i class="fa fa-cogs"></i>
            </div>
            <div class="overlay"></div>
            <img class="preload-image" data-original="<?php echo image() ?>/pictures/9.jpg" alt="img">
        </div>
        <div class="content">
            <?php
            if (session('level_user') == 'user') {
                ?>
                <a href="<?php echo site_url('mode_pmt/pmt/tambah'); ?>" class="button button-green button-round button-fullscreen" title="Tambah"><i class="fa fa-plus"></i> Tambah</a>
                <?php
            }
            ?>
            <div class="decoration"></div>
            <div class="table-responsive">
                <table style="white-space: nowrap;" class="table table-bordered table-hover" id="datatable" width="100%">
                    <thead>
                        <tr>
                            <th width='5%' style="vertical-align: middle" class="text-center">No</th>
                            <th style="vertical-align: middle" class="text-center">Tanggal</th>
                            <th style="vertical-align: middle" class="text-center">Merk</th>
                            <th style="vertical-align: middle" class="text-center">Type</th>
                            <th style="vertical-align: middle" class="text-center">Bay</th>
                            <th style="vertical-align: middle" class="text-center">Lokasi</th>    
                            <th style="vertical-align: middle" class="text-center">Tahun</th>                                
                            <th width='20%' style="vertical-align: middle" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($datanya as $value) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $no; ?></td>
                                <td><?php echo date('d F Y', strtotime($value->tanggal)); ?></td>
                                <td><?php echo $value->merk; ?></td>
                                <td><?php echo $value->type; ?></td>
                                <td><?php echo $value->bay; ?></td>
                                <td><?php echo $value->lokasi_gi; ?></td>
                                <td><?php echo $value->tahun; ?></td>
                                <td class="text-center">  
                                    <?php
                                    if (session('level_user') == 'user') {
                                        ?>
                                        <a href="<?php echo site_url('mode_pmt/pmt/cetak/' . $value->id); ?>" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                        <a href="<?php echo site_url('mode_pmt/pmt/edit/' . $value->id); ?>" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="<?php echo site_url('mode_pmt/pmt/hapus/' . $value->id); ?>" class="btn btn-danger btn-sm hapus_btn" title="Hapus"><i class="fa fa-trash"></i></a>                                                                                              
                                        <?php
                                    } else {
                                        ?>
                                        <a href="<?php echo site_url('mode_pmt/pmt/cetak/' . $value->id); ?>" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $no++;
                        }
                        echo (count($datanya) == 0) ? '<tr><td colspan="8" class="text-center">Maaf Data Tidak Ditemukan!</td></tr>' : '';
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="footer">
            <p class="footer-strip">Copyright <span id="copyright-year"></span> Silvanix.com. All Rights Reserved</p>
        </div>
        <div class="footer-clear"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
<?php konfirmasi_swal('Data PMT'); ?>
        $('#datatable').dataTable({
            "paging": true,
            "info": true,
            "bFilter": true,
            "oLanguage": {
                "zeroRecords": "Maaf data masih kosong!",
                "infoEmpty": "Menampilkan 0 data",
                "sSearch": "Cari Data: ",
                "searchPlaceholder": "Cari data..",
                "info": "Menampilkan _PAGE_ of _PAGES_",
                "lengthMenu": "Tampilkan _MENU_ data per halaman",
            },
            "scrollX": true,
            "scrollCollapse": true
        });
    });
</script>