<?php

class Pt_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_data_daftar($jenis, $gi, $bay) {
        $sql = "SELECT * FROM data_umum WHERE jenis_mode_testing='$jenis' and bay ='$bay' and lokasi_gi='$gi' ORDER BY tahun DESC;";
        return $this->db->query($sql)->result();
    }

    function get_unit($id) {
        $sql = "SELECT * FROM kr_organisasi";
        $sql.= ($id) ? " WHERE parent_id = '$id'" : '';
        $sql.= " ORDER BY org_key";
        return $this->db->query($sql)->result();
    }

    function insert($tablename, $data1) {
        return ($this->db->insert($tablename, $data1)) ? $this->db->insert_id() : false;
    }

    function insert_batch($tablename, $data) {
        return $this->db->insert_batch($tablename, $data);
    }

    function update($tablename, $where, $kolom) {
        $this->db->where($where);
        return $this->db->update($tablename, $kolom);
    }

//    function update($tablename, $where, $data) {
//        $this->db->where($where);
//        $this->db->update($tablename, $data);
//        return true;
//    }

    function delete($tablename, $where) {
        $this->db->where($where);
        return $this->db->delete($tablename);
    }

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function single_view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->row();
    }

    public function detail_organisasi($orgkey) {
        $sql = "select unit_kerja_simpeg unit, parent dinas from kr_organisasi WHERE org_key =" . $orgkey;
        return $this->db->query($sql)->row();
    }

    function get_data_edit($uk_data) {
        $sql = "SELECT
	checklist_pt.id,
	checklist_pt.id_alat,
	checklist_pt.id_data_umum,
	checklist_pt.k1,
	checklist_pt.k2,
	checklist_pt.k3,
	checklist_pt.k4,
	peralatan_pt.kode,
	peralatan_pt.nama_alat,
	peralatan_pt.k1 AS alat1,
	peralatan_pt.k2 AS alat2,
	peralatan_pt.k3 AS alat3,
	peralatan_pt.k4 AS alat4
FROM
	checklist_pt
JOIN peralatan_pt ON checklist_pt.id_alat = peralatan_pt.id
WHERE checklist_pt.id_data_umum=$uk_data";
        return $this->db->query($sql)->result();
    }

    function get_data_uji($uk_data) {
        $sql = "SELECT
hasil_uji_pt.id,
hasil_uji_pt.kode,
hasil_uji_pt.pembeda,
hasil_uji_pt.titik_ukur,
pengujian_pt.id as id_pengujian, 
pengujian_pt.injeksi_alat_uji, 
pengujian_pt.fasa_r_hu, 
pengujian_pt.fasa_s_hu, 
pengujian_pt.fasa_t_hu,
pengujian_pt.fasa_r_th,
pengujian_pt.fasa_s_th,
pengujian_pt.fasa_t_th
FROM
	pengujian_pt
JOIN hasil_uji_pt ON pengujian_pt.id_hasil_uji = hasil_uji_pt.id
WHERE pengujian_pt.id_data_umum=$uk_data";
        return $this->db->query($sql)->result();
    }

}
