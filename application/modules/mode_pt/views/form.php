<div class="gallery-fix"></div> 
<div class="header header-light">
    <a href="#" onclick="window.history.go(-1);
            return false;" title="Back"><i class="fa fa-chevron-left"></i></a>
    <h3><?php echo $judul; ?></h3>
</div>
<div class="navigation navigation-light">
    <div class="navigation-scroll">
        <a href="<?php echo base_url('welcome/menu'); ?>" class="menu-item"><i class="fa fa-home"></i><em>Main Menu</em><i class="fa fa-circle"></i></a>
        <?php if (session('level_user') == 'admin') { ?>
            <a href="<?php echo base_url('user'); ?>" class="menu-item"><i class="fa fa-cogs"></i><em>Kelola User</em><i class="fa fa-circle"></i></a>
        <?php } ?>
        <a href="<?php echo base_url('welcome/info/1'); ?>" class="menu-item"><i class="fa fa-building"></i><em>Tentang Kami</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/2'); ?>" class="menu-item"><i class="fa fa-cube"></i><em>Panduan</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/3'); ?>" class="menu-item"><i class="fa fa-phone"></i><em>Kontak</em><i class="fa fa-circle"></i></a>
        <a href="#" class="menu-item close-menu"><i class="fa fa-times-circle"></i><em>Close</em><i class="fa fa-circle"></i></a>
    </div>
</div>
<div id="page-content" class="bg-1">
    <div id="page-content-scroll">
        <div class="content">
            <br>
            <br>
            <br>
            <br>
            <div class="decoration"></div>
            <div class="col-lg-12">
                <form class="form-horizontal" id="ct" action="<?php echo @$action ?>" method="post">
                    <div style="text-align: center">
                        <b>CHECK LIST   PT ( POTENTIAL TRANSFORMER ) 20 kV</b>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2 col-xs-5">
                            <label for="Tanggal">Tahun</label>
                            <input type="text" name="tahun" class="form-control" id="tahun">
                        </div> 
                    </div> 
                    <div class="form-group">
                        <div class="col-md-6 col-xs-7">
                            <label for="Tanggal">Nomor</label>
                            <input type="text" name="nomor" id="nomor" class="form-control" placeholder="" value="">
                        </div>
                        <div class="col-md-4 col-xs-5">
                            <label for="Tanggal">Tanggal</label>
                            <input type="text" name="tanggal" id="tanggal" class="form-control" placeholder="" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-xs-6">
                            <label>Merk : </label>
                            <input id="merk" type="text" name="merk" class="form-control" placeholder="" value="">
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <label>Bay</label>
                            <input id="bay" id="bay" type="text" name="bay" class="form-control" placeholder="" value="<?php
                            if (session('bay_option') != '') {
                                echo session('bay_option');
                            }
                            ?>" <?php
                                   if (session('bay_option') != '') {
                                       echo 'readonly=""';
                                   }
                                   ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-12">
                            <label>Type : </label>
                            <input id="type" type="text" name="type" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-1">
                            <label>No. Serie</label>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>Fasa R : </label>
                            <input id="fasa_r" type="text" name="fasa_r" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-xs-6">
                            <label>Fasa S : </label>
                            <input id="fasa_s" type="text" name="fasa_s" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <label>Lokasi GI : </label>
                            <input id="lokasi_gi" type="text" name="lokasi_gi" class="form-control" value="<?php
                            if (session('gi_option') != '') {
                                echo session('gi_option');
                            }
                            ?>" <?php
                                   if (session('gi_option') != '') {
                                       echo 'readonly=""';
                                   }
                                   ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-12">
                            <label>Fasa T : </label>
                            <input id="fasa_t" type="text" name="fasa_t" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-12">
                            <label>Ratio Core 1 : </label>
                            <input id="ratio_1" type="text" name="ratio_1" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-12">
                            <label>Ratio Core 2 : </label>
                            <input id="ratio_2" type="text" name="ratio_2" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div><b>
                            I. Check List :
                        </b></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" width="100%" border="1">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle" class="text-center">No</th>
                                    <th style="vertical-align: middle" class="text-center">Peralatan Yang Diperiksa</th>
                                    <th colspan="2" style="vertical-align: middle" class="text-center">Kondisi Awal</th>
                                    <th colspan="2" style="vertical-align: middle" class="text-center">Kondisi Akhir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($data_alat as $value) {
                                    $exp = explode('.', $value->kode);
                                    $jml = count($exp);
                                    if ($jml == 1) {
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="idnyak[]" value="<?php echo $value->id; ?>">
                                                <b> <?php echo $value->kode; ?></b>
                                            </td>
                                            <td colspan="5"><b><?php echo $value->nama_alat; ?></b></td>
                                        </tr>
                                        <?php
                                    } elseif ($jml == 2) {
                                        ?>
                                        <tr>
                                            <td><input type="hidden" name="idnyak[]" value="<?php echo $value->id; ?>"></td>
                                            <td><?php echo $value->kode . ' ' . $value->nama_alat; ?></td>
                                            <td>
                                                <div class="checkbox">
                                                    <input id="k1_<?php echo $value->id; ?>" type="checkbox" name="k1_<?php echo $value->id; ?>" class="" title="Pilih" value="1">
                                                    <label for="k1_<?php echo $value->id; ?>"><?php echo $value->k1; ?></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="checkbox">
                                                    <input id="k2_<?php echo $value->id; ?>" type="checkbox" name="k2_<?php echo $value->id; ?>" class="" title="Pilih" value="1">
                                                    <label for="k2_<?php echo $value->id; ?>"><?php echo $value->k2; ?></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="checkbox">
                                                    <input id="k3_<?php echo $value->id; ?>" type="checkbox" name="k3_<?php echo $value->id; ?>" class="" title="Pilih" value="1">
                                                    <label for="k3_<?php echo $value->id; ?>"><?php echo $value->k3; ?></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="checkbox">
                                                    <input id="k4_<?php echo $value->id; ?>" type="checkbox" name="k4_<?php echo $value->id; ?>" class="" title="Pilih" value="1">
                                                    <label for="k4_<?php echo $value->id; ?>"><?php echo $value->k4; ?></label>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <tr>
                                    <td>7</td>
                                    <td>Catatan</td>
                                    <td colspan="8">
                                        <textarea  class="form-control" name="catatan1"></textarea>
                                    </td>
                                </tr>
                                <?php ?>
                            </tbody>
                        </table>
                    </div>
                    <div style="text-align: center">
                        <b>HASIL PENGUJIAN   CT ( CURRENT TRANSFORMER ) 150KV</b>
                    </div>
                    <div><b>
                            II. Hasil Pengukuran :
                        </b></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" width="100%" border="1">
                            <thead>
                            </thead>
                            <tbody id="data">
                                <tr>
                                    <td><b>1</b></td>
                                    <td colspan="10"><b>Tahanan Isolasi belitan</b></td>
                                </tr>
                                <?php
                                $no_1 = 1;
                                $no_2 = 1;
                                $no_3 = 7;
                                $no_4 = 1;
//                                $n = 1;
                                foreach ($hasil_uji as $value) {
                                    $exp = explode('.', $value->kode);
                                    $jml = count($exp);

                                    if ($value->pembeda == 1) {
                                        //untuk pertama foreachnya
                                        ?>
                                        <tr>
                                            <!--<td>-->
                                    <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                    <!--</td>-->
                                    <td colspan="2"><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                    <?php echo($no_1 == 1) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td><input type="text" class="form-control" name="tu1_<?php echo $value->id; ?>" value=""></td>
                                    <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value=""></td>
                                    <?php echo($no_1 == 1) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td><input type="text" class="form-control" name="tu2_<?php echo $value->id; ?>" value=""></td>
                                    <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value=""></td>
                                    <?php echo($no_1 == 1) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td><input type="text" class="form-control" name="tu3_<?php echo $value->id; ?>" value=""></td>
                                    <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value=""></td>
                                    </tr>
                                    <?php
                                    $no_1++;
                                } elseif ($jml == 2 and $value->kode == '1.1') {
                                    //injeksi teg 1
                                    ?>
                                    <tr>
                                        <td><input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>"></td>
                                        <td><b><?php echo $value->kode . ' ' . $value->titik_ukur; ?></b></td>
                                        <td colspan="5"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value=""></td>
                                        <td colspan="4">Volt</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2" rowspan="2" style="vertical-align: middle" class="text-center">Titik Ukur</th>
                                        <th colspan="3" style="vertical-align: middle" class="text-center">Fasa R</th>
                                        <th colspan="3" style="vertical-align: middle" class="text-center">Fasa S</th>
                                        <th colspan="3" style="vertical-align: middle" class="text-center">Fasa T</th>
                                    </tr>
                                    <tr>
                                        <th style="vertical-align: middle" class="text-center">Standart</th>
                                        <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                        <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                        <th style="vertical-align: middle" class="text-center">Standart</th>
                                        <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                        <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                        <th style="vertical-align: middle" class="text-center">Standart</th>
                                        <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                        <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                    </tr>
                                    <?php
                                    $no_2++;
                                } elseif ($jml == 2 and $value->kode == '1.2') {
                                    //injeksi teg 2
                                    ?>
                                    <tr>
                                        <td><input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>"></td>
                                        <td><b><?php echo $value->kode . ' ' . $value->titik_ukur; ?></b></td>
                                        <td colspan="5"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value=""></td>
                                        <td colspan="4">Volt</td>
                                    </tr>
                                    <?php
                                    $no_2++;
                                } elseif ($value->pembeda == 2) {
                                    //foreach 2
                                    ?>
                                    <tr>
                                        <!--<td>-->
                                    <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                    <!--</td>-->
                                    <td colspan="2"><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                    <?php echo($no_3 == 7) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td><input type="text" class="form-control" name="tu1_<?php echo $value->id; ?>" value=""></td>
                                    <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value=""></td>
                                    <?php echo($no_3 == 7) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td><input type="text" class="form-control" name="tu2_<?php echo $value->id; ?>" value=""></td>
                                    <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value=""></td>
                                    <?php echo($no_3 == 7) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td><input type="text" class="form-control" name="tu3_<?php echo $value->id; ?>" value=""></td>
                                    <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value=""></td>
                                    </tr>
                                    <?php
                                    $no_3++;
                                } elseif ($value->pembeda == 3) {
                                    //alat uji
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                        </td>
                                        <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>

                                        <td colspan="9"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value=""></td>
                                    </tr>
                                    <?php
                                } elseif ($value->pembeda == 4) {
                                    //Tahanan Pentanahan
                                    ?>
                                    <tr>
                                        <td><b>2</b></td>
                                        <td colspan="10"><b><?php echo $value->titik_ukur; ?></b></td>
                                    </tr>
                                    <tr>
                                        <th colspan="2" rowspan="2" style="vertical-align: middle" class="text-center">Titik Ukur</th>
                                        <th colspan="3" style="vertical-align: middle" class="text-center">Fasa R</th>
                                        <th colspan="3" style="vertical-align: middle" class="text-center">Fasa S</th>
                                        <th colspan="3" style="vertical-align: middle" class="text-center">Fasa T</th>
                                    </tr>
                                    <tr>
                                        <th style="vertical-align: middle" class="text-center">Standart</th>
                                        <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                        <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                        <th style="vertical-align: middle" class="text-center">Standart</th>
                                        <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                        <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                        <th style="vertical-align: middle" class="text-center">Standart</th>
                                        <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                        <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                    </tr>
                                    <?php
                                } elseif ($value->pembeda == 5) {
                                    //tahanan input
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                            <?php // echo $no_3;    ?>
                                        </td>
                                        <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                        <td>< 1 W</td>
                                        <td><input type="text" class="form-control" name="tu1_<?php echo $value->id; ?>" value=""></td>
                                        <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value=""></td>
                                        <td>< 1 W</td>
                                        <td><input type="text" class="form-control" name="tu2_<?php echo $value->id; ?>" value=""></td>
                                        <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value=""></td>
                                        <td>< 1 W</td>
                                        <td><input type="text" class="form-control" name="tu3_<?php echo $value->id; ?>" value=""></td>
                                        <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value=""></td>
                                    </tr>
                                    <?php
                                } elseif ($value->pembeda == 6) {
                                    //injeksi
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                        </td>
                                        <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>

                                        <td colspan="9"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value=""></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>

                            <tr>
                                <td><b>3</b></td>
                                <td colspan="10"><b>Tangen Delta (Hasil Uji Terlampir)</b></td>
                            </tr>
                            <tr>
                                <td><b>4</b></td>
                                <td><b>Catatan</b></td>
                                <td colspan="9">
                                    <textarea  class="form-control" name="catatan2"></textarea>
                                </td>
                            </tr>

                            <?php ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th style="vertical-align: middle" class="text-center">Pelaksana Pengujian :</th>
                                    <th style="vertical-align: middle" class="text-center">Pengawas Pekerjaan :</th>
                                    <th style="vertical-align: middle" class="text-center">Tanda tangan :</th/>
                                </tr>
                            </thead>
                            <tbody id="data">
                                <?php
                                for ($i = 1; $i < 4; $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><input type="text" class="form-control" name="pelaksana_<?php echo $i; ?>" value=""></td>
                                        <?php
                                        if ($i == 1) {
                                            echo '<td style=vertikal-align:center rowspan=3><input class="form-control"  type="text" name="pengawas_1" value=""></td>
                                                <td rowspan="3"></td>';
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <button id="simpan_data" type="submit" formnovalidate="" class="btn btn-success pull-right" title="Simpan Data SPP UP"><i class="fa fa-save"></i> Simpan</button>
                            <a href="<?php echo site_url('mode_ct/ct'); ?>"  class="btn btn-default pull-right"  title="Kembali ke Daftar SPP UP" style="margin-right: 10px;"><i class="fa fa-backward"></i>  Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="footer">
            <p class="footer-strip">Copyright <span id="copyright-year"></span> Silvanix.com. All Rights Reserved</p>
        </div>
        <div class="footer-clear"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tanggal').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        $('#tahun').datepicker({
            autoclose: true,
            format: 'yyyy',
            viewMode: "years",
            minViewMode: "years"
        });
    });
</script>