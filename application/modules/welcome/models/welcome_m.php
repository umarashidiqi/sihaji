<?php

class Welcome_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function ambil_data($data) {
        $user = $this->db->get_where('user', $data);
        return $user->num_rows();
    }

    function get_unit($id) {
        $sql = "SELECT * FROM kr_organisasi";
        $sql.= ($id) ? " WHERE parent_id = '$id'" : '';
        $sql.= " ORDER BY org_key";
        return $this->db->query($sql)->result();
    }

    function insert($tablename, $data1) {
        return ($this->db->insert($tablename, $data1)) ? $this->db->insert_id() : false;
    }

    function insert_batch($tablename, $data) {
        return $this->db->insert_batch($tablename, $data);
    }

    function update($tablename, $where, $kolom) {
        $this->db->where($where);
        return $this->db->update($tablename, $kolom);
    }

//    function update($tablename, $where, $data) {
//        $this->db->where($where);
//        $this->db->update($tablename, $data);
//        return true;
//    }

    function delete($tablename, $where) {
        $this->db->where($where);
        return $this->db->delete($tablename);
    }

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function single_view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->row();
    }

    public function detail_organisasi($orgkey) {
        $sql = "select unit_kerja_simpeg unit, parent dinas from kr_organisasi WHERE org_key =" . $orgkey;
        return $this->db->query($sql)->row();
    }

}
