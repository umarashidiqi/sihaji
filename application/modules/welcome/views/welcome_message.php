<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0 minimal-ui"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/android-chrome-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" sizes="196x196" href="<?php echo image() ?>/splash/apple-touch-icon-196x196.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo image() ?>/splash/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo image() ?>/splash/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo image() ?>/splash/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo image() ?>/splash/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo image() ?>/splash/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo image() ?>/splash/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo image() ?>/splash/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo image() ?>/splash/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo image() ?>/splash/apple-touch-icon-57x57.png">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-16x16.png" sizes="16x16">
    <link rel="shortcut icon" href="<?php echo image() ?>/splash/favicon.ico" type="image/x-icon"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
    <title>Sihaji 1.0</title>
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>framework.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>sweetalert.css">
    <script type="text/javascript" src="<?php echo js() ?>jquery.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>promise.min.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>plugins.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>custom.js"></script>
</head>
<body class="has-cover">
    <div class="gallery-fix"></div> 
    <div class="header header-light">
        <a href="#" class="open-menu"><i class="fa fa-navicon"></i></a>
        <div class="h3">Selamat Datang</div>
    </div>
    <div class="navigation navigation-light">
        <div class="navigation-scroll">
            <a href="<?php echo base_url(); ?>" class="menu-item"><i class="fa fa-user"></i><em>Login</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/1'); ?>" class="menu-item"><i class="fa fa-building"></i><em>Tentang Kami</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/2'); ?>" class="menu-item"><i class="fa fa-cube"></i><em>Panduan</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/3'); ?>" class="menu-item"><i class="fa fa-phone"></i><em>Kontak</em><i class="fa fa-circle"></i></a>
            <a href="#" class="menu-item close-menu"><i class="fa fa-times-circle"></i><em>Close</em><i class="fa fa-circle"></i></a>
        </div>
    </div>
    <div id="page-content" class="bg-1">
        <div id="page-content-scroll">
            <h3 class="header-mask header-mask-light">Welcome</h3>
            <div class="content">
                <div class="decoration"></div>
                <div class="page-login full-bottom">
                    <a href="<?php echo base_url(); ?>" class="page-login-logo"></a>
                    <?php echo validation_errors(); ?>
                    <form action="<?php echo base_url('welcome/aksi_login'); ?>" method="post" id="forms">	
                        <div class="login-input">
                            <i class="fa fa-user"></i>
                            <input required="" type="text" value="" onfocus="if (this.value == 'Username')
                                        this.value = ''" onblur="if (this.value == '')
                                                    this.value = 'Username'" name="username">
                                   <?php echo form_error('Username'); ?>
                        </div>
                        <div class="login-password">
                            <i class="fa fa-lock"></i>
                            <input required="" type="password" value="" onfocus="if (this.value == 'password')
                                        this.value = ''" onblur="if (this.value == '')
                                                    this.value = 'password'" name="password">
                                   <?php echo form_error('password'); ?>
                        </div>
                        <a href="#" class="login-forgot"></a>
                        <a href="#" class="login-create"></a>
                        <div class="clear"></div>
                        <button type="submit" class="login-button button button-xl button-green button-fullscreen">Login</button>
                    </form>
                    <div class="decoration"></div>
                    <div class="user-feed" style="padding-left: 25%">
                        <img class="user-feed-image preload-image" data-original="<?php echo image() ?>pln3.png" alt="img" src="<?php echo image() ?>pln3.png" style="display: block; border-radius: 0px !important">
                        <h5>BASCAMP</h5>
                        <em>SEMARANG</em>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="decoration"></div>
            </div>
            <div class="footer">
                <p class="footer-strip">Copyright <span id="copyright-year"></span> Si Haji. All Rights Reserved</p>
            </div>
            <div class="footer-clear"></div>
        </div>
    </div>
    <div class="share-bottom-tap-close"></div>
    <script type="text/javascript">
        first = $(location).attr('pathname');
        first.indexOf(1);
        first.toLowerCase();
        first = first.split("/")[1];
        base_url = window.location.origin;
        $(document).ready(function () {
            $("#forms").on("submit", function (event) {
                event.preventDefault();
                $.ajax({
                    url: $("#forms").attr('action'),
                    type: 'POST',
                    dataType: "JSON",
                    timeout: 10000,
                    data: $("#forms").serialize(),
                    success: function (data) {
                        if (data.back === 'true') {
                            swal({
                                title: 'Login Berhasil',
                                text: data.msg,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                            });
                            setTimeout(function () {
                                window.location.href = base_url + "/" + first + '/welcome/menu';
                            }, 1000)
                        } else {
                            swal({
                                title: 'Login Gagal',
                                text: data.msg,
                                type: 'error',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Ok!'
                            });
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000)
                        }
                    }
                    , error: function (x, t, m) {
                        if (t === "timeout") {
                            $("#form-info").html('Connection time out').fadeIn("slow");
                        } else {
                            $("#form-info").html(m).fadeIn("slow");
                        }
                        setTimeout(function () {
                            $("#form-info").fadeOut("slow");
                            $("#simpan").button('reset');
                        }, 3000);
                    }
                });
            });
        });
    </script>
</body>