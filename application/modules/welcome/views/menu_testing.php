<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0 minimal-ui"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/android-chrome-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" sizes="196x196" href="<?php echo image() ?>/splash/apple-touch-icon-196x196.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo image() ?>/splash/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo image() ?>/splash/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo image() ?>/splash/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo image() ?>/splash/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo image() ?>/splash/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo image() ?>/splash/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo image() ?>/splash/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo image() ?>/splash/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo image() ?>/splash/apple-touch-icon-57x57.png">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-16x16.png" sizes="16x16">
    <link rel="shortcut icon" href="<?php echo image() ?>/splash/favicon.ico" type="image/x-icon"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
    <title>Sihaji 1.0</title>
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>framework.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>font-awesome.css">
    <script type="text/javascript" src="<?php echo js() ?>jquery.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>plugins.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>custom.js"></script>
</head>
<body class="has-cover">
    <div class="gallery-fix"></div> 
    <div class="header header-light">
        <a href="#" class="open-menu"><i class="fa fa-navicon"></i></a>
        <div class="h3">TESTING MODE</div>
        <a href="#" onclick="window.history.go(-1);
                return false;" title="Back"><i class="fa fa-chevron-left"></i></a>
    </div>
    <div class="navigation navigation-light">
        <div class="navigation-scroll">
            <a href="<?php echo base_url('welcome/menu'); ?>" class="menu-item"><i class="fa fa-home"></i><em>Main Menu</em><i class="fa fa-circle"></i></a>
            <?php if (session('level_user') == 'admin') { ?>
                <a href="<?php echo base_url('user'); ?>" class="menu-item"><i class="fa fa-cogs"></i><em>Kelola User</em><i class="fa fa-circle"></i></a>
            <?php } ?>
            <a href="<?php echo base_url('welcome/info/1'); ?>" class="menu-item"><i class="fa fa-building"></i><em>Tentang Kami</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/2'); ?>" class="menu-item"><i class="fa fa-cube"></i><em>Panduan</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/3'); ?>" class="menu-item"><i class="fa fa-phone"></i><em>Kontak</em><i class="fa fa-circle"></i></a>
            <a href="#" class="menu-item close-menu"><i class="fa fa-times-circle"></i><em>Close</em><i class="fa fa-circle"></i></a>
        </div>
    </div>
    <div id="page-content" class="bg-1">
        <div id="page-content-scroll">
            <h3 class="header-mask header-mask-light">Welcome</h3>
            <div class="content-strip">
                <div class="strip-content">
                    <h4><?php echo $judul; ?></h4>
                    <p><?php echo $sub_judul; ?></p>
                    <i class="fa fa-cogs"></i>
                </div>
                <div class="overlay"></div>
                <img class="preload-image" data-original="<?php echo image() ?>/pictures/9.jpg" alt="img">
            </div>
            <div class="content">
                <div class="decoration"></div>
                <div class="one-third-responsive">
                    <div class="container half-bottom">
                        <a href="<?php echo site_url('mode_pmt/pmt'); ?>" class="button twitter-color button-fullscreen" style="font-size: 16px">PMT</a>
                        <a href="<?php echo site_url('mode_ct/ct'); ?>" class="button twitter-color button-fullscreen" style="font-size: 16px">CT</a>
                        <a href="<?php echo site_url('mode_pt/pt'); ?>" class="button mail-color button-fullscreen" style="font-size: 16px">PT</a>
                    </div>
                </div>
                <div class="one-third-responsive">
                    <div class="container half-bottom">
                        <a href="#" class="button mail-color button-fullscreen" style="font-size: 16px">CVT</a>
                        <a href="#" class="button button-blue button-fullscreen" style="font-size: 16px">LA</a>
                        <a href="#" class="button button-blue button-fullscreen" style="font-size: 16px">TRAFO</a>
                    </div>
                </div>
                <div class="one-third-responsive last-column">
                    <div class="container half-bottom">
                        <a href="#" class="button facebook-color button-fullscreen" style="font-size: 16px">PMS LINE</a>
                        <a href="#" class="button facebook-color button-fullscreen" style="font-size: 16px">PMS BUS 1</a>
                        <a href="#" class="button tumblr-color button-fullscreen" style="font-size: 16px">PMS BUS 2</a>
                    </div>
                </div>
                <div class="decoration"></div>
            </div>
            <div class="footer">
                <p class="footer-strip">Copyright <span id="copyright-year"></span> Silvanix.com. All Rights Reserved</p>
            </div>
            <div class="footer-clear"></div>
        </div>
    </div>
    <a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i>Back to top</a>
</body>