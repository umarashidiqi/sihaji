<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0 minimal-ui"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/android-chrome-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" sizes="196x196" href="<?php echo image() ?>/splash/apple-touch-icon-196x196.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo image() ?>/splash/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo image() ?>/splash/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo image() ?>/splash/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo image() ?>/splash/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo image() ?>/splash/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo image() ?>/splash/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo image() ?>/splash/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo image() ?>/splash/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo image() ?>/splash/apple-touch-icon-57x57.png">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-16x16.png" sizes="16x16">
    <link rel="shortcut icon" href="<?php echo image() ?>/splash/favicon.ico" type="image/x-icon"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
    <title>Sihaji 1.0</title>
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>framework.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>font-awesome.css">
    <script type="text/javascript" src="<?php echo js() ?>jquery.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>plugins.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>custom.js"></script>
</head>
<body class="has-cover">
    <div class="gallery-fix"></div> 
    <div class="header header-light">
        <a href="#" class="open-menu"><i class="fa fa-navicon"></i></a>
        <div class="h3">Menu INFO</div>
         <a href="#" onclick="window.history.go(-1);
                return false;" title="Back"><i class="fa fa-chevron-left"></i></a>
    </div>
    <div class="navigation navigation-light">
        <div class="navigation-scroll">
            <a href="<?php echo base_url('welcome'); ?>" class="menu-item"><i class="fa fa-user"></i><em>Login</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/1'); ?>" class="menu-item"><i class="fa fa-building"></i><em>Tentang Kami</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/2'); ?>" class="menu-item"><i class="fa fa-cube"></i><em>Panduan</em><i class="fa fa-circle"></i></a>
            <a href="<?php echo base_url('welcome/info/3'); ?>" class="menu-item"><i class="fa fa-phone"></i><em>Kontak</em><i class="fa fa-circle"></i></a>
            <a href="#" class="menu-item close-menu"><i class="fa fa-times-circle"></i><em>Close</em><i class="fa fa-circle"></i></a>
        </div>
    </div>
    <div id="page-content" class="bg-1">
        <div id="page-content-scroll">
            <?php if ($jenis == 1) { ?>
                <div class="splash-screen cover-screen container-fullscreen no-bottom" style="background-image:url(<?php echo image() ?>/bg/2.jpg);">
                    <div class="splash-content">
                        <a href="#" class="splash-logo scale-hover"><img class="preload-image" data-original="<?php echo image('pages-logo-dark.pngs') ?>" alt="img"></a>
                        <h4><strong class="color-yellow-light">Si Haji v1.0</strong></h4>
                        <h5>Sistem Informasi Hasil Pengujian</h5>
                        <p>
                            Prototype Perangkat Lunak
                            STO PLN 53
                            Sistem Informasi Hasil Pengujian
                            APP Semarang
                            Basecamp Semarang
                            Unit Transmisi Jawa Bagian Tengah
                            PT. PLN (PERSERO)
                        </p>
                        <a class="splash-button" href="<?php echo base_url() ?>">Get started</a>
                    </div> 
                    <div class="overlay"></div>
                </div> 
            <?php } else if ($jenis == 2) { ?>
                <div class="splash-screen cover-screen container-fullscreen no-bottom" style="background-image:url(<?php echo image() ?>/bg/2.jpg);">
                    <div class="splash-content">
                        <a href="#" class="splash-logo scale-hover"><img class="preload-image" data-original="<?php echo image('pages-logo-dark.pngs') ?>" alt="img"></a>
                        <h4><strong class="color-red-light">P A N D U A N</strong></h4>
                        <h5>Panduan Penggunaan Sistem Si Haji</h5>
                        <p>
                            Untuk dapat mengakses, silahkan login dengan user ID dan password yang anda miliki.
                        </p>
                    </div> 
                    <div class="overlay"></div>
                </div> 
            <?php } else if ($jenis == 3) { ?>
                <div class="content-fullscreen">
                    <iframe class="responsive-image maps" src="https://maps.google.com/?ie=UTF8&amp;ll=-7.030404,110.417333&amp;spn=0.006186,0.016512&amp;t=h&amp;z=17&amp;output=embed"></iframe>
                    <a href="#" class="button button-red button-fullscreen uppercase ultrabold">FullScreen Map</a>
                </div>
                <div class="content-strip" style="padding-left: 10px;padding-right: 10px; padding-top: 0px;">
                    <p class="center-text"><strong>Informasi dan Kontak</strong></p>
                    <div class="user-item">
                        <img class="user-item-image preload-image" data-original="<?php echo image(); ?>pictures/user.png" alt="img">
                        <h5>Aulia Ari Adhha</h5>
                        <em>085879851400</em>
                        <a href="#" class="user-item-icon-1 facebook-color scale-hover"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="user-item-icon-2 twitter-color scale-hover"><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="user-item">
                        <img class="user-item-image preload-image" data-original="<?php echo image(); ?>pictures/user.png" alt="img">
                        <h5>Luthfi Fakhrudin Nizar</h5>
                        <em>085645218625</em>
                        <a href="#" class="user-item-icon-1 facebook-color scale-hover"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="user-item-icon-2 twitter-color scale-hover"><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="user-item">
                        <img class="user-item-image preload-image" data-original="<?php echo image(); ?>pictures/user.png" alt="img">
                        <h5>Raden Rizky Cahya Darmawan</h5>
                        <em>085647720964</em>
                        <a href="#" class="user-item-icon-1 facebook-color scale-hover"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="user-item-icon-2 twitter-color scale-hover"><i class="fa fa-twitter"></i></a>
                    </div>
                </div> 
            <?php } ?>
        </div>
        <div class="footer">
            <p class="footer-strip">Copyright <span id="copyright-year"></span> Silvanix.com. All Rights Reserved</p>
        </div>
        <div class="footer-clear"></div>
    </div>
</div>
<a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i>Back to top</a>
</body>