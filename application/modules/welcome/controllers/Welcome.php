<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Welcome_m');
        $this->load->library('form_validation');
    }

    public function index() {
        if ($this->session->userdata('status') == "login") {
            redirect(site_url('welcome/menu'), 'refresh');
        }
        $this->data['hai'] = "SELAMAT DATANG!";
        $this->dataTema = 'welcome_message';
        $this->noview();
    }

    public function menu() {
        $this->valid_login();
        $this->dataTema = 'menu';
        $this->noview();
    }

    public function option($param) {
        $this->valid_login();
        $this->data['judul'] = "BAY OPTION";
//        $data_session_option = array(
//            'gi_option' => "SRONDOL"
//        );

        if ($param == 1) {
            $this->session->set_userdata('gi_option', 'SRONDOL');
            $this->data['sub_judul'] = "GI SRONDOL";
            $this->data['option'] = array("BAY TRAFO 1", "BAY TRAFO 2", "BAY PANDEAN LAMPER 1", "BAY PANDEAN LAMPER 2", "BAY KRAPYAK 1", "BAY KRAPYAK 2", "BAY KOPEL");
        } else if ($param == 2) {
            $this->session->set_userdata('gi_option', 'PUNDUK PAYUNG');
            $this->data['sub_judul'] = "GIS PUNDUK PAYUNG";
            $this->data['option'] = array("BAY TRAFO 1", "BAY TRAFO 2", "BAY KOPEL", "BAY PANDEAN LAMPER 1", "BAY PANDEAN LAMPER 2", "BAY UNGARAN 1", "BAY UNGARAN 2");
        } else if ($param == 3) {
            $this->session->set_userdata('gi_option', 'MRANGGEN');
            $this->data['sub_judul'] = "GI MRANGGEN";
            $this->data['option'] = array("BAY PURWODADI", "BAY PURWODADI 2", "BAY UNGARAN 1", "BAY UNGARAN 2", "BAY TRAFO 1", "BAY TRAFO 2", "BAY KOPEL");
        } else if ($param == 4) {
            $this->session->set_userdata('gi_option', 'KRAPYAK');
            $this->data['sub_judul'] = "GI KRAPYAK";
            $this->data['option'] = array("BAY KOPEL", "BAY SRONDOL 1", "BAY SRONDOL 2", "BAY TRAFO 1", "BAY TRAFO 2", "BAY TRAFO 3", "BAY RANDU GARUT 1", "BAY RANDU GARUT 2", "BAY BSB 1", "BAY BSB 2", "BAY UNGARAN", "BAY KALISARI", "BAY TAMBAK LOROK");
        } else if ($param == 5) {
            $this->session->set_userdata('gi_option', 'PANDEAN LAMPER');
            $this->data['sub_judul'] = "GI PANDEAN LAMPER";
            $this->data['option'] = array("BAY SRONDOL 1", "BAY SRONDOL 2", "BAY PUDAK PAYUNG 1", "BAY PUDAK PAYUNG 2", "BAY TRAFO 1", "BAY TRAFO 2", "BAY TRAFO 3", "BAY TAMBAK LOROK 1", "BAY TAMBAK LOROK 2", "BAY SIMPANG LIMA", "BAY KOPEL");
        } else if ($param == 6) {
            $this->session->set_userdata('gi_option', 'WELERI');
            $this->data['sub_judul'] = "GI WELERI";
            $this->data['option'] = array("BAY BATANG 1", "BAY BATANG 2", "BAY KALIWUNGU 1", "BAY KALIWUNGU 2", "BAY TRAFO 1", "BAY TRAFO 2", "BAY KOPEL");
        } else if ($param == 7) {
            $this->session->set_userdata('gi_option', 'KALIWUNGU');
            $this->data['sub_judul'] = "GI KALIWUNGU";
            $this->data['option'] = array("BAY POLYSINDO 1", "BAY POLYSINDO 2", "BAY RANDU GARUT 1", "BAY RANDU GARUT 2", "BAY TRAFO 1", "BAY TRAFO 2", "BAY KOPEL", "BAY WALERI 1", "BAY WALERI 2");
        } else if ($param == 8) {
            $this->session->set_userdata('gi_option', 'SAYUNG');
            $this->data['sub_judul'] = "GI SAYUNG";
            $this->data['option'] = array("BAY TRAFO 1", "BAY TRAFO 2", "BAY TRAFO 3", "BAY KUDUS 1", "BAY KUDUS 2", "BAY TAMBAK LOROK 1", "BAY TAMBAK LOROK 2", "BAY KOPEL");
        } else if ($param == 9) {
            $this->session->set_userdata('gi_option', 'RANDU GARUT');
            $this->data['sub_judul'] = "GI RANDU GARUT";
            $this->data['option'] = array("BAY TRAFO 1", "BAY TRAFO 2", "BAY KALIWUNGU 1", "BAY KALIWUNGU 2", "BAY KRAPYAK 1", "BAY KRAPYAK 2", "BAY KOPEL");
        } else if ($param == 10) {
            $this->session->set_userdata('gi_option', 'TAMBAK LOROK');
            $this->data['sub_judul'] = "GI TAMBAK LOROK";
            $this->data['option'] = array("BAY BAWEN", "BAY SAYUNG", "BAY LAMPER", "BAY KALISARI", "BAY KRAPYAK", "BAY TRAFO");
        } else if ($param == 11) {
            $this->session->set_userdata('gi_option', 'KALI SARI');
            $this->data['sub_judul'] = "GI KALI SARI";
            $this->data['option'] = array("BAY TRAFO 1", "BAY TRAFO 2", "BAY SIMPANG LIMA 1", "BAY SIMPANG LIMA 2", "BAY TAMBAK LOROK 1", "BAY TAMBAK LOROK 2", "BAY KRAPYAK 1", "BAY KRAPYAK 2");
        } else if ($param == 12) {
            $this->session->set_userdata('gi_option', 'SIMPANG LIMA');
            $this->data['sub_judul'] = "GI SIMPANG LIMA";
            $this->data['option'] = array("BAY TRAFO 1", "BAY TRAFO 2", "BAY KALISARI 1", "BAY KALISARI 2", "BAY PANDEAN LAMPER 1", "BAY PANDEAN LAMPER 2", "BAY KOPEL");
        } else if ($param == 13) {
            $this->session->set_userdata('gi_option', 'UNGARAN');
            $this->data['sub_judul'] = "GI UNGARAN";
            $this->data['option'] = array("BAY TRAFO", "BAY JELOK", "BAY BAWEN", "BAY MRANGGEN", "BAY BSB", "BAY KRAPYAK", "BAY PUDAK PAYUNG");
        } else if ($param == 14) {
            $this->session->set_userdata('gi_option', 'UNGARAN');
            $this->data['sub_judul'] = "GITET UNGARAN";
            $this->data['option'] = array("BAY REACTOR", "BAY IBT", "BAY PMT & CT", "BAY TANJUNG JATI", "BAY BUSBAR", "BAY NGIMBANG");
        } else if ($param == 15) {
            $this->session->set_userdata('gi_option', 'BUMI SEMARANG BARU');
            $this->data['sub_judul'] = "GI BUMI SEMARANG BARU";
            $this->data['option'] = array("BAY TRAFO 1", "BAY TRAFO 2", "BAY KRAPYAK 1", "BAY KRAPYAK 2", "BAY UNGARAN 1", "BAY UNGARAN 2", "BAY KOPEL");
        }
//        echo 'sesione' . session('gi_option');
        $this->dataTema = 'menu_option';
        $this->noview();
    }

    public function testing($param) {
        $this->valid_login();
        $param = strtoupper(str_replace("_", " ", $param));
        $this->data['judul'] = $param;
        $exp = explode('BAY ', $param);

        if ($exp[1] == 'PANDEAN LAMPER 1') {
            $this->session->set_userdata('bay_option', 'PDLAM 1');
        } elseif ($exp[1] == 'PANDEAN LAMPER 2') {
            $this->session->set_userdata('bay_option', 'PDLAM 2');
        } else {
            $this->data['judul_baynya'] = $exp[1];
            $this->session->set_userdata('bay_option', $this->data['judul_baynya']);
        }
        $this->data['sub_judul'] = "TESTING MODE";
        $this->dataTema = 'menu_testing';
        $this->noview();
    }

    public function info($param = null) {
        $this->data['jenis'] = $param;
        $this->dataTema = 'menu_info';
        $this->noview();
    }

    function aksi_login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
            'username' => $username,
            'password' => md5($password)
        );
        $cek = $this->Welcome_m->view("user", $where);
        if (!empty($cek)) {
            $data_session = array(
                'username' => $username,
                'status' => "login",
                'id_user' => $cek[0]->id,
                'level_user' => $cek[0]->level
            );
//            print_r_pre($data_session);
//            die();
            $this->session->set_userdata($data_session);
//            print_r_pre($data_session);
            $back = "true";
            $msg = "Selamat Datang Kembali.";
            echo json_encode(array('back' => $back, 'msg' => $msg));
        } else {
            $back = "false";
            $msg = "Username atau password salah.";
            echo json_encode(array('back' => $back, 'msg' => $msg));
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        $this->session->set_userdata(array('login' => false));
        redirect(base_url(), 'refresh');
    }

}
