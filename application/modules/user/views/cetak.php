<div class="col-md-12">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="text-center">PREVIEW CETAK SPJ</h4>
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <a class="no-print" href="javascript:printDiv('print-area-2');" href="">
                    <button type="button" class="btn btn-panjang btn-primary pull-right"><i class="fa fa-print"></i> Cetak</button>
                </a>
                <a href="#">
                    <button type="button" class="btn btn-panjang btn-success pull-right download" style="margin-right: 10px"><i class="fa fa-file-excel-o"></i> EX. Excel</button>
                </a>
                <script type="text/javascript">
                    function printDiv(elementId) {
                        var a = document.getElementById('printing-css').value;
                        var b = document.getElementById(elementId).innerHTML;
                        window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
                        window.frames["print_frame"].window.focus();
                        window.frames["print_frame"].window.print();
                    }
                </script>
                <style type="text/css">
                    .print-area{
                        padding-top: 1em;
                    }
                    td { 
                        padding: 5px;
                    }
                    .table {
                        border-collapse: collapse !important;
                        font-size: 12px !important;
                    }
                    .table td,
                    .table th {
                        background-color: #fff !important;
                    }
                    .text-left {text-align: left;}
                    .text-center {text-align: center;}
                    .text-right {text-align: right;}
                    .txt-j {text-align: justify;}
                    .tahoma10 {

                        font-size: 10px;
                        text-decoration: none;
                    }
                    .header20 {

                        font-size: 20px;
                        text-decoration: none;
                        padding: 10px;
                        margin: 10px;
                    }
                    .tahoma11 {

                        font-size: 11px;
                        text-decoration: none;
                        margin: 2px;
                        padding: 2px;
                        border-top: 1px;
                    }
                    .tahoma16 {

                        font-size: 16px;
                        text-decoration: none;
                    }
                    .border-tlb {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-right-style: none !important;
                        border-bottom-style: solid !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-tl {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-right-style: none !important;
                        border-bottom-style: none !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-lb {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: none !important;
                        border-right-style: none !important;
                        border-bottom-style: solid !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-lr {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: none !important;
                        border-right-style: solid !important;
                        border-bottom-style: none !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-r {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: none !important;
                        border-right-style: solid !important;
                        border-bottom-style: none !important;
                        border-left-style: none !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-l {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: none !important;
                        border-right-style: none !important;
                        border-bottom-style: none !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-tlbr {
                        border: 1.2px solid #000000 !important;
                    }
                    .border-lbr {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: none !important;
                        border-right-style: solid !important;
                        border-bottom-style: solid !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-tb {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-right-style: none !important;
                        border-bottom-style: solid !important;
                        border-left-style: none !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-tlb {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-bottom-style: solid !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-tbr {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-right-style: solid !important;
                        border-bottom-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-tlr {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-right-style: solid !important;
                        border-left-style: solid !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-br {
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-right-style: solid !important;
                        border-bottom-style: solid !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                    }
                    .border-tr {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-right-style: solid !important;
                        border-bottom-style: none !important;
                        border-left-style: none !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-b {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: none !important;
                        border-right-style: none !important;
                        border-bottom-style: solid !important;
                        border-left-style: none !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .border-t {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: solid !important;
                        border-right-style: none !important;
                        border-bottom-style: none !important;
                        border-left-style: none !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .txt-b {
                        font-weight: bold  !important;
                    }
                    .txt-u {
                        text-decoration: underline !important;
                    }
                    .txt-i {
                        font-style: italic !important;
                    }
                    .border-missing {
                        border-top-width: 1.2px !important;
                        border-right-width: 1.2px !important;
                        border-bottom-width: 1.2px !important;
                        border-left-width: 1.2px !important;
                        border-top-style: none !important;
                        border-right-style: none !important;
                        border-bottom-style: none !important;
                        border-left-style: none !important;
                        border-top-color: #000000 !important;
                        border-right-color: #000000 !important;
                        border-bottom-color: #000000 !important;
                        border-left-color: #000000 !important;
                    }
                    .space {margin-left: 50px; text-align: right; width: 300px}
                    .jarak {height: 7px !important}
                    .tp {
                        height: 7px !important; 
                    }
                </style>
                <div id="print-area-2" class="print-area col-md-12">
                    <form class="form-horizontal" action="#" method="post" id="collapseCari">
                        <div class="form-group">
                            <div class="col-md-offset-5 col-md-3">
                                <label for="Awal">Tanggal Awal</label>
                                <input type="text" class="form-control" placeholder="Masukkan Tanggal Awal">
                            </div>
                            <div class="col-md-3">
                                <label for="Akhir">Tanggal Akhir</label>
                                <input type="text" class="form-control" placeholder="Masukkan Tanggal Akhir">
                            </div>
                            <div class="col-md-1">
                                <label for="keyword">&nbsp;</label>
                                <button type="submit" class="btn btn-info form-control" title="Buka"><i class="fa fa-search"></i> Buka</button>
                            </div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <!--                    <table class="table table-no-bordered" cellspacing="0" width="100%" border="0" style="font-size: 14px;">
                                            <tr>
                                                <td colspan="4" class="text-center txt-b">DRAF</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-center txt-b">PROVINSI/KABUPATEN/KOTA ................</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-center txt-b">SURAT KONTRAK X</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-center txt-b">Nomor: .................................</td>
                                            </tr>
                                        </table>-->
                    <table class="table table-bordered" cellspacing="0" width="100%" border="0" style="font-size: 14px;">
                        <tr>
                            <td colspan="3" class="text-center txt-b">[ISI]</td>
                        </tr>
                    </table>
                    <table class="table table-no-bordered" cellspacing="0" width="100%" border="0" style="font-size: 14px;">
                        <tr>
                            <td colspan="3">*) Coret yang tidak perlu</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <dd>Jika Cetak Dokumen Final, kata <code>Draf</code> dihilangkan.</dd>
                    <br/>
                    <dd>Sub Teks tidak perlu ditampilkan</dd>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>