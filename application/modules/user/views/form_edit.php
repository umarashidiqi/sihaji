<div class=" col-md-12">
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="col-lg-12">
                <form class="form-horizontal" id="ct" action="<?php echo @$action ?>" method="post"
                      data-fv-framework="bootstrap"
                      data-fv-message="Form ini tidak boleh kosong!"
                      data-fv-feedbackicons-valid="glyphicon glyphicon-ok"
                      data-fv-feedbackicons-invalid="glyphicon glyphicon-remove"
                      data-fv-feedbackicons-validating="glyphicon glyphicon-refresh"
                      >
                    <div style="text-align: center">
                        <b>CHECK LIST   CT ( CURRENT TRANSFORMER ) 150 kV</b>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="Tanggal">Tahun</label>
                            <select class="form-control" name="tahun">
                                <option value="2016"<?php
                                if ($data_umum[0]->tahun == '2016') {
                                    echo 'selected';
                                }
                                ?>>2016</option>
                                <option value="2015"<?php
                                if ($data_umum[0]->tahun == '2015') {
                                    echo 'selected';
                                }
                                ?>>2015</option>
                                <option value="2014"<?php
                                if ($data_umum[0]->tahun == '2014') {
                                    echo 'selected';
                                }
                                ?>>2014</option>
                            </select>
                        </div> <div class="col-md-4">
                            <label for="Tanggal">Nomor</label>
                            <input type="text" name="nomor" id="nomor" class="form-control" placeholder="Masukkan Tanggal..." value="<?php echo $data_umum[0]->nomor ?>">
                        </div>
                        <div class="col-md-4">
                            <label for="Tanggal">Tanggal</label>
                            <div class="input-group input-append date" id="tanggal">
                                <input type="date" name="tanggal" id="tanggal" class="form-control" placeholder="Masukkan Tanggal..." value="<?php echo $data_umum[0]->tanggal ?>">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Merk</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="merk" type="text" name="merk" class="form-control" placeholder="Masukkan NIP Penerima..." value="<?php echo $data_umum[0]->merk ?>">
                                </div>
                                <div class="col-md-1">
                                    <label>Bay</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="bay" id="bay" type="text" name="bay" class="form-control" placeholder="" value="<?php echo $data_umum[0]->bay ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Type</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="type" type="text" name="type" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>No. Serie</label>
                                </div>
                                <div class="col-md-1">
                                    <label>Fasa R</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="fasa_r" type="text" name="fasa_r" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type_fasa_r ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>Fasa S</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="fasa_s" type="text" name="fasa_s" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type_fasa_s ?>">
                                </div>
                                <div class="col-md-1">
                                    <label>Lokasi GI</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="lokasi_gi" type="text" name="lokasi_gi" class="form-control" placeholder="Masukan" value="<?php echo $data_umum[0]->lokasi_gi ?>">
                                </div>


                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>Fasa T</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="fasa_t" type="text" name="fasa_t" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type_fasa_t ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Ratio Core 1</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="ratio_1" type="text" name="ratio_1" class="form-control" placeholder="" value="<?php echo $data_umum[0]->ratio_core_1 ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Ratio Core 2</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="ratio_2" type="text" name="ratio_2" class="form-control" placeholder="" value="<?php echo $data_umum[0]->ratio_core_2; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Ratio Core 3</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="ratio_3" type="text" name="ratio_3" class="form-control" placeholder="" value="<?php echo $data_umum[0]->ratio_core_3; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Ratio Core 4</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="ratio_4" type="text" name="ratio_4" class="form-control" placeholder="" value="<?php echo $data_umum[0]->ratio_core_4; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Ratio Core 5</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="ratio_5" type="text" name="ratio_5" class="form-control" placeholder="" value="<?php echo $data_umum[0]->ratio_core_5; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div><b>
                            I. Check List :
                        </b></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle" class="text-center">No</th>
                                    <th style="vertical-align: middle" class="text-center">Peralatan Yang Diperiksa</th>
                                    <th colspan="2" style="vertical-align: middle" class="text-center">Kondisi Awal</th>
                                    <th colspan="2" style="vertical-align: middle" class="text-center">Kondisi Akhir</th>
                                </tr>
                            </thead>
                            <tbody id="data">
                                <?php
                                foreach ($data_edit as $value) {
                                    $exp = explode('.', $value->kode);
                                    $jml = count($exp);
                                    if ($jml == 1) {
                                        ?>
                                        <tr>
                                            <td><b><?php echo $value->kode; ?></b></td>
                                            <td colspan="5"><b><?php echo $value->nama_alat; ?></b></td>
                                        </tr>
                                        <?php
                                    } elseif ($jml == 2) {
                                        ?>
                                        <tr>
                                            <td><input type="hidden" name="idnyak[]" value="<?php echo $value->id; ?>"></td>
                                            <td><?php echo $value->kode . ' ' . $value->nama_alat; ?></td>
                                            <td>
                                                <?php
                                                if ($value->k1 == '') {
                                                    echo ' <input type="checkbox"  name="k1_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k1_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat1; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($value->k2 == '') {
                                                    echo ' <input type="checkbox"  name="k2_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k2_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat2; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($value->k3 == '') {
                                                    echo ' <input type="checkbox"  name="k3_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k3_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat3; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($value->k4 == '') {
                                                    echo ' <input type="checkbox"  name="k4_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k4_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat4; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <tr>
                                    <td>7</td>
                                    <td>Catatan</td>
                                    <td colspan="8">
                                        <textarea  class="form-control" name="catatan1"><?php echo $catatan[0]->uraian_checklist; ?></textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="text-align: center">
                        <b>HASIL PENGUJIAN   CT ( CURRENT TRANSFORMER ) 150KV</b>
                    </div>
                    <div><b>
                            II. Hasil Pengukuran :
                        </b></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
<!--                                <tr>
                                    <th colspan="2" rowspan="2" style="vertical-align: middle" class="text-center">Titik Ukur</th>
                                    <th colspan="3" style="vertical-align: middle" class="text-center">Fasa R</th>
                                    <th colspan="3" style="vertical-align: middle" class="text-center">Fasa S</th>
                                    <th colspan="3" style="vertical-align: middle" class="text-center">Fasa T</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle" class="text-center">Standart</th>
                                    <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                    <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                    <th style="vertical-align: middle" class="text-center">Standart</th>
                                    <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                    <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                    <th style="vertical-align: middle" class="text-center">Standart</th>
                                    <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                    <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                </tr>-->
                            </thead>
                            <tbody id="data">
                                <tr>
                                    <td><b>1</b></td>
                                    <td colspan="10"><b>Tahanan Isolasi belitan</b></td>
                                </tr>
                                <?php
                                $no_1 = 1;
                                $no_2 = 1;
                                $no_3 = 7;
                                $no_4 = 1;
//                                $n = 1;
                                foreach ($data_uji as $value) {
                                    $exp = explode('.', $value->kode);
                                    $jml = count($exp);

                                    if ($value->pembeda == 1) {
                                        //untuk pertama foreachnya
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                                <?php echo $no_1; ?>
                                            </td>
                                            <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                            <td <?php // echo "rowspan='" . $n++ . "'"      ?>>1 KV / 1 MW</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value="<?php echo $value->fasa_r_hu; ?>"></td>
                                            <td>1 KV / 1 MW</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value="<?php echo $value->fasa_s_hu; ?>"></td>
                                            <td>1 KV / 1 MW</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value="<?php echo $value->fasa_t_hu; ?>"></td>
                                        </tr>
                                        <?php
                                        $no_1++;
                                    } elseif ($jml == 2 and $value->kode == '1.1') {
                                        //injeksi teg 1
                                        ?>
                                        <tr>
                                            <td><?php // echo 'ap';   ?></td>
                                            <td><b><?php echo $value->kode . ' ' . $value->titik_ukur; ?></b></td>
                                            <td colspan="9"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value="<?php echo $value->injeksi_alat_uji; ?>"></td>
                                        </tr>
                                        <tr>
                                            <th colspan="2" rowspan="2" style="vertical-align: middle" class="text-center">Titik Ukur</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa R</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa S</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa T</th>
                                        </tr>
                                        <tr>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                        </tr>
                                        <?php
                                        $no_2++;
                                    } elseif ($jml == 2 and $value->kode == '1.2') {
                                        //injeksi teg 2
                                        ?>
                                        <tr>
                                            <td><?php // echo 'ape';   ?></td>
                                            <td><b><?php echo $value->kode . ' ' . $value->titik_ukur; ?></b></td>
                                            <td colspan="9"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value="<?php echo $value->injeksi_alat_uji; ?>"></td>
                                        </tr>
                                        <?php
                                        $no_2++;
                                    } elseif ($value->pembeda == 2) {
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                                <?php echo $no_3; ?>
                                            </td>
                                            <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                            <td>1 KV / 1 MW</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value="<?php echo $value->fasa_r_hu; ?>"></td>
                                            <td>1 KV / 1 MW</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value="<?php echo $value->fasa_s_hu; ?>"></td>
                                            <td>1 KV / 1 MW</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value="<?php echo $value->fasa_t_hu; ?>"></td>
                                       </tr>
                                        <?php
                                        $no_3++;
                                    } elseif ($value->pembeda == 3) {
                                        //alat uji
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                            </td>
                                            <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>

                                            <td colspan="9"><input class="form-control" type="text" name="hu3_<?php echo $value->id; ?>" value="<?php echo $value->injeksi_alat_uji; ?>"></td>
                                        </tr>
                                        <?php
                                    } elseif ($value->pembeda == 4) {
                                        //Tahanan Pentanahan
                                        ?>
                                        <tr>
                                            <td><b>2</b></td>
                                            <td colspan="10"><b><?php echo $value->titik_ukur; ?></b></td>
                                        </tr>
                                        <tr>
                                            <th colspan="2" rowspan="2" style="vertical-align: middle" class="text-center">Titik Ukur</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa R</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa S</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa T</th>
                                        </tr>
                                        <tr>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                        </tr>
                                        <?php
                                    } elseif ($value->pembeda == 5) {
                                        //tahanan input
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                                <?php // echo $no_3;  ?>
                                            </td>
                                            <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                            <td>< 1 W</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value="<?php echo $value->fasa_r_hu; ?>"></td>
                                            <td>< 1 W</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value="<?php echo $value->fasa_s_hu; ?>"></td>
                                            <td>< 1 W</td>
                                            <td></td>
                                            <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value="<?php echo $value->fasa_t_hu; ?>"></td>
                                       </tr>
                                        <?php
                                    } elseif ($value->pembeda == 6) {
                                        //tahanan input
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                            </td>
                                            <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>

                                            <td colspan="9"><input class="form-control" type="text" name="hu3_<?php echo $value->id; ?>" value="<?php echo $value->injeksi_alat_uji; ?>"></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                }
                                ?>
                                <tr>
                                    <td><b>3</b></td>
                                    <td><b>Catatan</b></td>
                                    <td colspan="9">
                                        <textarea  class="form-control" name="catatan2"><?php echo $catatan[0]->uraian_uji; ?></textarea>
                                    </td>
                                </tr>

                                <?php ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th style="vertical-align: middle" class="text-center">Pelaksana Pengujian :</th>
                                    <th style="vertical-align: middle" class="text-center">Pengawas Pekerjaan</th>
                                    <th style="vertical-align: middle" class="text-center">Penanggung Jawab</th/>
                                </tr>
                            </thead>
                            <tbody id="data">
                                <?php
                                for ($i = 0; $i < 3; $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        <td><input class="form-control"  type="text" name="pelaksana_<?php echo $i; ?>" value="<?php echo $penanggung_jawab[$i]->pelaksana; ?>"></td>
                                        <td><input class="form-control"  type="text" name="pengawas_<?php echo $i; ?>" value="<?php echo $penanggung_jawab[$i]->pengawas; ?>"></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <button id="simpan_data" type="submit" formnovalidate="" class="btn btn-success pull-right" title="Simpan Data SPP UP"><i class="fa fa-save"></i> Simpan</button>
                            <a href="<?php echo site_url('mode_ct/ct'); ?>"  class="btn btn-default pull-right"  title="Kembali ke Daftar SPP UP" style="margin-right: 10px;"><i class="fa fa-backward"></i>  Kembali</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
