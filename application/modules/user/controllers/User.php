<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('User_m'));
    }

    public function index() {
        $this->data['judul'] = "Menu Data User";
//        $this->data['datanya'] = $this->User_m->get('user');
        $this->data['datanya'] = $this->User_m->view('user', array('level' => 'user'));

        $this->content = 'daftar';
        $this->view_m();
    }

    public function tambah() {
        $this->data['action'] = base_url('user/tambah_proses');
        $this->data['judul'] = "Tambah Data User ";
        $this->data['sub_judul'] = "Form Tambah Data User ";
        $this->content = 'form';
        $this->view();
    }

    public function edit() {
        $id = 44;
        $this->data['action'] = base_url('mode_ct/ct/tambah_proses');
        $this->data['judul'] = "Tambah Data CT ";
        $this->data['data_edit'] = $this->User_m->get_data_edit($id);
        $this->data['data_umum'] = $this->User_m->view('data_umum', array('id' => $id));
//        print_r_pre($this->data['data_umum']);
        $this->data['data_uji'] = $this->User_m->get_data_uji($id);
//        print_r_pre($this->data['data_uji']);
        $this->data['catatan'] = $this->User_m->view('catatan', array('id_data_umum' => $id));
        $this->data['penanggung_jawab'] = $this->User_m->view('penanggung_jawab', array('id_data_umum' => $id));

        $this->data['data_alat'] = $this->User_m->get('peralatan_ct');
        $this->data['hasil_uji'] = $this->User_m->get('hasil_uji_ct');
//        $this->data['data_edit'] = $this->User_m->get_data_edit(6);

        $this->content = 'form_edit';
        $this->view();
    }

    public function excel() {
        $id = 39;

        $this->data['data_edit'] = $this->User_m->get_data_edit($id);
        $this->data['data_umum'] = $this->User_m->view('data_umum', array('id' => $id));
//         print_r_pre($this->data['data_umum']);
        $this->data['data_uji'] = $this->User_m->get_data_uji($id);
        $this->data['catatan'] = $this->User_m->view('catatan', array('id_data_umum' => $id));
        $this->data['penanggung_jawab'] = $this->User_m->view('penanggung_jawab', array('id_data_umum' => $id));

        $this->data['data_alat'] = $this->User_m->get('peralatan_ct');
        $this->data['hasil_uji'] = $this->User_m->get('hasil_uji_ct');
//        $this->data['data_edit'] = $this->User_m->get_data_edit(6);

        $this->content = 'excel';
        $this->view();

        $data['data_edit'] = $this->User_m->get_data_edit($id);
        $data['data_umum'] = $this->User_m->view('data_umum', array('id' => $id));
        $data['data_uji'] = $this->User_m->get_data_uji($id);
        $data['catatan'] = $this->User_m->view('catatan', array('id_data_umum' => $id));
        $data['penanggung_jawab'] = $this->User_m->view('penanggung_jawab', array('id_data_umum' => $id));

        $data['data_alat'] = $this->User_m->get('peralatan_ct');
        $data['hasil_uji'] = $this->User_m->get('hasil_uji_ct');
        $data['data_edit'] = $this->User_m->get_data_edit(6);

        $this->load->view('excel');
        $html = $this->output->get_output();
        //$this->view();
        generate_pdf($html);
    }

    public function tambah_proses() {
        $usere = $this->input->post('usere');
//        print_r_pre($usere);
        foreach ($usere as $key => $value) {
            if ($this->input->post('nama_' . $value) != '' and $this->input->post('password_' . $value) != '') {
                print_r_pre($value);
                $pj = array(
                    'username' => $this->input->post('nama_' . $value),
                    'password' => md5($this->input->post('password_' . $value)),
                    'level' => 'user'
                );
//                print_r_pre($pj);
                if ($this->User_m->insert('user', $pj)) {
                    echo 'sukses';
                } else {
                    echo 'gagal';
                }
            }
        }
        redirect(base_url('user'));
    }

    public function delete($id) {
        $this->User_m->delete('user', array('id' => $id));
        redirect(base_url('user'));
    }

}
