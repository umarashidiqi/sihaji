<?php

class La_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_unit($id) {
        $sql = "SELECT * FROM kr_organisasi";
        $sql.= ($id) ? " WHERE parent_id = '$id'" : '';
        $sql.= " ORDER BY org_key";
        return $this->db->query($sql)->result();
    }

    function insert($tablename, $data1) {
        return ($this->db->insert($tablename, $data1)) ? $this->db->insert_id() : false;
    }

    function insert_batch($tablename, $data) {
        return $this->db->insert_batch($tablename, $data);
    }

    function update($tablename, $where, $kolom) {
        $this->db->where($where);
        return $this->db->update($tablename, $kolom);
    }

//    function update($tablename, $where, $data) {
//        $this->db->where($where);
//        $this->db->update($tablename, $data);
//        return true;
//    }

    function delete($tablename, $where) {
        $this->db->where($where);
        return $this->db->delete($tablename);
    }

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

     function view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function single_view($tablename, $data = '') {
        if ($data !== '') {
            $this->db->where($data);
        }
        $query = $this->db->get($tablename);

        return $query->row();
    }

    public function detail_organisasi($orgkey) {
        $sql = "select unit_kerja_simpeg unit, parent dinas from kr_organisasi WHERE org_key =" . $orgkey;
        return $this->db->query($sql)->row();
    }

    function get_data_edit($uk_data) {
        $sql = "SELECT
	checklist_la.id,
	checklist_la.id_alat,
	checklist_la.id_data_umum,
	checklist_la.peralatan,
	checklist_la.k1,
	checklist_la.k2,
	checklist_la.k3,
	checklist_la.k4,
	peralatan_la.kode,
	peralatan_la.nama_alat,
	peralatan_la.k1 AS alat1,
	peralatan_la.k2 AS alat2,
	peralatan_la.k3 AS alat3,
	peralatan_la.k4 AS alat4
FROM
	checklist_la
JOIN peralatan_la ON checklist_la.id_alat = peralatan_la.id
WHERE checklist_la.id_data_umum=$uk_data";
        return $this->db->query($sql)->result();
    }
    
     function get_data_uji($uk_data) {
        $sql = "SELECT
hasil_uji_la.id,
hasil_uji_la.kode,
hasil_uji_la.pembeda,
hasil_uji_la.titik_ukur,
pengujian_la.id as id_pengujian, 
pengujian_la.injeksi_teg, 
pengujian_la.fasa_r_hu, 
pengujian_la.fasa_s_hu, 
pengujian_la.fasa_t_hu,
pengujian_la.fasa_r_sebelum,
pengujian_la.fasa_r_sesudah,
pengujian_la.fasa_s_sebelum,
pengujian_la.fasa_s_sesudah,
pengujian_la.fasa_t_sebelum,
pengujian_la.fasa_t_sesudah
FROM
	pengujian_la
JOIN hasil_uji_la ON pengujian_la.id_hasil_uji = hasil_uji_la.id
WHERE pengujian_la.id_data_umum=$uk_data";
        return $this->db->query($sql)->result();
    }
    
    

}
