<?php
//$user = $this->session->userdata('user');
?>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <div id="chart" style="min-width: 310px; height: 250px; margin: 0 auto"></div>
        </div>
    </div>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="text-center tebal">DAFTAR SPJ</h4>
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <form class="form-horizontal" action="#" method="post">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="bulan">Bulan</label>
                            <select class="form-control" name="bulan" id="bulanCari" required="">
                                <option value="0" selected>Tahunan</option>
                                <?php
//                                for ($i = 1; $i <= 12; $i++) {
//                                    echo '<option value="' . bulan($i) . '" ' . (($i == date('n')) ? 'selected' : '') . '>' . bulan($i) . '</option>';
//                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="unit">Kata Kunci Pencarian</label>
                            <input type="text" id="keyword" name="cari" class="form-control" placeholder="Masukkan kata kunci pencarian...">
                        </div>
                        <div class="col-md-2">
                            <label>&nbsp;</label>
                            <button type="button" id="btnCari" class="btn btn-info form-control" title="Cari Data Mutasi"><i class="fa fa-search"></i> Cari</button>
                        </div>
                        
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle" class="text-center">No</th>
                                    <th style="vertical-align: middle" class="text-center">Merk</th>
                                    <th style="vertical-align: middle" class="text-center">Type</th>
                                    <th style="vertical-align: middle" class="text-center">Lokasi</th>                                
                                    <th style="vertical-align: middle" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="data">
                                <?php
                                $no = 1;
                                foreach ($datanya as $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value->merk; ?></td>
                                        <td><?php echo $value->type; ?></td>
                                        <td><?php echo $value->lokasi_gi; ?></td>
                                        <td class="text-center">                                                                            
                                            <a href="" class="btn btn-primary" title="Edit Rekanan"><i class="fa fa-edit"></i>Update</a>
                                            <a href="" class="hapus_btn btn btn-danger" title="Hapus Rekanan"><i class="fa fa-remove"></i>Download</a>                                                                            
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                          <div class="form-group">
                <?php
//                    if ($user == 'bp' || $user == 'bpp') {
                ?>
                                    <a class="btn btn-success pull-right" title="Tambah SPJ" href=""><i class="fa fa-plus"></i> Tambah</a>
                <?php
//                    }
                ?>
                                    <div class="h3"><span style="text-transform: uppercase !important" id="unite"></span></div>
                                </div>


            </div>
        </div>
    </div>
</div>


<div class="gallery-fix"></div> 
<div class="header header-light">
    <a href="#" class="open-menu"><i class="fa fa-navicon"></i></a>
    <h3><?php echo $judul; ?></h3>
    <a href="#" onclick="window.history.go(-1);
            return false;" title="Back"><i class="fa fa-chevron-left"></i></a>
</div>
<div class="navigation navigation-light">
    <div class="navigation-scroll">
        <a href="<?php echo base_url('welcome/menu'); ?>" class="menu-item"><i class="fa fa-home"></i><em>Main Menu</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/1'); ?>" class="menu-item"><i class="fa fa-building"></i><em>Tentang Kami</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/2'); ?>" class="menu-item"><i class="fa fa-cube"></i><em>Panduan</em><i class="fa fa-circle"></i></a>
        <a href="<?php echo base_url('welcome/info/3'); ?>" class="menu-item"><i class="fa fa-phone"></i><em>Kontak</em><i class="fa fa-circle"></i></a>
        <a href="#" class="menu-item close-menu"><i class="fa fa-times-circle"></i><em>Close</em><i class="fa fa-circle"></i></a>
    </div>
</div>
<div id="page-content" class="bg-1">
    <div id="page-content-scroll">
        <h3 class="header-mask header-mask-light">Welcome</h3>
        <div class="content-strip">
            <div class="strip-content">
                <h4>Daftar Data CT</h4>
                <p>Sistem Informasi Hasil Pengujian</p>
                <i class="fa fa-cogs"></i>
            </div>
            <div class="overlay"></div>
            <img class="preload-image" data-original="<?php echo image() ?>/pictures/9.jpg" alt="img">
        </div>
        <div class="content">
            <a href="" class="button button-xs button-green button-round pull-right button-fullscreen" title="Tambah"><i class="fa fa-plus"></i> Tambah</a>
            <div class="decoration"></div>
            <?php
            $no = 1;
            foreach ($datanya as $value) {
                ?>
                <div class="toggle store-history-toggle">
                    <a href="#" class="toggle-title">
                        <strong><?php echo $value->merk; ?></strong> 
                        <strong><?php echo $value->type; ?></strong> 
                        <strong><?php echo $value->lokasi_gi; ?></strong> 
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="toggle-content">
                        <a href="" class="button button-xs button-green button-round button-fullscreen" title="Edit">Edit</a>
                        <a href="" class="button button-xs  button-red button-round  button-fullscreen" title="Hapus"></i> Download</a>                                                      
                    </div>
                </div>
                <?php
                $no++;
            }
            ?>
        </div>
        <div class="footer">
            <p class="footer-strip">Copyright <span id="copyright-year"></span> Silvanix.com. All Rights Reserved</p>
        </div>
        <div class="footer-clear"></div>
    </div>
</div>
<a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i>Back to top</a>