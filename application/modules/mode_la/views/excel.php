<?php
$filename = 'ct' . ".xls";
header('Content-type: application/ms-excel');
header('Content-Disposition: attachment; filename=' . $filename);
header("Pragma: no-cache");
header("Expires: 0");
?>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="0" cellpadding="3">
    <tr>
        <td rowspan="6" width="15%" style="text-align: center; vertical-align: middle; border-top: 1px solid #000; border-left: 1px solid #000"><img width="60px" height="70px" src="<?php echo image('download.png'); ?>"></td>
    <tr>
    <tr>
        <td colspan="9" style="border-left: 1px solid #000; border-right: 1px solid #000"><strong>PT. PLN (PERSERO)</strong></td>
    </tr>
    <tr>
        <td colspan="9" style="border-left: 1px solid #000; border-right: 1px solid #000"><strong>UNIT TRANSMISI JAWA BAGIAN TENGAH</strong></td>
    </tr>
    <tr>
        <td colspan="9" style="border-left: 1px solid #000; border-right: 1px solid #000"><strong>AREA PELAKSANA PEMELIHARAAN SEMARANG</strong></td>
    </tr>
    <tr>
        <td colspan="9" style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000">Jalan : Jend.Sudirman km.23 Gedung C - 50511  Telp. : (024) 6922402 , JWOT.30999  FAX. (024) 6921235</td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="4" width="30%" style="text-align: center; border-bottom: none;"><strong>DAFTAR LAMPIRAN  "LAPORAN TEKNIK"</strong></td>
        <td width="10%">Nomor</td>
        <td colspan="4" width="30%"> : </td>
    </tr>
    <tr>
        <td colspan="4" width="30%" style="text-align: center; border-top: none;"></td>
        <td width="10%">Tanggal</td>
        <td colspan="4" width="30%"> : </td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="9" style="text-align: center"><strong>CHECK LIST   CT ( CURRENT TRANSFORMER ) 150 kV</strong></td>
    </tr>
    <tr>
        <td colspan="3">Merk</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">Bay</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">PDLAMP II</td>
    </tr>
    <tr>
        <td colspan="3">Type</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td>No Seri</td>
        <td colspan="2">Fasa R</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Fasa S</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">Lokasi GI</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">SRONDOL</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Fasa T</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">Ratio Core 1</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">Ratio Core 2</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">Ratio Core 3</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">Ratio Core 4</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">Ratio Core 5</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="6"><strong>I. Check List :</strong></td>
    </tr>
    <tr>
        <td width="2%" style="text-align: center; font-weight: bold"> No </td>
        <td width="58%" style="text-align: center; font-weight: bold"> Peralatan Yang Diperiksa </td>
        <td width="20%" colspan="4" style="text-align: center; font-weight: bold"> Kondisi Awal </td>
        <td width="20%" colspan="4" style="text-align: center; font-weight: bold"> Kondisi Akhir </td>
    </tr>
    <?php
    foreach ($data_edit as $value) {
        $exp = explode('.', $value->kode);
        $jml = count($exp);
        if ($jml == 1) {
            ?>
            <tr>
                <td style="text-align: center"><?php echo $value->kode; ?></td>
                <td><?php echo $value->nama_alat . '-' . $value->id; ?></td>
                <td width="20%" colspan="4"></td>
                <td width="20%" colspan="4"></td>
            </tr>
            <?php
        } elseif ($jml == 2) {
            ?>
            <tr>
                <td><input type="hidden" name="idnyak[]" value="<?php echo $value->id; ?>"></td>
                <td><?php echo $value->kode . ' ' . $value->nama_alat . '-' . $value->id; ?></td>
                <td width="2%" <?php echo ($value->k1 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td width="7%">
                    <?php echo $value->alat1; ?>
                </td>
                <td width="2%" <?php echo ($value->k2 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td width="7%">
                    <?php echo $value->alat2; ?>
                </td>
                <td width="2%" <?php echo ($value->k3 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td width="7%">
                    <?php echo $value->alat3; ?>
                </td>
                <td width="2%" <?php echo ($value->k4 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td width="7%">
                    <?php echo $value->alat4; ?>
                </td>
            </tr>
            <?php
        }
    }
    ?>
    <tr>
        <td>7</td>
        <td colspan="5">Catatan</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="7">
            <?php echo $catatan[0]->uraian_checklist; ?>
        </td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="3">Pelaksana Pengujian :</td>
        <td colspan="3">Pengawas Pekerjaan :</td>
        <td>Tanda Tangan :</td>
    </tr>
    <tr>
        <td colspan="3">1.</td>
        <td colspan="3"></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">2.</td>
        <td colspan="3"></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">3.</td>
        <td colspan="3"></td>
        <td></td>
    </tr>
</table>
<br>
<br>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="3">
    <tr>
        <td rowspan="6" width="15%" style="text-align: center; vertical-align: middle"><img width="60px" height="70px" src="<?php echo image('download.png'); ?>"></td>
    <tr>
    <tr>
        <td><strong>PT. PLN (PERSERO)</strong></td>
    </tr>
    <tr>
        <td><strong>UNIT TRANSMISI JAWA BAGIAN TENGAH</strong></td>
    </tr>
    <tr>
        <td><strong>AREA PELAKSANA PEMELIHARAAN SEMARANG</strong></td>
    </tr>
    <tr>
        <td>Jalan : Jend.Sudirman km.23 Gedung C - 50511  Telp. : (024) 6922402 , JWOT.30999  FAX. (024) 6921235</td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <tr>
        <td width="30%" style="text-align: center; border-bottom: none;"><strong>DAFTAR LAMPIRAN  "LAPORAN TEKNIK"</strong></td>
        <td width="10%">Nomor</td>
        <td width="30%"> : </td>
    </tr>
    <tr>
        <td width="30%" style="text-align: center; border-top: none;"></td>
        <td width="10%">Tanggal</td>
        <td width="30%"> : </td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="7" style="text-align: center"><strong>'HASIL PENGUJIAN   CT ( CURRENT TRANSFORMER ) 150KV</strong></td>
    </tr>
    <tr>
        <td colspan="2">Merk</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">Bay</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">PDLAMP II</td>
    </tr>
    <tr>
        <td colspan="2">Type</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td>No Seri</td>
        <td>Fasa R</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td></td>
        <td>Fasa S</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">Lokasi GI</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">SRONDOL</td>
    </tr>
    <tr>
        <td></td>
        <td>Fasa T</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Ratio Core 1</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Ratio Core 2</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Ratio Core 3</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Ratio Core 4</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Ratio Core 5</td>
        <td style="text-align: center">:</td>
        <td style="text-align: center">TRENCH</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="9"><strong>II Hasil Pengukuran</strong></td>
    </tr>
    <tr>
        <td colspan="9"><strong>1 Tahanan Isolasi Belitan</strong></td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" border="1" cellpadding="5">
    <thead>
        <tr>
            <th colspan="2" rowspan="2" style="vertical-align: middle; text-align: center; font-weight: bold">Titik Ukur</th>
            <th colspan="3" style="vertical-align: middle; text-align: center; font-weight: bold">Fasa R</th>
            <th colspan="3" style="vertical-align: middle; text-align: center; font-weight: bold">Fasa S</th>
            <th colspan="3" style="vertical-align: middle; text-align: center; font-weight: bold">Fasa T</th>
        </tr>
        <tr>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Standart</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Th.Lalu</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Hasil ukur</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Standart</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Th.Lalu</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Hasil ukur</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Standart</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Th.Lalu</th>
            <th style="vertical-align: middle; text-align: center; font-weight: bold">Hasil ukur</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no_1 = 1;
        $no_2 = 1;
        $no_3 = 7;
        $no_4 = 1;
        foreach ($data_uji as $value) {
            $exp = explode('.', $value->kode);
            $jml = count($exp);
            if ($value->pembeda == 1) {
                //untuk pertama foreachnya
                ?>
                <tr>
                    <td style="text-align: center; font-weight: bold">
                        <?php echo $no_1; ?>
                    </td>
                    <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                    <td style="text-align: center">1 KV/ M</td>
                    <td></td>
                    <td style="text-align: center"><?php echo $value->fasa_r_hu; ?></td>
                    <td style="text-align: center">1 KV/ M</td>
                    <td></td>
                    <td style="text-align: center"><?php echo $value->fasa_s_hu; ?></td>
                    <td style="text-align: center">1 KV/ M</td>
                    <td></td>
                    <td style="text-align: center"><?php echo $value->fasa_t_hu; ?></td>
                </tr>
                <?php
                $no_1++;
            } elseif ($jml == 2) {
                //inkesi teg
                ?>
                <tr>
                    <td></td>
                    <td><b><?php echo $value->kode . ' ' . $value->titik_ukur; ?></b></td>
                    <td colspan="9"><?php echo $value->injeksi_alat_uji; ?></td>
                </tr>
                <?php
                $no_2++;
            } elseif ($value->pembeda == 2) {
                ?>
                <tr>
                    <td>
                        <?php echo $no_3; ?>
                    </td>
                    <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                    <td>1 KV/ M</td>
                    <td></td>
                    <td><?php echo $value->fasa_r_hu; ?></td>
                    <td>1 KV/ M</td>
                    <td></td>
                    <td><?php echo $value->fasa_s_hu; ?></td>
                    <td>1 KV/ M</td>
                    <td></td>
                    <td><?php echo $value->fasa_t_hu; ?></td>
                </tr>
                <?php
                $no_3++;
            } elseif ($value->pembeda == 3) {
                //alat uji 1
                ?>
                <tr>
                    <td>
                        <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                    </td>
                    <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>

                    <td colspan="9"><?php echo $value->injeksi_alat_uji; ?></td>
                </tr>
                <?php
            } elseif ($value->pembeda == 4) {
                ?>
                <tr>
                    <td><b>2</b></td>
                    <td colspan="10"><b><?php echo $value->titik_ukur; ?></b></td>
                </tr>
                <?php
            } elseif ($value->pembeda == 5) {
                //tahanan input
                ?>
                <tr>
                    <td><b>2</b></td>
                    <td><b>Tahanan Pentanahan</b></td>
                    <td colspan="9"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                    <td>< 1 W</td>
                    <td></td>
                    <td><?php echo $value->fasa_r_hu; ?></td>
                    <td>1 KV/ M</td>
                    <td></td>
                    <td><?php echo $value->fasa_s_hu; ?></td>
                    <td>1 KV/ M</td>
                    <td></td>
                    <td><?php echo $value->fasa_t_hu; ?></td>
                    <?php
                } elseif ($value->pembeda == 6) {
                    //alat uji 2
                    ?>
                <tr>
                    <td>
                        <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                    </td>
                    <td><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>

                    <td colspan="9"><?php echo $value->injeksi_alat_uji; ?></td>
                </tr>
                <?php
            }
        }
        ?>
        <tr>
            <td><b>3</b></td>
            <td><b>Catatan</b></td>
            <td colspan="9">
                <?php echo $catatan[0]->uraian_uji; ?>
            </td>
        </tr>
        <?php ?>
    </tbody>
</table>