<style type="text/css">
    .text-miring{font-style: italic;}
    .text-garis-bawah{text-decoration: underline;}
    .text-coret {text-decoration: line-through;}
    .text-right{text-align: right}
    .text-center{text-align: center}
    .text-rata{text-align: justify}
    .text-bold{font-weight: bold}
    .text-top{vertical-align: top !important}
    .text-middle{vertical-align: middle !important;}
    .font-20{font-size: 20px}
    .font-18{font-size: 18px}
    .font-16{font-size: 16px}
    .font-14{font-size: 14px}
    .font-13{font-size: 13px}
    .font-12{font-size: 12px}
    .font-11{font-size: 11px}
    .font-10{font-size: 10px}
    .font-9{font-size: 7px}
    sup {font-size: 10px;}
    .padding-10 {padding-top: 10px; padding-bottom: 10px; padding-left: 10px;}
    .padding-left-10{padding-left: 10px}
    .padding-right-10{padding-right: 10px}
    .padding-top-10{padding-top: 10px}
    .padding-bottom-10{padding-bottom: 10px}
    .padding-left-15{padding-left:15px}
    .padding-right-15{padding-right:15px}
    .padding-top-15{padding-top:15px}
    .padding-bottom-15{padding-bottom:15px}
    .padding-left-25{padding-left:25px}
    .padding-right-25{padding-right:25px}
    .padding-top-25{padding-top:25px}
    .padding-bottom-25{padding-bottom:25px}
    .padding-left-50{padding-left:50px}
    .padding-right-50{padding-right:50px}
    .padding-top-50{padding-top:50px}
    .padding-bottom-50{padding-bottom:50px}
    .row-hight-10{line-height: 10px;}
    .row-hight-20{line-height: 20px;}
    .row-hight-30{line-height: 30px;}
    .row-hight-40{line-height: 40px;}
    .border-tlb {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: none;
        border-bottom-style: solid;
        border-left-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-lb {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: none;
        border-right-style: none;
        border-bottom-style: solid;
        border-left-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-lr {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: none;
        border-right-style: solid;
        border-bottom-style: none;
        border-left-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-r {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: none;
        border-right-style: solid;
        border-bottom-style: none;
        border-left-style: none;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-tlbr {
        border: 1px solid #000000;
    }
    .border-lbr {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: none;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-tb {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: none;
        border-bottom-style: solid;
        border-left-style: none;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-tlb {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-tlr {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: solid;
        border-left-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-tbr {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-br {
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #000000;
        border-bottom-color: #000000;
    }
    .border-tr {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-tl {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-left-style: solid;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-t {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: none;
        border-bottom-style: none;
        border-left-style: none;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-b {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: none;
        border-right-style: none;
        border-bottom-style: solid;
        border-left-style: none;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    .border-l {
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: none;
        border-right-style: none;
        border-left-style: solid;
        border-bottom-style: none;
        border-top-color: #000000;
        border-right-color: #000000;
        border-bottom-color: #000000;
        border-left-color: #000000;
    }
    table {
        border-collapse: collapse;
        padding: 5px;
    }
    thead {display: table-header-group;}
    tfoot {display: table-header-group;}
</style>
<table style="font-size: 10px;" cellspacing="0" width="100%" cellpadding="3">
    <tr>
        <td class="border-tlbr" width="15%" style="text-align: center; vertical-align: middle"><img width="60px" height="70px" src="<?php echo image('download.png'); ?>"></td>
        <td class="border-tbr" colspan="3">
            <strong>PT. PLN (PERSERO)</strong><br>
            <strong>UNIT TRANSMISI JAWA BAGIAN TENGAH</strong><br>
            <strong>AREA PELAKSANA PEMELIHARAAN SEMARANG</strong><br>
            Jalan : Jend.Sudirman km.23 Gedung C - 50511  Telp. : (024) 6922402 , JWOT.30999  FAX. (024) 6921235
        </td>
    </tr>
    <tr>
        <td width="30%" colspan="2" class="border-lr" style="text-align: center;"><strong>DAFTAR LAMPIRAN  "LAPORAN TEKNIK"</strong></td>
        <td width="10%" class="border-br">Nomor</td>
        <td width="30%"class="border-br"> : <?php echo $data_umum[0]->nomor ?></td>
    </tr>
    <tr>
        <td width="30%" colspan="2" class="border-lbr" style="text-align:"></td>
        <td width="10%" class="border-br">Tanggal</td>
        <td width="30%" class="border-br"> : <?php echo $data_umum[0]->tanggal ?></td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%">
    <tr>
        <td colspan="7" style="text-align: center; padding-top: 10px; padding-bottom: 10px;"><strong>CHECK LIST   LA ( LIGHTNING ARRESTER )</strong></td>
    </tr>
    <tr>
        <td colspan="2">Merk</td>
        <td style="text-align: center">:</td>
        <td class="border-tlbr" style="text-align: center"><?php echo $data_umum[0]->merk ?></td>
        <td style="text-align: center">Bay</td>
        <td style="text-align: center">:</td>
        <td class="border-tlbr" style="text-align: center"><?php echo $data_umum[0]->bay ?></td>
    </tr>
    <tr>
        <td colspan="2">Type</td>
        <td style="text-align: center">:</td>
        <td class="border-tlbr" style="text-align: center"><?php echo $data_umum[0]->type ?></td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td>No Seri</td>
        <td>Fasa R</td>
        <td style="text-align: center">:</td>
        <td class="border-tlbr" style="text-align: center"><?php echo $data_umum[0]->type_fasa_r ?></td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td></td>
        <td>Fasa S</td>
        <td style="text-align: center">:</td>
        <td class="border-tlbr" style="text-align: center"><?php echo $data_umum[0]->type_fasa_s ?></td>
        <td style="text-align: center">Lokasi GI</td>
        <td style="text-align: center">:</td>
        <td class="border-tlbr" style="text-align: center"><?php echo $data_umum[0]->lokasi_gi ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Fasa T</td>
        <td style="text-align: center">:</td>
        <td class="border-tlbr" style="text-align: center"><?php echo $data_umum[0]->type_fasa_t ?></td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
        <td style="text-align: center">&nbsp;</td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" cellpadding="2">
    <tr>
        <td colspan="10"><strong>I. Check List :</strong></td>
    </tr>
    <tr>
        <td class="border-tlbr" width="2%" style="text-align: center; font-weight: bold"> No </td>
        <td class="border-tlbr" width="58%" style="text-align: center; font-weight: bold"> Peralatan Yang Diperiksa </td>
        <td class="border-tlbr" width="10%" colspan="4" style="text-align: center; font-weight: bold"> Kondisi Awal </td>
        <td class="border-tlbr" width="10%" colspan="4" style="text-align: center; font-weight: bold"> Kondisi Akhir </td>
    </tr>
    <?php
    foreach ($data_edit as $value) {
        $exp = explode('.', $value->kode);
        $jml = count($exp);
        if ($jml == 1) {
            ?>
            <tr>
                <td width="2%" class="border-lr" style="text-align: center"><?php echo $value->kode; ?></td>
                <td width="58%"><?php echo $value->nama_alat; ?></td>
                <td width="20%" colspan="4"></td>
                <td class="border-r" width="20%" colspan="4"></td>
            </tr>
            <?php
        } elseif ($jml == 2) {
            ?>
            <tr>
                <td width="2%" class="border-l"><input type="hidden" name="idnyak[]" value="<?php echo $value->id; ?>"></td>
                <td width="58%" class="border-l"><?php echo $value->kode . ' ' . $value->nama_alat; ?></td>
                <td class="border-tlbr" <?php echo ($value->k1 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td class="font-9">
                    <?php echo $value->alat1; ?>
                </td>
                <td class="border-tlbr" <?php echo ($value->k2 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td class="font-9">
                    <?php echo $value->alat2; ?>
                </td>
                <td class="border-tlbr" <?php echo ($value->k3 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td class="font-9">
                    <?php echo $value->alat3; ?>
                </td>
                <td class="border-tlbr" <?php echo ($value->k4 == '') ? "" : 'style="background-color: #194e70;"' ?>></td>
                <td  class="border-r font-9">
                    <?php echo $value->alat4; ?>
                </td>
            </tr>
            <?php
        }
    }
    ?>
    <tr>
        <td colspan="10" class="border-t">&nbsp;</td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" cellpadding="5">
    <tr>
        <td colspan="9"><strong>II Hasil Pengukuran</strong></td>
    </tr>
    <tr>
        <td colspan="9"><strong>1 Tahanan Isolasi Belitan</strong></td>
    </tr>
</table>
<table style="font-size: 10px;" cellspacing="0" width="100%" cellpadding="5">
    <?php
    $no_1 = 1;
    $no_2 = 1;
    $no_3 = 7;
    $no_4 = 1;
    foreach ($data_uji as $value) {
        $exp = explode('.', $value->kode);
        $jml = count($exp);

        if ($value->pembeda == 1 and $value->kode == '1') {
            //untuk injeksi
            ?>
            <tr>
                <td></td>
                <td class=""><b><?php echo $value->titik_ukur; ?> : </b></td>
                <td class="border-tlbr" colspan="2"><?php echo $value->injeksi_teg; ?></td>
                <td colspan="7">Volt</td>
            </tr>
            <tr><td colspan="11">&nbsp;</td></tr>
            <tr>
                <th colspan="2" rowspan="2" style="vertical-align: middle" class="border-tlbr text-center">Titik Ukur</th>
                <th colspan="3" style="vertical-align: middle" class="border-tlbr text-center">Fasa R</th>
                <th colspan="3" style="vertical-align: middle" class="border-tlbr text-center">Fasa S</th>
                <th colspan="3" style="vertical-align: middle" class="border-tlbr text-center">Fasa T</th>
            </tr>
            <tr>
                <th style="vertical-align: middle" class="border-tlbr text-center">Standart</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Th.Lalu</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Hasil ukur</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Standart</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Th.Lalu</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Hasil ukur</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Standart</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Th.Lalu</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Hasil ukur</th>
            </tr>
            <?php
            $no_1++;
        } elseif ($value->pembeda == 1 and $value->kode == '10') {
            //alat uji
            ?>
            <tr>
                <td class="border-tlb" colspan="2"><?php echo $value->titik_ukur; ?> : </td>
                <td class="border-tbr" colspan="9"><?php echo $value->injeksi_teg; ?></td>
            </tr>
            <tr>
                <td class="border-tlb"><b>2</b></td>
                <td class="border-tbr" colspan="10"><b>Tahanan Petanahan</b></td>
            </tr>
            <tr>
                <th colspan="2" rowspan="2" style="vertical-align: middle" class="border-tlbr text-center">Titik Ukur</th>
                <th colspan="3" style="vertical-align: middle" class="border-tlbr text-center">Fasa R</th>
                <th colspan="3" style="vertical-align: middle" class="border-tlbr text-center">Fasa S</th>
                <th colspan="3" style="vertical-align: middle" class="border-tlbr text-center">Fasa T</th>
            </tr>
            <tr>
                <th style="vertical-align: middle" class="border-tlbr text-center">Standart</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Th.Lalu</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Hasil ukur</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Standart</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Th.Lalu</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Hasil ukur</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Standart</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Th.Lalu</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Hasil ukur</th>
            </tr>
            <?php
        } elseif ($value->pembeda == 1 and $jml == 2) {
            ?>
            <tr>
                <td class="border-tlbr" colspan="2"><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                <?php echo($no_2 == 1) ? "<td  class='border-tlbr' style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                <td class="border-tlbr"></td>
                <td class="border-tlbr"><?php echo $value->fasa_r_hu; ?></td>
                <?php echo($no_2 == 1) ? "<td  class='border-tlbr' style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                <td class="border-tlbr"></td>
                <td class="border-tlbr"><?php echo $value->fasa_s_hu; ?></td>
                <?php echo($no_2 == 1) ? "<td  class='border-tlbr' style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                <td class="border-tlbr"></td>
                <td class="border-tlbr"><?php echo $value->fasa_t_hu; ?></td>
            </tr>
            <?php
            $no_2++;
        } elseif ($value->kode == '2' and $value->pembeda == '2') {
            ?>
            <tr>
                <td class="border-tlb"><b>2</b></td>
                <td class="border-tbr" colspan="10"><b><?php echo $value->titik_ukur; ?></b></td>
            </tr>
            <?php
            $no_2++;
        } elseif ($value->kode == '2.1' and $value->pembeda == 2) {
            ?>
            <tr>
                <td class="border-tlbr" colspan="2"><?php echo $value->titik_ukur; ?></td>
                <td class="border-tlbr text-center">1 KV/ M</td>
                <td class="border-tlbr"></td>
                <td class="border-tlbr text-center"><?php echo $value->fasa_r_hu; ?></td>
                <td class="border-tlbr text-center">1 KV/ M</td>
                <td class="border-tlbr"></td>
                <td class="border-tlbr text-center"><?php echo $value->fasa_s_hu; ?></td>
                <td class="border-tlbr text-center">1 KV/ M</td>
                <td class="border-tlbr"></td>
                <td class="border-tlbr text-center"><?php echo $value->fasa_t_hu; ?></td>
            </tr>
            <?php
            $no_3++;
        } elseif ($value->kode == '' and $value->pembeda == 2) {
            //alat uji
            ?>
            <tr>
                <td class="border-tlb" colspan="2"><?php echo $value->titik_ukur; ?> : </td>
                <td class="border-tbr" colspan="9"><?php echo $value->injeksi_teg; ?></td>
            </tr>
            <tr>
                <td class="border-tlb"><b>3</b></td>
                <td class="border-tbr" colspan="10"><b>Counter Arrester</b></td>
            </tr>
            <?php
        } elseif ($value->kode == '3' and $value->pembeda == 3) {
            //alat uji
            ?>
            <tr>
                <td class="border-tlb"><b>3</b></td>
                <td class="border-tbr" colspan="10"><b><?php echo $value->titik_ukur; ?></b></td>
            </tr>
            <?php
        } elseif ($value->kode == '' and $value->pembeda == 3) {
            //penunjukan
            ?>
            <tr>
                <th colspan="5" rowspan="2" style="vertical-align: middle" class="border-tlbr text-center">Titik Ukur</th>
                <th colspan="2" style="vertical-align: middle" class="border-tlbr text-center">Fasa R</th>
                <th colspan="2" style="vertical-align: middle" class="border-tlbr text-center">Fasa S</th>
                <th colspan="2" style="vertical-align: middle" class="border-tlbr text-center">Fasa T</th>
            </tr>
            <tr>
                <th style="vertical-align: middle" class="border-tlbr text-center">Sebelum</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Sesudah</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Sebelum</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Sesudah</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Sebelum</th>
                <th style="vertical-align: middle" class="border-tlbr text-center">Sesudah</th>
            </tr>
            <tr>
                <td class="border-tlbr" colspan="5"><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                <td class="border-tlbr"><?php echo $value->fasa_r_sebelum; ?></td>
                <td class="border-tlbr"><?php echo $value->fasa_r_sesudah; ?></td>
                <td class="border-tlbr"><?php echo $value->fasa_s_sebelum; ?></td>
                <td class="border-tlbr"><?php echo $value->fasa_s_sesudah; ?></td>
                <td class="border-tlbr"><?php echo $value->fasa_t_sebelum; ?></td>
                <td class="border-tlbr"><?php echo $value->fasa_t_sesudah; ?></td>
            </tr>
            <?php
        }
    }
    ?>
    <tr>
        <td class="border-tl"><b>4</b></td>
        <td  class="border-tr" colspan="10"><b>Catatan</b></td>
    </tr>
    <tr>
        <td class="border-lb"></td>
        <td class="border-br" colspan="10"><?php echo $catatan[0]->uraian_uji; ?></td>
    </tr>
</table>
<br>
<table style="font-size: 10px;" cellspacing="0" width="100%" cellpadding="5">
    <tr>
        <td width="40%" colspan="4" class="text-center text-bold border-tlbr">Pelaksana Pengujian :</td>
        <td width="30%" colspan="3" class="text-center text-bold border-tlbr">Pengawas Pekerjaan :</td>
        <td width="30%" colspan="3" class="text-center text-bold border-tlbr">Tanda Tangan :</td>
    </tr>
    <?php
    for ($i = 0; $i < 3; $i++) {
        ?>
        <tr>
            <td width="40%" class="border-tlbr" colspan="4"><?php echo $i + 1 . ". " . $penanggung_jawab[$i]->pelaksana; ?></td>
            <td width="30%" class="border-tlbr" colspan="3"><?php echo $penanggung_jawab[$i]->pengawas; ?></td>
            <td width="30%" class="border-tlbr" colspan="3"></td>
        </tr>
        <?php
    }
    ?>
</table>