<div class=" col-md-12">
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="col-lg-12">
                <form class="form-horizontal" id="ct" action="<?php echo @$action ?>" method="post"
                      data-fv-framework="bootstrap"
                      data-fv-message="Form ini tidak boleh kosong!"
                      data-fv-feedbackicons-valid="glyphicon glyphicon-ok"
                      data-fv-feedbackicons-invalid="glyphicon glyphicon-remove"
                      data-fv-feedbackicons-validating="glyphicon glyphicon-refresh"
                      >
                    <div style="text-align: center">
                        <b>CHECK LIST   LA ( LIGHTNING ARRESTER )</b>
                    </div><br/>
                    <div class="form-group">
                        <div class="col-md-2 col-xs-5">
                            <label for="Tanggal">Tahun</label>
                            <select class="form-control" name="tahun">
                                <option value="2016"<?php
                                if ($data_umum[0]->tahun == '2016') {
                                    echo 'selected';
                                }
                                ?>>2016</option>
                                <option value="2015"<?php
                                if ($data_umum[0]->tahun == '2015') {
                                    echo 'selected';
                                }
                                ?>>2015</option>
                                <option value="2014"<?php
                                if ($data_umum[0]->tahun == '2014') {
                                    echo 'selected';
                                }
                                ?>>2014</option>
                            </select>
                        </div> 
                    </div> 
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="Tanggal">Nomor</label>
                            <input type="text" name="nomor" id="nomor" class="form-control" placeholder="Masukkan Tanggal..." value="<?php echo $data_umum[0]->nomor ?>">
                        </div>
                        <div class="col-md-4">
                            <label for="Tanggal">Tanggal</label>
                            <div class="input-group input-append date" id="tanggal">
                                <input type="date" name="tanggal" id="tanggal" class="form-control" placeholder="Masukkan Tanggal..." value="<?php echo date('m-d-Y', strtotime($data_umum[0]->tanggal)) ?>">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Merk</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="merk" type="text" name="merk" class="form-control" placeholder="Masukkan NIP Penerima..." value="<?php echo $data_umum[0]->merk ?>">
                                </div>
                                <div class="col-md-1">
                                    <label>Bay</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="bay" id="bay" type="text" name="bay" class="form-control" placeholder="" value="<?php echo $data_umum[0]->bay ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>Type</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="type" type="text" name="type" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label>No. Serie</label>
                                </div>
                                <div class="col-md-1">
                                    <label>Fasa R</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="fasa_r" type="text" name="fasa_r" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type_fasa_r ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>Fasa S</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="fasa_s" type="text" name="fasa_s" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type_fasa_s ?>">
                                </div>
                                <div class="col-md-1">
                                    <label>Lokasi GI</label>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="lokasi_gi" type="text" name="lokasi_gi" class="form-control" placeholder="Masukan" value="<?php echo $data_umum[0]->lokasi_gi ?>">
                                </div>


                            </div>
                            <div class="form-group">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <label>Fasa T</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-3">
                                    <input id="fasa_t" type="text" name="fasa_t" class="form-control" placeholder="" value="<?php echo $data_umum[0]->type_fasa_t ?>">
                                </div>
                            </div>


                        </div>
                    </div>
                    <div><b>
                            I. Check List :
                        </b></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle" class="text-center">No</th>
                                    <th style="vertical-align: middle" class="text-center">Peralatan Yang Diperiksa</th>
                                    <th colspan="2" style="vertical-align: middle" class="text-center">Kondisi Awal</th>
                                    <th colspan="2" style="vertical-align: middle" class="text-center">Kondisi Akhir</th>
                                </tr>
                            </thead>
                            <tbody id="data">
                                <?php
                                foreach ($data_edit as $value) {
                                    $exp = explode('.', $value->kode);
                                    $jml = count($exp);
                                    if ($jml == 1) {
                                        //unntuk 1 2 4 5
                                        ?>
                                        <tr>
                                            <td><b><?php echo $value->kode; ?></b></td>
                                            <td colspan="5"><b><?php echo $value->nama_alat; ?></b></td>
                                        </tr>
                                        <?php
                                    } elseif ($jml == 2) {
                                        //untuk subnya 1.2 dll
                                        ?>
                                        <tr>
                                            <td><input type="hidden" name="idnyak[]" value="<?php echo $value->id; ?>"></td>
                                            <td><?php echo $value->kode . ' ' . $value->nama_alat; ?></td>
                                            <td>
                                                <?php
                                                if ($value->k1 == '') {
                                                    echo ' <input type="checkbox"  name="k1_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k1_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat1; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($value->k2 == '') {
                                                    echo ' <input type="checkbox"  name="k2_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k2_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat2; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($value->k3 == '') {
                                                    echo ' <input type="checkbox"  name="k3_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k3_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat3; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($value->k4 == '') {
                                                    echo ' <input type="checkbox"  name="k4_' . $value->id . '" class="" title="Pilih" value="">';
                                                } else {
                                                    echo ' <input type="checkbox" checked name="k4_' . $value->id . '" class="" title="Pilih" value="1">';
                                                }
                                                ?>
                                                <?php echo $value->alat4; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div><b>
                            II. Hasil Pengukuran :
                        </b></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
<!--                                <tr>
                                    <th colspan="2" rowspan="2" style="vertical-align: middle" class="text-center">Titik Ukur</th>
                                    <th colspan="3" style="vertical-align: middle" class="text-center">Fasa R</th>
                                    <th colspan="3" style="vertical-align: middle" class="text-center">Fasa S</th>
                                    <th colspan="3" style="vertical-align: middle" class="text-center">Fasa T</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle" class="text-center">Standart</th>
                                    <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                    <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                    <th style="vertical-align: middle" class="text-center">Standart</th>
                                    <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                    <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                    <th style="vertical-align: middle" class="text-center">Standart</th>
                                    <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                    <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                </tr>-->
                            </thead>
                            <tbody id="data">
                                <tr>
                                    <td><b>1</b></td>
                                    <td colspan="10"><b>Tahanan  Isolasi</b></td>
                                </tr>
                                <?php
                                $no_1 = 1;
                                $no_2 = 1;
                                $no_3 = 7;
                                $no_4 = 1;
                                foreach ($data_uji as $value) {
                                    $exp = explode('.', $value->kode);
                                    $jml = count($exp);

                                    if ($value->pembeda == 1 and $value->kode == '1') {
                                        //untuk injeksi
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                            </td>
                                            <td><b><?php echo $value->kode . ' ' . $value->titik_ukur; ?></b></td>
                                            <td colspan="3"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value="<?php echo $value->injeksi_teg; ?>"></td>
                                            <td colspan="7">Volt</td>
                                        </tr>
                                        <tr>
                                            <th colspan="2" rowspan="2" style="vertical-align: middle" class="text-center">Titik Ukur</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa R</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa S</th>
                                            <th colspan="3" style="vertical-align: middle" class="text-center">Fasa T</th>
                                        </tr>
                                        <tr>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                            <th style="vertical-align: middle" class="text-center">Standart</th>
                                            <th style="vertical-align: middle" class="text-center">Th.Lalu</th>
                                            <th style="vertical-align: middle" class="text-center">Hasil ukur</th>
                                        </tr>
                                        <?php
                                        $no_1++;
                                    } elseif ($value->pembeda == 1 and $value->kode == '10') {
                                        //alat uji
                                        ?>
                                        <tr>
                                            <!--<td>-->
                                    <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                    <!--</td>-->
                                    <td colspan="2"><?php echo $value->titik_ukur; ?></td>
                                    <td colspan="9"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value="<?php echo $value->injeksi_teg; ?>"></td>
                                    </tr>
                                    <tr>
                                        <td><b>2</b></td>
                                        <td colspan="10"><b>Tahanan Petanahan</b></td>
                                    </tr>
                                    <?php
                                } elseif ($value->pembeda == 1 and $jml == 2) {
                                    ?>
                                    <tr>
                                        <!--<td>-->
                                    <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                    <!--</td>-->
                                    <td colspan="2"><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                    <?php echo($no_2 == 1) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td></td>
                                    <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value="<?php echo $value->fasa_r_hu; ?>"></td>
                                    <?php echo($no_2 == 1) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td></td>
                                    <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value="<?php echo $value->fasa_s_hu; ?>"></td>
                                    <?php echo($no_2 == 1) ? "<td style='vertical-align: middle;text-align: center;' rowspan='3'>1 KV / 1 MW</td>" : ""; ?>
                                    <td></td>
                                    <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value="<?php echo $value->fasa_t_hu; ?>"></td>
                                    </tr>
                                    <?php
                                    $no_2++;
                                } elseif ($value->kode == '2' and $value->pembeda == '2') {
                                    ?>
                                    <tr>
                                        <td><b>2</b></td>
                                        <td><b><?php echo $value->titik_ukur; ?></b></td>
                                        <td colspan="9"></td>
                                    </tr>
                                    <?php
                                    $no_2++;
                                } elseif ($value->kode == '2.1' and $value->pembeda == 2) {
                                    ?>
                                    <tr>
                                        <!--<td>-->
                                    <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                    <!--</td>-->
                                    <td colspan="2"><?php echo $value->titik_ukur; ?></td>
                                    <td>1 KV/ M</td>
                                    <td></td>
                                    <td><input type="text" class="form-control" name="hu1_<?php echo $value->id; ?>" value="<?php echo $value->fasa_r_hu; ?>"></td>
                                    <td>1 KV/ M</td>
                                    <td></td>
                                    <td><input type="text" class="form-control" name="hu2_<?php echo $value->id; ?>" value="<?php echo $value->fasa_s_hu; ?>"></td>
                                    <td>1 KV/ M</td>
                                    <td></td>
                                    <td><input type="text" class="form-control" name="hu3_<?php echo $value->id; ?>" value="<?php echo $value->fasa_t_hu; ?>"></td>
                                    </tr>
                                    <?php
                                    $no_3++;
                                } elseif ($value->kode == '' and $value->pembeda == 2) {
                                    //alat uji
                                    ?>
                                    <tr>
                                        <!--<td>-->
                                    <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                    <!--</td>-->
                                    <td colspan="2"><?php echo $value->titik_ukur; ?></td>
                                    <td colspan="9"><input class="form-control" type="text" name="injeksi_<?php echo $value->id; ?>" value="<?php echo $value->injeksi_teg; ?>"></td>
                                    </tr>
                                    <tr>
                                        <td><b>3</b></td>
                                        <td colspan="10"><b>Counter Arrester</b></td>
                                    </tr>
                                    <?php
                                } elseif ($value->kode == '3' and $value->pembeda == 3) {
                                    //alat uji
                                    ?>
                                    <tr>
                                        <td><b>3</b></td>
                                        <td colspan="10"><b><?php echo $value->titik_ukur; ?></b></td>

                                    </tr>
                                    <?php
                                } elseif ($value->kode == '' and $value->pembeda == 3) {
                                    //penunjukan
                                    ?>
                                    <tr>
                                       <!--<td>-->
                                    <input type="hidden" name="hasil_ukurnya[]" value="<?php echo $value->id; ?>">
                                    <!--</td>-->
                                    <td colspan="5"><?php echo $value->kode . ' ' . $value->titik_ukur; ?></td>
                                    <td><input type="text" class="form-control" name="fasa_r_sebelum_<?php echo $value->id; ?>" value="<?php echo $value->fasa_r_sebelum; ?>"></td>
                                    <td><input type="text" class="form-control" name="fasa_r_sesudah_<?php echo $value->id; ?>" value="<?php echo $value->fasa_r_sesudah; ?>"></td>
                                    <td><input type="text" class="form-control" name="fasa_s_sebelum_<?php echo $value->id; ?>" value="<?php echo $value->fasa_s_sebelum; ?>"></td>
                                    <td><input type="text" class="form-control" name="fasa_s_sesudah_<?php echo $value->id; ?>" value="<?php echo $value->fasa_s_sesudah; ?>"></td>
                                    <td><input type="text" class="form-control" name="fasa_t_sebelum_<?php echo $value->id; ?>" value="<?php echo $value->fasa_t_sebelum; ?>"></td>
                                    <td><input type="text" class="form-control" name="fasa_t_sesudah_<?php echo $value->id; ?>" value="<?php echo $value->fasa_t_sesudah; ?>"></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            <tr>
                                <td><b>4</b></td>
                                <td><b>Catatan</b></td>
                                <td colspan="9">
                                    <textarea  class="form-control" name="catatan2"><?php echo $catatan[0]->uraian_uji; ?></textarea>
                                </td>
                            </tr>

                            <?php ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dTable2" width="100%" border="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th style="vertical-align: middle" class="text-center">Pelaksana Pengujian :</th>
                                    <th style="vertical-align: middle" class="text-center">Pengawas Pekerjaan</th>
                                    <th style="vertical-align: middle" class="text-center">Penanggung Jawab</th/>
                                </tr>
                            </thead>
                            <tbody id="data">
                                <?php
                                for ($i = 0; $i < 3; $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        <td><input class="form-control"  type="text" name="pelaksana_<?php echo $i; ?>" value="<?php echo $penanggung_jawab[$i]->pelaksana; ?>"></td>
                                        <td><input class="form-control"  type="text" name="pengawas_<?php echo $i; ?>" value="<?php echo $penanggung_jawab[$i]->pengawas; ?>"></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <button id="simpan_data" type="submit" formnovalidate="" class="btn btn-success pull-right" title="Simpan Data SPP UP"><i class="fa fa-save"></i> Simpan</button>
                            <a href="<?php echo site_url('mode_la/la'); ?>"  class="btn btn-default pull-right"  title="Kembali ke Daftar SPP UP" style="margin-right: 10px;"><i class="fa fa-backward"></i>  Kembali</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#spp_up").formValidation();
        set_jumlah();
        var jml = $('#jumlah_spp_val').val();
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        function set_jumlah() {
            $("#jumlah_spp").autoNumeric('init', {aSep: '.', aDec: ',', mDec: 0, vMax: '999999999999999999'});
            rupiah = $("#jumlah_spp").autoNumeric('get');
            $("#jumlah_spp_val").val(rupiah);

        }
        $('#jumlah_spp').keyup(function () {
            set_jumlah();
            var jml = $('#jumlah_spp_val').val();
            $.ajax({type: "POST",
                data: {jumlah: jml},
                url: "<?php echo base_url('spp_up/generate_terbilang') ?>", asynchronous: true,
                cache: false,
                success: function (result) {

                    $('#terbilang').text(result.toUpperCase());
                }
            });

        });

        $('#nip_penerima').on('keyup', function () {
            $("#nip_penerima").autocomplete({
                minLength: 1,
                source:
                        function (req, add) {
                            $.ajax({
                                url: "<?php echo base_url('spp_up/get_penerima') ?>", dataType: 'json',
                                type: 'POST',
                                data: req,
                                success: function (data) {
                                    if (data.response === "true") {
                                        add(data.message);
                                    } else {
                                        add(data.value);
                                    }
                                }
                            });
                        }, select: function (event, ui) {
                    $("#nip_penerima").val(ui.item.value);
                    $("#nama_penerima").val(ui.item.label);
                    $("#nama_bank").val(ui.item.nama_bank);
                    $("#no_rekening").val(ui.item.nomor_rekening);
                    $("#pemilik_rekening").val(ui.item.pemilik_rek);
                    $("#npwp").val(ui.item.npwp);
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                        .append("<a><strong>" + item.value + "</strong><br>" + "<span style='margin-left:20px'>" + item.label + "</span>" + "</a>")
                        .appendTo(ul);
            };
        });

        var $form = $("#spp_up");
        $($form).on('submit', function (e) {
            e.preventDefault();
            var cek = $("#jumlah_spp").val();
            var tgl = $("#tanggale").val();
            var nip_penerima = $('#nip_penerima').val();
            var uraian = $('#uraian').val();
            var nama_penerima = $("#nama_penerima").val();
            var nama_bank = $("#nama_bank").val();
            var no_rekening = $("#no_rekening").val();
            var pemilik_rekening = $("#pemilik_rekening").val();
            var npwp = $("#npwp").val();
            if (cek === '' || tgl === '' || nip_penerima === '' || uraian === '' || nama_penerima === '' || nama_bank === '' || no_rekening === '' || pemilik_rekening === '' || npwp === '') {
                swal('Terjadi Kesalahan!',
                        'Maaf semua form harus diisi!',
                        'error');
                return false;
            } else {
                swal({
                    title: 'Konfirmasi simpan!',
                    text: "Apakah anda yakin akan menyimpan data ini?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                }).then(function (tes) {
                    if (tes) {
                        $.ajax({
                            url: $($form).attr('action'),
                            type: 'POST',
                            data: $($form).serialize(),
                            success: function (result) {
                                swal({title: 'Informasi simpan!',
                                    text: "Data berhasil disimpan.",
                                    type: 'success'
                                }).then(function (ya) {
                                    window.location.href = "<?php echo site_url('spp_up'); ?>";
                                });
                            }
                        });
                    }
                });
            }
        });

    });
</script>