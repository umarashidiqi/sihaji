<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class La extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->valid_login();
        $this->load->model(array('La_m'));
    }

    public function index() {
        $this->data['judul'] = "Menu Data LA";
        $this->data['datanya'] = $this->La_m->view('data_umum', array('jenis_mode_testing' => 'LA'));
        $this->content = 'daftar';
        $this->view_m();
    }

    public function tambah() {
        $this->data['action'] = base_url('mode_la/la/tambah_proses');
        $this->data['judul'] = "Tambah Data LA ";
        $this->data['sub_judul'] = "Form Tambah Data LA ";
        $this->data['data_alat'] = $this->La_m->get('peralatan_la');
        $this->data['hasil_uji'] = $this->La_m->get('hasil_uji_la');
        $this->content = 'form';
        $this->view();
    }

    public function edit() {
        $id = 37;
        $this->data['action'] = base_url('mode_ct/ct/tambah_proses');
        $this->data['judul'] = "Tambah Data CT ";
        $this->data['data_edit'] = $this->La_m->get_data_edit($id);
        $this->data['data_umum'] = $this->La_m->view('data_umum', array('id' => $id));
//        print_r_pre($this->data['data_umum']);
        $this->data['data_uji'] = $this->La_m->get_data_uji($id);
        $this->data['catatan'] = $this->La_m->view('catatan', array('id_data_umum' => $id));

        $this->data['penanggung_jawab'] = $this->La_m->view('penanggung_jawab', array('id_data_umum' => $id));

//        $this->data['data_alat'] = $this->La_m->get('peralatan_ct');
//        $this->data['hasil_uji'] = $this->La_m->get('hasil_uji_ct');
//        $this->data['data_edit'] = $this->La_m->get_data_edit(6);

        $this->content = 'form_edit';
        $this->view();
    }

    public function cetak($id) {
        $this->data['data_edit'] = $this->La_m->get_data_edit($id);
        $this->data['data_umum'] = $this->La_m->view('data_umum', array('id' => $id));
//         print_r_pre($this->data['data_umum']);
        $this->data['data_uji'] = $this->La_m->get_data_uji($id);
        $this->data['catatan'] = $this->La_m->view('catatan', array('id_data_umum' => $id));
        $this->data['penanggung_jawab'] = $this->La_m->view('penanggung_jawab', array('id_data_umum' => $id));

        $this->data['data_alat'] = $this->La_m->get('peralatan_ct');
        $this->data['hasil_uji'] = $this->La_m->get('hasil_uji_ct');
//        $this->data['data_edit'] = $this->La_m->get_data_edit(6);

        $this->content = 'cetak';
        $this->view();

        $html = $this->output->get_output();
        //$this->view();
        generate_pdf($html);
    }

    public function tambah_proses() {
        $rule = array(
            array('field' => 'nilai', 'label' => 'Nilai', 'rules' => 'required'),
            array('field' => 'tanggal', 'label' => 'Tanggal', 'rules' => 'required'),
            array('field' => 'uraian', 'label' => 'Uraian', 'rules' => 'required')
        );
        $input_data_umum = array(
            'id_user' => session('id_user'),
            'nomor' => $this->input->post('nomor'),
            'tanggal' => $this->input->post('tanggal'),
            'merk' => $this->input->post('merk'),
            'type' => $this->input->post('type'),
            'type_fasa_r' => $this->input->post('fasa_r'),
            'type_fasa_s' => $this->input->post('fasa_s'),
            'type_fasa_t' => $this->input->post('fasa_t'),
//            'ratio_core_1' => $this->input->post('ratio_1'),
//            'ratio_core_2' => $this->input->post('ratio_2'),
//            'ratio_core_3' => $this->input->post('ratio_3'),
//            'ratio_core_4' => $this->input->post('ratio_4'),
//            'ratio_core_5' => $this->input->post('ratio_5'),
            'bay' => $this->input->post('bay'),
            'lokasi_gi' => $this->input->post('lokasi_gi'),
            'jenis_mode_testing' => 'LA',
            'tahun' => $this->input->post('tahun')
        );
        echo 'input data umum</br>';
        print_r_pre($input_data_umum);
        if ($id = $this->La_m->insert('data_umum', $input_data_umum)) {
            $uk_alat = $this->input->post('idnyak');
            echo 'input data ceklist</br>';
            foreach ($uk_alat as $value) {
                $checklist = array(
                    'id_data_umum' => $id,
                    'id_alat' => $value,
                    'k1' => $this->input->post('k1_' . $value),
                    'k2' => $this->input->post('k2_' . $value),
                    'k3' => $this->input->post('k3_' . $value),
                    'k4' => $this->input->post('k4_' . $value)
                );
                print_r_pre($checklist);
                if ($this->La_m->insert('checklist_la', $checklist)) {
                    echo 'sukses';
                }
            }

            $input_catatan = array(
                'id_data_umum' => $id,
//                'uraian_checklist' => $this->input->post('catatan1'),
                'uraian_uji' => $this->input->post('catatan2')
            );
            echo 'input data catatan</br>';
            print_r_pre($input_catatan);
            if ($this->La_m->insert('catatan', $input_catatan)) {
                echo 'sukses';
            }

//        echo '----' . $this->input->post('hu1_3');
            echo 'input data hasil uji</br>';
            $hasil_ukurnya = $this->input->post('hasil_ukurnya');
            foreach ($hasil_ukurnya as $value) {
                $input_hasiluji = array(
                    'id_data_umum' => $id,
                    'id_hasil_uji' => $value,
//                    'injeksi_teg1' => $this->input->post('injeksi_2'),
//                    'injeksi_teg2' => $this->input->post('injeksi_9'),
                    'fasa_r_std' => '',
                    'fasa_r_th' => '',
                    'fasa_r_hu' => $this->input->post('hu1_' . $value),
                    'fasa_s_std' => '',
                    'fasa_s_th' => '',
                    'fasa_s_hu' => $this->input->post('hu2_' . $value),
                    'fasa_t_std' => '',
                    'fasa_t_th' => '',
                    'fasa_t_hu' => $this->input->post('hu3_' . $value),
                    'injeksi_teg' => $this->input->post('injeksi_' . $value),
                    'fasa_r_sebelum' => $this->input->post('fasa_r_sebelum_' . $value),
                    'fasa_r_sesudah' => $this->input->post('fasa_r_sesudah_' . $value),
                    'fasa_s_sebelum' => $this->input->post('fasa_s_sebelum_' . $value),
                    'fasa_s_sesudah' => $this->input->post('fasa_s_sesudah_' . $value),
                    'fasa_t_sebelum' => $this->input->post('fasa_t_sebelum_' . $value),
                    'fasa_t_sesudah' => $this->input->post('fasa_t_sesudah_' . $value)
                );
                print_r_pre($input_hasiluji);
                if ($this->La_m->insert('pengujian_la', $input_hasiluji)) {
                    echo 'sukses';
                }
            }
            echo 'input data pj</br>';
            for ($i = 1; $i < 4; $i++) {
                $pj = array(
                    'id_data_umum' => $id,
                    'pelaksana' => $this->input->post('pelaksana_' . $i),
                    'pengawas' => $this->input->post('pengawas_' . $i)
                );
                print_r_pre($pj);
                if ($this->La_m->insert('penanggung_jawab', $pj)) {
                    echo 'sukses';
                }
            }
        }
        redirect(base_url('mode_la/la'));
    }

}
