<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sistem Informasi Hasil Pengujian">
        <meta name="author" content="Umar Ashidiqi">
        <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/android-chrome-192x192.png" sizes="192x192">
        <title>Si Haji v1.0</title>
        <!-- plugin CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo css('bootstrap.min.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo css() ?>jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo css() ?>jquery.datetimepicker.css">
        <link rel="stylesheet" type="text/css" href="<?php echo css() ?>sweetalert.css">
        <link rel="stylesheet" type="text/css" href="<?php echo css('style.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo css('font-awesome.css'); ?>" rel="stylesheet">

        <script type="text/javascript" src="<?php echo js('jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo js() ?>jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo js() ?>sweetalert.min.js"></script>
        <script type="text/javascript" src="<?php echo js() ?>dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo js() ?>jquery.datetimepicker.js"></script>
        <script type="text/javascript" src="<?php echo js('pace.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo js('bootstrap.min.js'); ?>"></script>
    </head> 
    <body>
        <div class="wrapper">
            <?php $this->load->view($content); ?>
        </div>
    </body>

</html>

