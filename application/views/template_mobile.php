<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0 minimal-ui"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/android-chrome-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" sizes="196x196" href="<?php echo image() ?>/splash/apple-touch-icon-196x196.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo image() ?>/splash/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo image() ?>/splash/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo image() ?>/splash/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo image() ?>/splash/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo image() ?>/splash/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo image() ?>/splash/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo image() ?>/splash/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo image() ?>/splash/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo image() ?>/splash/apple-touch-icon-57x57.png">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo image() ?>/splash/favicon-16x16.png" sizes="16x16">
    <link rel="shortcut icon" href="<?php echo image() ?>/splash/favicon.ico" type="image/x-icon"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
    <title>Sihaji 1.0</title>
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>framework.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo css() ?>font-awesome.css">

    <script type="text/javascript" src="<?php echo js() ?>jquery.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>plugins.js"></script>
    <script type="text/javascript" src="<?php echo js() ?>custom.js"></script>
</head>
<body class="has-cover">
    <?php $this->load->view($content); ?>
    <a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i>Back to top</a>
</body>