<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
  |--------------------------------------------------------------------------
  | Class Application
  | Parent class from this application
  |--------------------------------------------------------------------------
 */
class Base_Controller extends CI_Controller {

    protected $subTitle;
    protected $data;
    protected $dataContent;
    protected $content;
    protected $sess;
    protected $theme = 'template';
    protected $theme_phone = 'template_mobile';
    protected $linkPage;
    protected $segment = 3;
    protected $totalPage = 0;
    protected $perPage = 10;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function valid_login() {
        if ($this->session->userdata('status') == "") {
            redirect(site_url('welcome'), 'refresh');
        } else {
            $this->sess = $this->session->all_userdata();
             // all_userdata diambil dari Session di CI
        }
    }

    public function valid_admin() {
        if ($this->session->userdata('level') <> 'SU') {
            show_404();
        }
    }

    public function view() {
        if (isset($this->subTitle)) {
            $this->data['title'] = $this->subTitle;
        } else {
            $this->data['title'] = system_name();
        }
        $this->data['subTitle'] = $this->subTitle;
        $this->data['data'] = $this->dataContent;
        $this->data['content'] = $this->content;

        return $this->load->view($this->theme, $this->data);
    }
    public function view_m() {
        if (isset($this->subTitle)) {
            $this->data['title'] = $this->subTitle;
        } else {
            $this->data['title'] = system_name();
        }
        $this->data['subTitle'] = $this->subTitle;
        $this->data['data'] = $this->dataContent;
        $this->data['content'] = $this->content;

        return $this->load->view($this->theme_phone, $this->data);
    }

    public function noview() {
        if (isset($this->subTitle)) {
            $this->data['title'] = $this->subTitle;
        } else {
            $this->data['title'] = system_name();
        }
        $this->data['subTitle'] = $this->subTitle;
        $this->data['data'] = $this->dataContent;
        $this->data['content'] = $this->content;
        $this->data['Tema'] = $this->dataTema;

        return $this->load->view($this->data['Tema'], $this->data);
    }

    protected function encrypt($str = "") {
        $pengacak = "AJWKXLAJSCLWLWDAKDKSAJDADKEOIJEOQWENQWENQONEQWAJSNDKASO";
        $passEnkrip = md5($pengacak . md5($str) . $pengacak);
        return $passEnkrip;
    }

    protected function pagination() {
        $config = array();
        $config['uri_segment'] = $this->segment;
        $config['base_url'] = $this->linkPage;
        $config['total_rows'] = $this->totalPage;
        $config['per_page'] = $this->perPage;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = $config['last_tag_open'] = $config['next_tag_open'] = $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close'] = $config['next_tag_close'] = $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = "<li class='active'><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }

    protected function start_page($uri) {
        if ($this->uri->segment($uri) > 0) {
            $start = $this->uri->segment($uri);
            $this->data['no'] = $start + 1;
        } else {
            $start = 0;
            $this->data['no'] = $start + 1;
        }
        return $start;
    }

}
