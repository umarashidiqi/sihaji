<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/*
  |--------------------------------------------------------------------------
  | function of path css
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_css')) {

    function css($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_css') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of path javascript
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_js')) {

    function js($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_js') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of path images
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_img')) {

    function image($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_img') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of path uploads
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_upload')) {

    function path_upload($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_upload') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of name system
  |--------------------------------------------------------------------------
 */
if (!function_exists('system_name')) {

    function system_name($descript = '') {
        $CI = & get_instance();
        if ($descript != '') {
            return $CI->config->item('name_system') . ' - ' . $descript;
        }
        return $CI->config->item('name_system');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of company
  |--------------------------------------------------------------------------
 */
if (!function_exists('company')) {

    function company() {
        $CI = & get_instance();
        return $CI->config->item('company');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of Address
  |--------------------------------------------------------------------------
 */
if (!function_exists('address')) {

    function address() {
        $CI = & get_instance();
        return $CI->config->item('address');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of telepon
  |--------------------------------------------------------------------------
 */
if (!function_exists('telepon')) {

    function telepon() {
        $CI = & get_instance();
        return $CI->config->item('telepon');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of version
  |--------------------------------------------------------------------------
 */
if (!function_exists('version')) {

    function version() {
        $CI = & get_instance();
        return $CI->config->item('version');
    }

}


/*
  |--------------------------------------------------------------------------
  | function of developer
  |--------------------------------------------------------------------------
 */
if (!function_exists('developer')) {

    function developer() {
        $CI = & get_instance();
        return $CI->config->item('developer');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of telepon
  |--------------------------------------------------------------------------
 */
if (!function_exists('telepon')) {

    function telepon() {
        $CI = & get_instance();
        return $CI->config->item('telepon');
    }

}

/**
 * -----------------------------------------------------------------------------
 * fungsi filter data
 * -----------------------------------------------------------------------------
 */
if (!function_exists('filter_data')) {

    function filter_data($data) {
        $data = trim($data);
        $back = strtoupper(stripslashes(strip_tags($data, ENT_QUOTES)));
        return $back;
    }

}

if (!function_exists('filter_numeric')) {

    function filter_numeric($no = '') {
        $no = str_replace(',', '', $no);
        $nomor = filter_data($no);
        if ($no == '') {
            return 0;
        }
        return $nomor;
    }

}

if (!function_exists('sentence_case')) {

    function sentence_case($string) {
        $sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $new_string = '';
        foreach ($sentences as $key => $sentence) {
            $new_string .= ($key & 1) == 0 ?
                    ucfirst(strtolower(trim($sentence))) :
                    $sentence . ' ';
        }
        return trim($new_string);
    }

}

if (!function_exists('random_string')) {

    function random_string($str = "") {
        //$pengacak = "AJWKXLAJSCLWLWDAKDKSAJDADKEOIJEOQWENQWENQONEQWAJSNDKASO";
        $passEnkrip = md5($str);
        return $passEnkrip;
    }

}

if (!function_exists("format_idr")) {

    function format_idr($angka = '') {
        if (strlen($angka) > 0) {
            return number_format($angka, 2, '.', ',');
        } else {
            return 0;
        }
    }

}

if (!function_exists('enkripsi_numeric')) {

    function enkripsi_numeric($string = '') {
        $string = filter_numeric($string);
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, ENCRYPTION_KEY, $string, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

}

if (!function_exists('dekripsi_numeric')) {

    function dekripsi_numeric($string = '') {
        $string = filter_numeric($string);
        if (strlen($string) > 1) {
            return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, ENCRYPTION_KEY, base64_decode($string), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
        } else {
            return $string;
        }
    }

}

if (!function_exists('gender')) {

    function gender($type = '') {
        if ($type == '') {
            $data = array(
                '' => '-- Pilih --',
                'L' => 'Laki - Laki',
                'P' => 'Perempuan'
            );
        } else {
            switch ($type) {
                case 'L' : $data = 'Laki - Laki';
                    break;
                case 'P' : $data = 'Perempuan';
                    break;
                default : $data = "";
            }
        }
        return $data;
    }

}

if (!function_exists('kebangsaan')) {

    function kebangsaan($type = '') {
        if ($type == '') {
            $data = array(
                '' => '-- Pilih --',
                'WNI' => 'Indonesia',
                'WNA' => 'Orang Asing'
            );
        } else {
            switch ($type) {
                case 'WNI' : $data = 'Indonesia';
                    break;
                case 'WNA' : $data = 'Orang Asing';
                    break;
                default : $data = "";
            }
        }
        return $data;
    }

}

if (!function_exists('pendidikan')) {

    function pendidikan($type = '') {
        $data = array(
            '' => '-- Pilih --',
            'SD' => 'SD',
            'SMP' => 'SMP',
            'SMA' => 'SMA',
            'SMK' => 'SMK',
            'D I' => 'D I',
            'D II' => 'D II',
            'D III' => 'D III',
            'S1' => 'S1',
            'S2' => 'S2',
            'S3' => 'S3'
        );
        return $data;
    }

}

if (!function_exists("gol_darah")) {

    function gol_darah() {
        $data = array(
            '' => '-- Pilih --',
            'O' => 'Gol. O',
            'A' => 'Gol. A',
            'B' => 'Gol. B',
            'AB' => 'Gol. AB'
        );
        return $data;
    }

}

if (!function_exists("marital")) {

    function marital($kode = '') {
        if ($kode == '') {
            $data = array(
                '' => '-- Pilih --',
                'KW' => 'Kawin',
                'BK' => 'Belum Kawin',
                'JN' => 'Janda',
                'DD' => 'Duda'
            );
        } else {
            switch ($kode) {
                case 'KW' : $data = 'Kawin';
                    break;
                case 'BK' : $data = 'Belum Kawin';
                    break;
                case 'JN' : $data = 'Janda';
                    break;
                case 'DD' : $data = 'Duda';
                    break;
                default : $data = "";
            }
        }
        return $data;
    }

}

if (!function_exists("agama")) {

    function agama() {
        $data = array(
            '' => '-- Pilih --',
            'ISLAM' => 'Islam',
            'KRISTEN' => 'kristen',
            'KATHOLIK' => 'Katholik',
            'HINDU' => 'Hindu',
            'BUDHA' => 'Budha'
        );
        return $data;
    }

}

if (!function_exists('dateToIndo')) {

    function dateToIndo($date = '') {
        $pecah = explode('-', $date);
        if (count($pecah) > 2) {
            $getDate = $pecah[2] . '-' . $pecah[1] . '-' . $pecah[0];
        } else {
            $getDate = $date;
        }
        return $getDate;
    }

}

if (!function_exists("numerator")) {

    function numerator($prefix = '', $range = 10) {
        $CI = & get_instance();
        $prefix .= date("y");
        $year = date('Y');
        $month = date('m');
        $desid = session('desid');
        $CI->load->database();
        $sql = $CI->db->query("select mnu_value from numerator where mnu_key='$prefix' AND mnu_desid='$desid' ");
        if ($sql->num_rows() == 0) {
            $nomor = "";
            $CI->db->query("insert into numerator values ('$prefix','$year','$month', '0', '$desid') ");
            $long = 0;
        } else {
            $nomor = $prefix . $sql->row()->mnu_value;
            $long = strlen($nomor);
        }
        $integer = '';
        $shot = strlen($prefix);
        $sisa = ($long - $shot);
        if ($long == 0) {
            $sufix = $range - $shot;
            for ($i = 1; $i <= $sufix; $i++) {
                $integer .= '0';
            }
        } else {
            $integer = substr($nomor, $shot, $sisa);
        }
        $kode = (int) substr($integer, 0);
        $kode++;
        $newKode = sprintf("%0" . strlen($integer) . "s", $kode);
        $CI->db->query("update numerator set mnu_value='$newKode' where mnu_key='$prefix' ");
        $auto_number = $prefix . $newKode;
        return $auto_number;
    }

}

if (!function_exists("data_404")) {

    function data_404() {
        echo "<h2>Data Not Found</h2>";
    }

}

if (!function_exists('date_differ')) {

    function date_differ($data = array()) {
        if (is_array($data)) {
            $date_diff = (strtotime($data[0]) - strtotime($data[1])) / 86400;
        } else {
            $date_diff = "0";
        }
        return $date_diff;
    }

}

if (!function_exists('session')) {

    function session($name) {
        $CI = & get_instance();
        return $CI->session->userdata($name);
    }

}
if (!function_exists('print_r_pre')) {

    function print_r_pre($data = '', $die = false) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        if ($die)
            die();
    }

}

if (!function_exists('generate_pdf')) {

    function generate_pdf($view, $options = false) {
        $CI = &get_instance();
        $CI->load->library('dompdf_gen');
        $CI->dompdf->load_html($view);
        $CI->dompdf->set_paper('legal', 'portrait');
        $CI->dompdf->render();
        $CI->dompdf->stream("Data" . ".pdf");
    }

}

function konfirmasi_swal($objek = '', $options = array()) {
    $options = (!$options) ?
            array("class" => 'hapus_btn',
        "title" => 'Ingin Menghapus ' . $objek . '?',
        "subtitle" => 'Anda tidak dapat mengembalikan data yang telah dihapus',
        "subtitle_ok" => $objek . ' telah terhapus',
        "subtitle_fail" => $objek . ' gagal terhapus'
            ) : $options;

    $alert = ' $(".' . $options['class'] . '").click(function(e){
                    e.preventDefault();
                    var url=$(this).attr("href");';
    $alert.='swal({
                    title:"' . $options['title'] . '", 
                    text:"' . $options['subtitle'] . '",
                    type: "warning",
                    confirmButtonColor:"#d43f3a",
                    showCancelButton:true,
                    confirmButtonText:"Ya",
                    cancelButtonText:"Batal",
                    closeOnConfirm:false
                }).then(function (isConfirm) {
                     if(isConfirm){
                         $.ajax({
                            type:"POST",
                            url: url,
                            asynchronous: true,
                            cache: false,
                            beforeSend: function(){
                              swal({
                                title:"memproses...",
                                imageUrl:"/epen/themes/images/ring2.gif"
                              });
                            },
                           success: function(result){
                              if(result){
                                swal({
                                    title:"Berhasil",
                                    text:"' . $options['subtitle_ok'] . '",
                                    type:"success",
                                    closeOnConfirm:false
                                }).then(function (ya) {
                                    window.location.reload(false);
                                });
                              }else{
                                swal("Gagal","' . $options['subtitle_fail'] . '","error");
                              }
                           }
                        });
                     }
                 }
            );

        });';
    echo $alert;
}
