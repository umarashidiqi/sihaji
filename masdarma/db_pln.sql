-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2016 at 01:10 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_pln`
--

-- --------------------------------------------------------

--
-- Table structure for table `checklist_ct`
--

CREATE TABLE IF NOT EXISTS `checklist_ct` (
  `id` int(11) NOT NULL,
  `id_data_umum` varchar(50) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `peralatan` varchar(50) DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `kondisi_awal` varchar(50) DEFAULT NULL,
  `kondisi_akhir` varchar(50) DEFAULT NULL,
  `kondisi_awal_kiri` varchar(50) DEFAULT NULL,
  `kondisi_awal_kanan` varchar(50) DEFAULT NULL,
  `kondisi_akhir_kiri` varchar(50) DEFAULT NULL,
  `kondisi_akhir_kanan` varchar(50) DEFAULT NULL,
  `catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checklist_ct`
--

INSERT INTO `checklist_ct` (`id`, `id_data_umum`, `kode`, `peralatan`, `jenis`, `kondisi_awal`, `kondisi_akhir`, `kondisi_awal_kiri`, `kondisi_awal_kanan`, `kondisi_akhir_kiri`, `kondisi_akhir_kanan`, `catatan`) VALUES
(1, '1', '1.1', 'kebersihan', NULL, NULL, NULL, 'baik', NULL, 'bagus', NULL, NULL),
(2, '1', '1.2', 'kerapian', NULL, NULL, NULL, '', 'tidak baik', NULL, 'jelek', NULL),
(3, '1', '2.1', 'bobot', NULL, NULL, NULL, 'longgar', NULL, NULL, 'jelek', NULL),
(4, '1', '2.2', 'gas', NULL, NULL, NULL, 'longgar', NULL, 'cukup', NULL, NULL),
(5, '1', '2.3', 'kebengkokan', NULL, NULL, NULL, NULL, 'tidak bengkok', 'cukup', NULL, NULL),
(6, '1', '4.1', 'kepanjangan', NULL, NULL, NULL, NULL, 'tidak bengkok', NULL, NULL, NULL),
(7, '1', '4.2', 'kependekan', NULL, NULL, NULL, 'cukup', NULL, NULL, 'tidak baik', NULL),
(8, '1', '4.3', 'kebesaran', NULL, NULL, NULL, NULL, 'lebih', NULL, 'tidak buruk', NULL),
(9, '1', '5.1', 'kekecilan', NULL, NULL, NULL, NULL, 'besar', 'super', NULL, NULL),
(10, '1', '6.1', 'kesempitan', NULL, NULL, NULL, NULL, 'besar', 'sedang', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `checklist_la`
--

CREATE TABLE IF NOT EXISTS `checklist_la` (
  `id` int(11) NOT NULL,
  `id_data_umum` varchar(255) DEFAULT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `peralatan` varchar(255) DEFAULT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `kondisi_awal` varchar(255) DEFAULT NULL,
  `kondisi_akhir` varchar(255) DEFAULT NULL,
  `kondisi_awal_kiri` varchar(255) DEFAULT NULL,
  `kondisi_awal_kanan` varchar(255) DEFAULT NULL,
  `kondisi_akhir_kiri` varchar(255) DEFAULT NULL,
  `kondisi_akhir_kanan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checklist_pmt`
--

CREATE TABLE IF NOT EXISTS `checklist_pmt` (
  `id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `peralatan` varchar(50) DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `kondisi_awal` varchar(50) DEFAULT NULL,
  `kondisi_akhir` varchar(255) DEFAULT NULL,
  `kondisi_awal_kiri` varchar(50) DEFAULT NULL,
  `kondisi_awal_kanan` varchar(50) DEFAULT NULL,
  `kondisi_akhir_kiri` varchar(50) DEFAULT NULL,
  `kondisi_akhir_kanan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_umum`
--

CREATE TABLE IF NOT EXISTS `data_umum` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `merk` varchar(50) DEFAULT NULL,
  `type_fasa_r` varchar(50) DEFAULT NULL,
  `type_fasa_s` varchar(50) DEFAULT NULL,
  `type_fasa_t` varchar(50) DEFAULT NULL,
  `ratio_core_1` varchar(50) DEFAULT NULL,
  `ratio_core_2` varchar(50) DEFAULT NULL,
  `ratio_core_3` varchar(50) DEFAULT NULL,
  `ratio_core_4` varchar(50) DEFAULT NULL,
  `ratio_core_5` varchar(50) DEFAULT NULL,
  `bay` varchar(50) DEFAULT NULL,
  `lokasi_gi` varchar(50) DEFAULT NULL,
  `jenis_mode_testing` varchar(50) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_umum`
--

INSERT INTO `data_umum` (`id`, `id_user`, `nomor`, `tanggal`, `type`, `merk`, `type_fasa_r`, `type_fasa_s`, `type_fasa_t`, `ratio_core_1`, `ratio_core_2`, `ratio_core_3`, `ratio_core_4`, `ratio_core_5`, `bay`, `lokasi_gi`, `jenis_mode_testing`, `tahun`) VALUES
(1, 1, '123', '2016-09-28 20:48:08', 'typenya', 'merk', '100', '2003', '4000', '800 L/A', '900 L/V', '900 P/A', '', '', 'PD LAMP 2', 'Semarang', 'CT', '2016'),
(2, 2, '12332', '2016-09-28 20:48:08', 'typenyae', 'merk2', '10023', '20031412', '4000412', '800 L/AS', '900 L/VA', '900 P/AD', '', '', 'PD LAMP 2', 'Semarang', 'CT', '2016');

-- --------------------------------------------------------

--
-- Table structure for table `penanggung_jawab`
--

CREATE TABLE IF NOT EXISTS `penanggung_jawab` (
  `id` int(11) NOT NULL,
  `pelaksana` varchar(50) DEFAULT NULL,
  `pengawas` varchar(50) DEFAULT NULL,
  `id_data_umum` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengujian_ct`
--

CREATE TABLE IF NOT EXISTS `pengujian_ct` (
  `id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `jenis_uji` varchar(255) DEFAULT NULL,
  `injeksi_teg1` varchar(255) DEFAULT NULL,
  `injeksi_teg2` varchar(255) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `alat_uji` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengujian_la`
--

CREATE TABLE IF NOT EXISTS `pengujian_la` (
  `id` int(11) NOT NULL,
  `id_data_umum` int(11) DEFAULT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `jenis_uji` varchar(255) DEFAULT NULL,
  `injeksi_teg` varchar(255) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `fasa_r_sebelum` varchar(255) DEFAULT NULL,
  `fasa_r_sesudah` varchar(255) DEFAULT NULL,
  `fasa_s_sebelum` varchar(255) DEFAULT NULL,
  `fasa_s_sesudah` varchar(255) DEFAULT NULL,
  `fasa_t_sebelum` varchar(255) DEFAULT NULL,
  `fasa_t_sesudah` varchar(255) DEFAULT NULL,
  `catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengujian_pmt`
--

CREATE TABLE IF NOT EXISTS `pengujian_pmt` (
  `id` int(11) NOT NULL,
  `id_data_umum` int(255) DEFAULT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `jenis_uji` varchar(255) DEFAULT NULL,
  `tahanan_isolasi` varchar(255) DEFAULT NULL,
  `titik_ukur` varchar(255) DEFAULT NULL,
  `alat_uji` varchar(255) DEFAULT NULL,
  `fasa_r_std` varchar(255) DEFAULT NULL,
  `fasa_r_th` varchar(255) DEFAULT NULL,
  `fasa_r_hu` varchar(255) DEFAULT NULL,
  `fasa_s_std` varchar(255) DEFAULT NULL,
  `fasa_s_th` varchar(255) DEFAULT NULL,
  `fasa_s_hu` varchar(255) DEFAULT NULL,
  `fasa_t_std` varchar(255) DEFAULT NULL,
  `fasa_t_th` varchar(255) DEFAULT NULL,
  `fasa_t_hu` varchar(255) DEFAULT NULL,
  `catatan` text,
  `pmt_sebelum` varchar(255) DEFAULT NULL,
  `pmt_sesudah` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checklist_ct`
--
ALTER TABLE `checklist_ct`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_la`
--
ALTER TABLE `checklist_la`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist_pmt`
--
ALTER TABLE `checklist_pmt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_umum`
--
ALTER TABLE `data_umum`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penanggung_jawab`
--
ALTER TABLE `penanggung_jawab`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengujian_ct`
--
ALTER TABLE `pengujian_ct`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengujian_la`
--
ALTER TABLE `pengujian_la`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengujian_pmt`
--
ALTER TABLE `pengujian_pmt`
 ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
